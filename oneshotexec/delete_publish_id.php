<?php

if (PHP_OS == "WIN32" || PHP_OS == "WINNT") {
	// Windwos用の処理
	require "C:\SRC\dev_saikio_batch\import\config.php";
	require "C:\SRC\dev_saikio_batch\import\db_oracle.php";
	require "C:\SRC\dev_saikio_batch\import\\tools.php";
	require "C:\SRC\dev_saikio_batch\import\log.php";
	require "C:\SRC\dev_saikio_batch\import\crawler_setting_data.php";
} else {
	// サーバ環境用の処理
	require "/var/www/import/config.php";
	require '/var/www/import/db_oracle.php';
	require "/var/www/import/tools.php";
	require "/var/www/import/log.php";
	require "/var/www/import/crawler_setting_data.php";
}

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);

/*
cronで定期的に稼働することが前提のプログラム
日付ごとにステータスを変えていく
*/

if( $argc != 4 ){
	echo "[1] : sell or rent\n";
	echo "[2] : country_cd\n";
	echo "[3] : target_data\n";
	exit;
}

if($argv[1]=='' or preg_match("/^(?!(sell|rent))$/", $argv[1])) {
	die("please set sell or rent");
}
if($argv[2]=='') {
	die("please set country_cd");
}
if($argv[3]=='') die("please set target_date");

$listing_type=$argv[1];
$country_cd=$argv[2];
$target_date=$argv[3];
$limit_num=100;
$target_num=10000000;

$log = new log();
$logfile='delete_'.$listing_type.'_'.$country_cd.'_'.$target_date;

$log->freeform($logfile, 'START');
$dt_start = new DateTime();

$loop_max = $target_num / $target_num;
$loop_num=0;
$dt_loop_in = new DateTime();
$dt_loop_out = new DateTime();
//対象publish_id抽出
$sql = "SELECT publish_id FROM tmp_closetarget_".$listing_type."_".$country_cd."_". $target_date ." WHERE id BETWEEN :start_id AND :end_id";
$stmt = $pdo_tp->prepare($sql);
//1件ずつ処理する
$sql1 = "DELETE FROM tag_mapping_" . $listing_type . " WHERE publish_id=:publish_id";
$sql2 = "DELETE FROM /*+INDEX(CLEANSING_" . $listing_type . "_CCD_AND_PUBID)*/ cleansing_" . $listing_type . " WHERE publish_id=:publish_id AND country_cd=" . $country_cd;
$sql3 = "DELETE FROM publish_" . $listing_type . " WHERE publish_id=:publish_id";
$stmt1 = $pdo_tp->prepare($sql1);
$stmt2 = $pdo_tp->prepare($sql2);
$stmt3 = $pdo_tp->prepare($sql3);

for($i=0; $i<=$target_num/$limit_num; $i++){
	$loop_num++;
	$dt_loop_in = new DateTime();
	$start_id = ($loop_num - 1) * $limit_num + 1;
	$end_id = $loop_num * $limit_num;
	$stmt->bindParam(':start_id', $start_id, PDO::PARAM_INT);
	$stmt->bindParam(':end_id', $end_id, PDO::PARAM_INT);
	$stmt->execute();
	$dt_loop_exec = new DateTime();
	$elapsed = $dt_loop_exec->diff($dt_loop_in)->format("%I:%S.%F");
	$log->freeform($logfile, 'ループ開始(' . $loop_num .'件) publish_id取得時間-> ' . $elapsed);
	$rows = $stmt->fetchAll();
	if(count($rows)==0){
		//処理終了
		$log->freeform($logfile, "exit:loop_num=".$loop_num);
		$dt_end = new DateTime();
		$elapsed = $dt_end->diff($dt_start)->format("%H:%I:%S.%F");
		$log->freeform($logfile, '処理終了 -> ' . $elapsed);
		exit;
	}
	foreach ($rows as $row) {
		$publish_id = $row['publish_id'];
		if($publish_id != '1'){
			$stmt1->bindParam(':publish_id', $publish_id, PDO::PARAM_INT);
			$stmt2->bindParam(':publish_id', $publish_id, PDO::PARAM_INT);
			$stmt3->bindParam(':publish_id', $publish_id, PDO::PARAM_INT);
			$dt1 = new DateTime;
			$stmt1->execute();
			$dt2 = new DateTime;
			$stmt2->execute();
			$dt3 = new DateTime;
			$stmt3->execute();
			$dt4 = new DateTime;
			$elapsed1 = $dt2->diff($dt1)->format("%I:%S.%F");
			$elapsed2 = $dt3->diff($dt2)->format("%I:%S.%F");
			$elapsed3 = $dt4->diff($dt3)->format("%I:%S.%F");
			$log->freeform($logfile, $publish_id . '/' . $elapsed1 . '/' . $elapsed2 . '/' . $elapsed3);
		}
	}
	$dt_loop_out = new DateTime();
	$elapsed = $dt_loop_out->diff($dt_loop_in)->format("%H:%I:%S.%F");
	$log->freeform($logfile, 'ループ終了(' . $loop_num .'件) -> ' . $elapsed);
	if($loop_num % 10 == 0){
		echo $loop_num * $limit_num . ' ';
	}
}
$dt_end = new DateTime();
$elapsed = $dt_end->diff($dt_start)->format("%H:%I:%S.%F");
$log->freeform($logfile, '処理終了 -> ' . $elapsed);

?>
