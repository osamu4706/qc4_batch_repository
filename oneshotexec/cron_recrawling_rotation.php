<?php

if (PHP_OS == "WIN32" || PHP_OS == "WINNT") {
	// Windwos用の処理
	require "C:\SRC\dev_saikio_batch\import\config.php";
	require "C:\SRC\dev_saikio_batch\import\db_oracle.php";
	require "C:\SRC\dev_saikio_batch\import\\tools.php";
	require "C:\SRC\dev_saikio_batch\import\log.php";
	require "C:\SRC\dev_saikio_batch\import\crawler_setting_data.php";
} else {
	// サーバ環境用の処理
	require "/var/www/import/config.php";
	require '/var/www/import/db_oracle.php';
	require "/var/www/import/tools.php";
	require "/var/www/import/log.php";
	require "/var/www/import/crawler_setting_data.php";
}

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);
$pdo_high= DB::getPdo(DB_ORA_TNS_HIGH);
$log = new log();

/*
cronで定期的に稼働することが前提のプログラム
日付ごとにステータスを変えていく
(1)crawl_setting.recleasing_num=0にセット
*/

if( $argc != 3 ){
	echo "[1] : set_word or reset_word\n";
	echo "[2] : site_no\n";
	exit;
}

if($argv[1]=='') die("please set site_no");
if($argv[2]=='') die("please set reset_word or set_word");

//terminalの第2引数を設定
$site_num='site_'.$argv[1];

if(array_key_exists($site_num,$specified_data)){
//	specified_crawlerの値を取得
	$specified_key=$specified_data[$site_num];
}else{
	$specified_key='';
}

print_r($specified_key);

//サイトno
$site_no[0] = $argv[1];
//reset or set
$my_flg = $argv[2];

$logfile = sprintf('recrawling_rotation_%s_%s',$site_no[0], date('Ymd'));
print_r($logfile);
$site_no_sql = '';
if(count($site_no)==0){
	exit;
}else if(count($site_no)==1){
	$site_no_sql = ' = ' . $site_no[0] . ' ';
}

if ($my_flg=='set'){
	$sql = "SELECT /*+INDEX(a CRAWLING_LIST_I01)*/ count(*) AS cnt FROM propre.crawling_list  WHERE site_no" . $site_no_sql ." AND status = 0";
	print_r($sql);
	$stmt = $pdo_high->query($sql);
	$row = $stmt->fetch();
	if($row['cnt'] > 0) {
		//status=0があれば、status=3300に変更
		$sql3 = "UPDATE crawl_setting SET status=3300 WHERE site_no" . $site_no_sql." and status=0 and rownum<100000";
		$stmt = $pdo_tp->prepare($sql3);
//		$stmt->bindValue(':specified_key', null, PDO::PARAM_STR);
		$stmt->execute();
	}else if ($row['cnt'] == 0){
		$log->freeform($logfile, 'picker開始');
	}

}

exit;
?>
