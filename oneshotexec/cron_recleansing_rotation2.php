<?php

/* 実行環境から見るので絶対パスで指定 */
require "/var/www/import/config.php";
require '/var/www/import/db_oracle.php';
require "/var/www/import/tools.php";
require "/var/www/import/log.php";

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);
$pdo_high= DB::getPdo(DB_ORA_TNS_HIGH);
$log = new log();

/*
cronで定期的に稼働することが前提のプログラム
日付ごとにステータスを変えていく
(1)crawl_setting.recleansing_num=0にセット
*/


if( $argc != 3 ){
	echo "[1] : set_word or reset_word\n";
	echo "[2] : site_no\n";
	exit;
}

if($argv[1]=='') die("please set site_no");
if($argv[2]=='') die("please set reset_word or set_word");

//サイトno
$site_no = $argv[1];
//reset or set
$my_flg = $argv[2];

$logfile = sprintf('recleansing_rotation_%s_%s',$site_no, date('Ymd'));
$site_no_sql = '';
if(count($site_no)==0){
	exit;
}else if(count($site_no)==1){
	$site_no_sql = ' = ' . $site_no . ' ';
}

if ($my_flg=='set'){
	$sql = "SELECT /*+INDEX(a CRAWLING_LIST_I01)*/ count(*) AS cnt FROM crawl_setting  WHERE site_no" . $site_no_sql ." AND status = 0";
	$stmt = $pdo_high->query($sql);
	$row = $stmt->fetch();
	if($row['cnt'] == 0) {
		//status=0がなかったら、crawl_settingのrescrape_num=1に設定し再クレンジングスタート
		$sql3 = "UPDATE crawl_setting SET recleansing_num=1, specified_crawler=:specified_key WHERE site_no" . $site_no_sql;
		$stmt = $pdo_tp->prepare($sql3);
		$stmt->bindValue(':specified_key', null, PDO::PARAM_STR);
		$stmt->execute();
		$log->freeform($logfile, '再クレンジング開始');
	}

}else if($my_flg=='reset'){
	if($site_no==8){
		$specified_key='901,902,';
	}
	//crawl_settingのrescrape_num=0に設定し日時処理開始
	$sql3 = "UPDATE crawl_setting SET recleansing_num=0, specified_crawler=:specified_key WHERE site_no" . $site_no_sql;
	$stmt = $pdo_tp->prepare($sql3);
	$stmt->bindParam(':specified_key', $specified_key, PDO::PARAM_STR);
	$stmt->execute(); //rescrape_num,recleansing_num数は1件目の時点で変えてすぐにdequeues処理に入れるようにする
	$log->freeform($logfile, '日時処理開始');

}



exit;
?>
