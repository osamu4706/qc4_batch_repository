<?php

/** 実行ファイルから見たパスで指定 */
require __DIR__.'/../import/config.php';
require __DIR__.'/../import/db_oracle.php';
require __DIR__.'/../import/log.php';

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);
$log = new log();
/**
 * ■ picker取り扱い件数取得バッチ
 *  JST14-15時くらいに当日分のピッカーが完了するので、16時にバッチ実施。
 *
 *  picker_targetから当日稼働分のサイトNo,を取得し、
 *  そのサイトNo.ごとにcrawler_listから各種件数を取得、
 *  records_we_madeテーブルに登録する
 *
 * ■16時にスケジュール実行する。
 * 0 16 * * * opc /usr/bin/php /var/www/batch/set_picker_num.php
 *
 *
 */

$time_start_loop = microtime(true);

#ymd作成
$dt = new DateTime();
$ymd = $dt->format('Ymd');

// records_we_madeテーブルにmst_portalの全サイトNoのレコードを作成しておく。
// 当日、検索内容がない場合でも、TOTAL_CURL_NUMには、前日の数値を入れる
$sql_portal = "SELECT site_no FROM mst_portal";
$stmt = $pdo_medium->query($sql_portal);
$rows = $stmt->fetchAll();

#records_we_madeテーブルに登録
##登録時間は9時間足さずに、UTCの時間で設定する。
##全てのサイト分のレコードを、全ての件数にデフォルトで0を設定してINSERTする
$sql = "INSERT INTO records_we_made (YYYYMMDD, SITE_NO, ENQUEUES_NUM, NEW_CURL_NUM, DELETE_CURL_NUM, TOTAL_CURL_NUM, COUNT_DATETIME)
        VALUES ('".$ymd."', :site_no, 0,0,0,0,SYSDATE)";
$stmt = $pdo_tp->prepare($sql);

foreach($rows as $row) {
    $time_start = microtime(true);

    $target_site_no = $row['site_no'];

    # records_we_madeへのデータの登録
    $stmt->bindParam(':site_no', $target_site_no, PDO::PARAM_INT);
    try{
        $stmt->execute();
    } catch (PDOException $e) {
        $msg = "[ERROR]set_picker_num insert:{$target_site_no}:" . $e->getMessage();
        $log->freeform("php_batch_errors", $msg);
        exit;
    }
    $time_cd = microtime(true) - $time_start;
    $Msg = "exec insert: " . sprintf("%.20f", $time_cd) . 'sec';
    //echo "{$Msg}\n";
    //$log->freeform("set_picker_num", $Msg);
}

#crawlilng_listから件数を取得する
#enqueue数
$sql1 = "SELECT site_no, count(*) cnt_enq FROM crawling_list WHERE yyyymmdd='".$ymd."' GROUP BY site_no";
$stmt = $pdo_medium->query($sql1);
$rows = $stmt->fetchAll();

#records_we_madeテーブルのENQUEUES_NUMにenqueue数をUPDATE
$sql = "UPDATE records_we_made SET ENQUEUES_NUM = :enqueues_num WHERE yyyymmdd='".$ymd."' and site_no = :site_no";
$stmt = $pdo_tp->prepare($sql);

# サイトNo.ごとに件数を取得し、データを登録する
foreach($rows as $row) {
    $time_start = microtime(true);

    $target_site_no = $row['site_no'];
    $cnt_enq = $row['cnt_enq'];

    # records_we_madeへのデータの登録
    $stmt->bindParam(':site_no', $target_site_no, PDO::PARAM_INT);
    $stmt->bindParam(':enqueues_num', $cnt_enq, PDO::PARAM_INT);
    try{
        $stmt->execute();
    } catch (PDOException $e) {
        $msg = "[ERROR]set_picker_num enqueues_num:{$target_site_no}:" . $e->getMessage();
        $log->freeform("php_batch_errors", $msg);
        exit;
    }
    $time_cd = microtime(true) - $time_start;
    $Msg = "exec enqueues_num: " . sprintf("%.20f", $time_cd) . 'sec';
    //echo "{$Msg}\n";
    //$log->freeform("set_picker_num", $Msg);
}

#新規件数
$sql = "SELECT site_no, count(*) cnt_new FROM crawling_list WHERE to_char(created, 'YYYYMMDD') = '".$ymd."' GROUP BY site_no";
$stmt = $pdo_medium->query($sql);
$rows = $stmt->fetchAll();

#records_we_madeテーブルにUPDATE
$sql = "UPDATE records_we_made SET new_curl_num=:new_curl_num WHERE yyyymmdd='".$ymd."' and site_no=:site_no ";
$stmt = $pdo_tp->prepare($sql);

# サイトNo.ごとに件数を取得し、データを登録する
foreach($rows as $row) {
    $time_start = microtime(true);

    $target_site_no = $row['site_no'];
    $cnt_new = $row['cnt_new'];

    # records_we_madeテーブルにUPDATE
    $stmt->bindParam(':site_no', $target_site_no, PDO::PARAM_INT);
    $stmt->bindParam(':new_curl_num', $cnt_new, PDO::PARAM_INT);
    try{
        $stmt->execute();
    } catch (PDOException $e) {
        $msg = "[ERROR]set_picker_num new_curl_num:{$target_site_no}:" . $e->getMessage();
        $log->freeform("php_batch_errors", $msg);
        exit;
    }
    $time_cd = microtime(true) - $time_start;
    $Msg = "exec new_curl_num: " . sprintf("%.20f", $time_cd) . 'sec';
    //echo "{$Msg}\n";
    //$log->freeform("set_picker_num", $Msg);
}

#削除件数 : yyyymmddが3日前のデータを削除件数として設定する。
//$chk_ymd = $dt->modify('-3 days')->format('Ymd');
$chk_ymd = date("Ymd",strtotime($ymd."-3 day"));
//echo $chk_ymd;
$sql = "SELECT site_no, count(*) cnt_del FROM crawling_list WHERE yyyymmdd='".$chk_ymd."' GROUP BY site_no";
$stmt = $pdo_medium->query($sql);
$rows = $stmt->fetchAll();

#records_we_madeテーブルにUPDATE
$sql = "UPDATE records_we_made SET delete_curl_num=:delete_curl_num WHERE yyyymmdd='".$ymd."' and site_no=:site_no ";
$stmt = $pdo_tp->prepare($sql);

# サイトNo.ごとに件数を取得し、データを登録する
foreach($rows as $row){
    $time_start = microtime(true);

    $target_site_no = $row['site_no'];
    $cnt_del = $row['cnt_del'];

    # records_we_madeへのデータの登録
    $stmt->bindParam(':site_no', $target_site_no, PDO::PARAM_INT);
    $stmt->bindParam(':delete_curl_num', $cnt_del, PDO::PARAM_INT);

    try{
        $stmt->execute();
    } catch (PDOException $e) {
        $msg = "[ERROR]set_picker_num delete_curl_num:{$target_site_no}:" . $e->getMessage();
        $log->freeform("php_batch_errors", $msg);
        exit;
    }
    $time_cd = microtime(true) - $time_start;
    $Msg = "exec delete_curl_num: " . sprintf("%.20f", $time_cd) . 'sec';
    //echo "{$Msg}\n";
    //$log->freeform("set_picker_num", $Msg);
}

#累計値
$sql = "SELECT site_no, count(*) cnt_total FROM crawling_list GROUP BY site_no";
$stmt = $pdo_medium->query($sql);
$rows = $stmt->fetchAll();

#records_we_madeテーブルにUPDATE
$sql = "UPDATE records_we_made SET total_curl_num=:total_curl_num WHERE yyyymmdd='".$ymd."' and site_no=:site_no ";
$stmt = $pdo_tp->prepare($sql);

# サイトNo.ごとに件数を取得し、データを登録する
foreach($rows as $row){
    $time_start = microtime(true);

    $target_site_no = $row['site_no'];
    $cnt_total = $row['cnt_total'];

    # records_we_madeへのデータの登録
    $stmt->bindParam(':site_no', $target_site_no, PDO::PARAM_INT);
    $stmt->bindParam(':total_curl_num', $cnt_total, PDO::PARAM_INT);
    try{
        $stmt->execute();
    } catch (PDOException $e) {
        $msg = "[ERROR]set_picker_num total_curl_num:{$target_site_no}:" . $e->getMessage();
        $log->freeform("php_batch_errors", $msg);
        exit;
    }

    $time_cd = microtime(true) - $time_start;
    $Msg = "exec total_curl_num: " . sprintf("%.20f", $time_cd) . 'sec';
    //echo "{$Msg}\n";
    //$log->freeform("set_picker_num", $Msg);
}

$time_cd = microtime(true) - $time_start_loop;
$Msg = "total time:" . sprintf("%.20f", $time_cd) . 'sec';
//echo "{$Msg}\n";
$log->freeform("set_picker_num", $Msg);

#ログ
//$log->freeform("set_picker_num", '[batch] ' . $row_cnt . '件 -> 各処理待機時間終了');

?>
