<?php

/** 実行ファイルから見たパスで指定 */
require __DIR__.'/../import/config.php';
require __DIR__.'/../import/db_oracle.php';
require __DIR__.'/../import/log.php';

//$db = DB::getInstance();
$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);   //2019/12/13変更
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);   //2019/12/13変更
$log = new log();
/**
■クロール中：status=10のレコードに対して

指定時間の経過でクロール失敗とみなして
再クロール待ち：status=30への変更をする。
合わせてtry_countに+1をする。
■再スクレイプ中：status=11のレコードに対して

指定時間の経過でスクレイプ失敗とみなして
status=320（スクレイプ失敗／要再調査）に変更する。
■再クレンジング中：status=12のレコードに対して

指定時間の経過でクレンジング失敗とみなして
status=330（クレンジング失敗／要再調査）に変更する。
■try_count >= 6のリストをクロール失敗（status=310）に変更する。

■10分おきにスケジュール実行する。
5,15,25,35,45,55 * * * * root /usr/bin/php /var/www/batch/incrawl_reset_ora.php

 */

$elasped_time = 30;  //min

#件数確認
$sql = "SELECT count(*) AS cnt FROM crawling_list WHERE status IN(10,11,12) AND
updated + interval '" . $elasped_time . "' minute < current_timestamp";

//echo "sql:{$sql}、";

$stmt = $pdo_medium->query($sql);  //2019/12/13変更
$row = $stmt->fetch();
$row_cnt = $row['cnt'];
//echo $row_cnt;

#全部0件のときはスルー
if($row_cnt > 0){
	#■クロール中：status=10のレコードに対して
	$time_start = microtime(true);
	$sql = "UPDATE crawling_list SET status=30, try_count=try_count+1, updated=SYSDATE+9/24 WHERE status=10 AND
	updated + interval'" . $elasped_time . "' minute < current_timestamp";
	//echo "クロール中：status=10：{$sql}";
	$stmt = $pdo_medium->query($sql);
	$time = microtime(true) - $time_start;
	$log->freeform("incrawl_reset", 'status=10 to 30: ' . sprintf("%.20f", $time) . '秒');
	//echo "処理時間：status=10：".sprintf("%.20f", $time)."秒";

	#■再スクレイプ中：status=11のレコードに対して
	$time_start = microtime(true);
	$sql = "UPDATE crawling_list SET status=320, updated=SYSDATE+9/24 WHERE status=11 AND
	updated + interval '" . $elasped_time . "' minute < current_timestamp";
	$stmt = $pdo_medium->query($sql);
	$time = microtime(true) - $time_start;
	$log->freeform("incrawl_reset", 'status=11 to 320: ' . sprintf("%.20f", $time) . '秒');
	//echo "処理時間：status=11：".sprintf("%.20f", $time)."秒";
	//echo "再スクレイプ中：status=11：{$sql}";

	#■再クレンジング中：status=12のレコードに対して
	$time_start = microtime(true);
	$sql = "UPDATE crawling_list SET status=330, updated=SYSDATE+9/24 WHERE status=12 AND
	updated + interval '" . $elasped_time . "' minute < current_timestamp";
	$stmt = $pdo_medium->query($sql);
	$time = microtime(true) - $time_start;
	$log->freeform("incrawl_reset", 'status=12 to 330: ' . sprintf("%.20f", $time) . '秒');
	//echo "処理時間：status=12：".sprintf("%.20f", $time)."秒";
	//echo "再クレンジング中：status=12：{$sql}";
}

#■try_count >= 6のリストをクロール失敗（status=310）に変更する。
//まず、更新対象があるかをチェックする。
$time_start = microtime(true);
$sql = "SELECT crawl_id FROM crawling_list WHERE try_count>=6 AND status IN (0,10,30)";
$stmt = $pdo_medium->query($sql);
$rows = $stmt->fetchAll();
$num = count($rows);
//echo "対象件数：{$num}";
$time = microtime(true) - $time_start;
//echo "処理時間:try_count >= 6：".sprintf("%.20f", $time)."秒";

if ($num !=0) {
	$sql = "UPDATE crawling_list SET status=310, updated=SYSDATE+9/24 WHERE crawl_id=:crawl_id";

	for ($i=0; $i<$num; $i++) {
		$row = $rows[$i];
		$row_id = $row['crawl_id'];
//		echo ",対象crawl_id：{$row_id}";

		$stmt = $pdo_medium->prepare($sql);
		$stmt->bindParam(':crawl_id', $row_id, PDO::PARAM_INT);
		$stmt->execute();   // updateSQLの実施
	}
} else {
//	echo "対象レコードなし";
}
$time = microtime(true) - $time_start;

#ログ
$log->freeform("incrawl_reset", 'status -> 310: ' . sprintf("%.20f", $time) . '秒、' . $num .'件');
//echo "処理時間:UPDATE status->310：".sprintf("%.20f", $time)."秒";

$log->freeform("incrawl_reset", 'status=10,11,12 AND elapsed: ' . $row_cnt . '件 -> 各処理待機時間終了');

?>
