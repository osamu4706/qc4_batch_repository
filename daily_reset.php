<?php

/** 実行ファイルから見たパスで指定 */
require __DIR__.'/../import/config.php';
require __DIR__.'/../import/db_oracle.php';
require __DIR__.'/../import/log.php';

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);

/**
picker抽出ターゲット(picker_target)の当日取得件数(daily_num)と
クロール設定＞クロール件数の0リセット

**/

#当日取得数のログを保存

$sql = "UPDATE picker_target SET daily_num=0";
$stmt = $pdo_tp->query($sql);

$sql = "UPDATE crawl_setting SET daily_num=0";
$stmt = $pdo_tp->query($sql);


#ログ

?>
