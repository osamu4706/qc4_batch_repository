<?php

/** 実行ファイルから見たパスで指定 */
require __DIR__.'/../import/config.php';
require __DIR__.'/../import/db_oracle.php';
require __DIR__.'/../import/log.php';

//$db = DB::getInstance();
// UPDATEなどはTPを利用する。
$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
// 全検索や、処理に時間のかかるものはMEDIUMを使用する。
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);
$log = new log();

/**
再クロール待ちのクロールリスト：status=30のレコードに対して
指定時間の経過で未クロール(dequeues排出対象)への変更をする。
※クロール失敗時はstatus=30にして
30分後、1時間後、3時間後、6時間後、12時間後に再クロール(当日分は30:00まで)。
try_count=
1:30min
2:60min
3:180min
4:300min
5:720min
経過したものをstatus=0にする。
 *
■10分おきにスケジュール実行する。
0,10,20,30,40,50 * * * * root /usr/bin/php /var/www/batch/recrawl_set_ora.php
*/

#ymd作成
$dt = new DateTime();
//$ymd = intval($dt->format('Ymd'));
$ymd = $dt->format('Ymd');
if($dt->format('G') < 6){  //6:00までは当日扱い
	$ymd = $ymd - 1;
}

#件数確認
$sql = "SELECT count(*) AS cnt FROM crawling_list WHERE status=30 AND (
try_count=1 AND updated + interval '30' minute < current_timestamp OR
try_count=2 AND updated + interval '60' minute < current_timestamp OR
try_count=3 AND updated + interval '120' minute < current_timestamp OR
try_count=4 AND updated + interval '210' minute < current_timestamp OR
try_count=5 AND updated + interval '300' minute < current_timestamp 
)";
//echo "sql:{$sql}、";

$stmt = $pdo_medium->query($sql);
$row = $stmt->fetch();
$row_cnt = $row['cnt'];

//error_log('status=30: ' . $row_cnt . '件 -> update status=0');

#全部0件のときはスルー
if($row_cnt > 0) {
	$time_start = microtime(true);

	$sql = "UPDATE crawling_list SET status=0, updated=SYSDATE+9/24 WHERE status=30 AND (
try_count=1 AND updated + interval '30' minute < current_timestamp OR
try_count=2 AND updated + interval '60' minute < current_timestamp OR
try_count=3 AND updated + interval '120' minute < current_timestamp OR
try_count=4 AND updated + interval '210' minute < current_timestamp OR
try_count=5 AND updated + interval '300' minute < current_timestamp 
)";

//echo "sql:{$sql}、";
	$stmt = $pdo_tp->query($sql);

	$time = microtime(true) - $time_start;
	#ログ
	$log->freeform("recrawl_set", 'status=10 to 30: ' . sprintf("%.20f", $time) . '秒');
	$log->freeform("recrawl_set", 'status=30: ' .  $row_cnt . '件 -> update status=0');
}


?>
