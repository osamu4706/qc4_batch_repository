<?php

/** 実行ファイルから見たパスで指定 */
require __DIR__.'/../import/config.php';
require __DIR__.'/../import/db_oracle.php';

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);

/**
 *
 * PSAP処理テーブル登録バッチ
 *
 * 10分おきに実行する
 * 0,10,20,30,40,50 * * * * opc /usr/bin/php /var/www/batch/set_psap_data.php
 *
 */

## psap_process_list テーブルに、discord_display_flag が 1 から始まるレコードを登録する
## status は 100（psap_estデータ取得済み） の固定でINSERTする
## screening_flg は 1 の固定でINSERTする

$sql = "INSERT 
INTO psap_process_list( 
    process_id
    , property_title
    , property_address
    , psap_est_id
    , status
    , screening_flg
) 
SELECT
      rownum + (SELECT max(process_id) FROM psap_process_list)
    , title
    , address
    , id 
    , 100
    , 1
FROM
    psap_est 
WHERE
    substr(discord_display_flag, 1, 1) = 1 
    AND reins = 1 
    AND id NOT IN (SELECT psap_est_id FROM psap_process_list) 
    AND create_at > trunc(sysdate - INTERVAL '1' DAY)";

$stmt = $pdo_tp->query($sql);

## 更新処理：：status が「200」で、screening_flg が「1」のレコードに対して、以下の条件でデータの更新を行う
# 乖離10％以上で、かつ 差額200万以上、かつ 再査定売買リスクフラグが「5」以外の場合は、screening_flg に「123」、status に「210」を設定
# 上記以外で、乖離10％以上の場合は、screening_flg に「12」、status に「209」を設定
# それ以外の場合は、screening_flg に「1」、status に「208」を設定
$sql = "UPDATE psap_process_list ppl 
SET
	(screening_flg, status) = ( 
		SELECT
			  CASE 
				WHEN ( 
					c.price1 - p.est_sell_unit_price_medium * e.senyu_size
				) / c.price1 < - 0.1 
				AND ( 
					p.est_sell_unit_price_medium * e.senyu_size - c.price1
				) >= 2000000 
				AND p.est_sell_risk_flag <> 5 
					THEN 123 
				WHEN ( 
					c.price1 - p.est_sell_unit_price_medium * e.senyu_size
				) / c.price1 < - 0.1 
					THEN 12 
				ELSE 1
				END screening_flg
			, CASE 
				WHEN ( 
					c.price1 - p.est_sell_unit_price_medium * e.senyu_size
				) / c.price1 < - 0.1 
				AND ( 
					p.est_sell_unit_price_medium * e.senyu_size - c.price1
				) >= 2000000 
				AND p.est_sell_risk_flag <> 5 
					THEN 210 
				WHEN ( 
					c.price1 - p.est_sell_unit_price_medium * e.senyu_size
				) / c.price1 < - 0.1 
					THEN 209
				ELSE 208
				END status
			
		FROM
			psap_process_list p JOIN crawl_reins_stock_sell_mansion c 
				ON p.psap_est_id = c.id JOIN psap_est e 
				ON p.psap_est_id = e.id 
				AND e.reins = 1 
		WHERE
			p.process_id = ppl.process_id
	) 
WHERE
	status = 200 
	and screening_flg = 1";

$stmt = $pdo_tp->query($sql);

?>
