<?php

require '/var/qc4.propre.com/import/dbclass.php';
require "/var/qc4.propre.com/import/log.php";

$db = DB::getInstance();

$offset = 1000;  //この件数単位に処理をする

#rent件数取得
$sql = "SELECT count(*) AS cnt FROM map_rent";
$stmt = $db->query($sql);
$row = $stmt->fetch();
$cnt_map = $row['cnt'];

#tag_mappingの 1015:ドル建て価格と 1016:ドル建て面積単価を全て削除する
$sql = "DELETE FROM tag_mapping_rent WHERE tag_id IN (1015,1016) LIMIT " . ($offset * 10);
for($i=0; $i<((int)($cnt_map / ($offset * 10)) + 1) * 2; $i++){  //tag_mappingのDELETEはoffsetの10倍単位で消す。1015と1016で最大物件数の2倍
	$stmt = $db->query($sql);
}

$sql = "UPDATE map_rent SET usd_price=0 WHERE usd_price>0 LIMIT " . $offset;
for($i=0; $i<(int)($cnt_map / $offset) + 1; $i++){  //UPDATEでLIMIT句のstartが使えないためusd_price=0に初期化する
	$stmt = $db->query($sql);
}

$sql = "SELECT map.currency_cd, count(*) AS cnt, c.exchange_rate FROM map_rent map INNER JOIN mst_currency c ON map.currency_cd = c.currency_cd GROUP BY currency_cd";
$stmt = $db->query($sql);
$rows = $stmt->fetchAll();

$sql2 = "UPDATE map_rent SET usd_price = price * :exchange_rate, usd_unit_price = price * :exchange_rate / floor_sqft WHERE currency_cd=:currency_cd AND price>0 AND usd_price=0 LIMIT " . $offset;
error_log($sql2);
$stmt2 = $db->prepare($sql2);

foreach($rows as $row){
	#currency_cdごとにまずはmapのusd_XXXをUPDATE
	$currency_cd = $row['currency_cd'];
	$cnt = $row['cnt'];
	$exchange_rate = $row['exchange_rate'];
	for($i=0; $i<(int)($cnt / $offset) + 1; $i++){
		$start = $i * $offset;
		$stmt2->bindParam(':exchange_rate', $exchange_rate, PDO::PARAM_STR);
		$stmt2->bindParam(':currency_cd', $currency_cd, PDO::PARAM_STR);
		$stmt2->execute();
	}
}

$sql3 = "INSERT INTO tag_mapping_rent SELECT 1015,usd_price,publish_id,map.meshcode1,map.meshcode2,map.meshcode3,map.meshcode6 FROM map_rent map WHERE usd_price > 0 ORDER BY publish_id LIMIT :start, " . $offset;
$stmt3 = $db->prepare($sql3);
error_log($sql3);

$sql4 = "INSERT INTO tag_mapping_rent SELECT 1016,floor(usd_unit_price * 100),publish_id,map.meshcode1,map.meshcode2,map.meshcode3,map.meshcode6 FROM map_rent map WHERE usd_unit_price > 0 ORDER BY publish_id LIMIT :start, " . $offset;
$stmt4 = $db->prepare($sql4);
error_log($sql4);

for($i=0; $i<(int)($cnt_map / $offset) + 1; $i++){
	#tag_mapping INSERT
	$start = $i * $offset;
	$stmt3->bindParam(':start', $start, PDO::PARAM_INT);
	$stmt3->execute();
	$stmt4->bindParam(':start', $start, PDO::PARAM_INT);
	$stmt4->execute();
}
#ログ

?>
