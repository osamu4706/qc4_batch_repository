<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/qc4.propre.com/import/config.php";
require '/var/qc4.propre.com/import/dbclass.php';
require "/var/qc4.propre.com/import/log.php";

$db = DB::getInstance();
$log = new log();

$offset = 10000;  //この件数単位にtag_mappingのDELETE/INSERT処理をする
/********
以下の件数がカウント100,000件以内ずつになるように
meshcode3で分割して処理をすること！
SELECT map.currency_cd, c.exchange_rate, count(*) AS cnt FROM map_sell map INNER JOIN mst_currency c ON map.currency_cd = c.currency_cd GROUP BY currency_cd;
SELECT map.currency_cd, c.exchange_rate, count(*) AS cnt FROM map_rent map INNER JOIN mst_currency c ON map.currency_cd = c.currency_cd GROUP BY currency_cd;
*********/
$meshdiv_sell = array();
$meshdiv_sell['CAD'] = array(0,9999999999);
$meshdiv_sell['JPY'] = array(0,9999999999);
$meshdiv_sell['MMK'] = array(0,9999999999);
$meshdiv_sell['MYR'] = array(0,9999999999);
$meshdiv_sell['NZD'] = array(0,9999999999);
$meshdiv_sell['PHP'] = array(0,9999999999);
$meshdiv_sell['SGD'] = array(0,9999999999);
$meshdiv_sell['THB'] = array(0,9999999999);
$meshdiv_sell['AUD'] = array(0,6052173784,9999999999);
$meshdiv_sell['EUR'] = array(0,1065033721,1072071334,3057003519,9999999999);
$meshdiv_sell['GBP'] = array(0,3080014562,9999999999);
$meshdiv_sell['IDR'] = array(0,6009062719,6009064765,6010073463,6010127634,6012121568,6016230090,9999999999);
$meshdiv_sell['USD'] = array(0,3045877631,3051841437,3056974151,3061722779,3063880223,4051091639,4069054689,9999999999);
$meshdiv_sell['VND'] = array(0,2016063441,9999999999);

$meshdiv_rent = array();
$meshdiv_rent['CAD'] = array(0,9999999999);
$meshdiv_rent['JPY'] = array(0,9999999999);
$meshdiv_rent['MMK'] = array(0,9999999999);
$meshdiv_rent['MYR'] = array(0,9999999999);
$meshdiv_rent['NZD'] = array(0,9999999999);
$meshdiv_rent['PHP'] = array(0,9999999999);
$meshdiv_rent['SGD'] = array(0,9999999999);
$meshdiv_rent['THB'] = array(0,9999999999);
$meshdiv_rent['AUD'] = array(0,9999999999);
$meshdiv_rent['EUR'] = array(0,9999999999);
$meshdiv_rent['GBP'] = array(0,9999999999);
$meshdiv_rent['IDR'] = array(0,9999999999);
$meshdiv_rent['USD'] = array(0,9999999999);
$meshdiv_rent['VND'] = array(0,9999999999);

#通貨・レートデータ取得
$sql = "SELECT * FROM mst_currency";
$stmt = $db->query($sql);
$rows_currency = $stmt->fetchAll();


/* SELL処理 */

#sell件数取得
$sql = "SELECT count(*) AS cnt FROM map_sell";
$stmt = $db->query($sql);
$row = $stmt->fetch();
$cnt_map_sell = $row['cnt'];

#tag_mappingの 1015:ドル建て価格と 1016:ドル建て面積単価を全て削除する
$sql = "DELETE FROM tag_mapping_sell WHERE tag_id IN (1015,1016) LIMIT " . $offset;
for($i=0; $i<((int)($cnt_map_sell / $offset) + 1) * 2; $i++){  //1015と1016で最大で物件数の2倍のタグが存在
	$stmt = $db->query($sql);
}

#mapのusd,usd_unit_price更新用
$sql1 = "UPDATE map_sell SET usd_price = price * :exchange_rate WHERE currency_cd=:currency_cd AND meshcode3 > :mesh3_start AND meshcode3 < :mesh3_end AND price > 0 AND price * :exchange_rate < 2100000000";
$stmt1 = $db->prepare($sql1);

$sql2 = "UPDATE map_sell SET usd_unit_price = price * :exchange_rate / floor_sqft WHERE currency_cd=:currency_cd AND meshcode3 > :mesh3_start AND meshcode3 < :mesh3_end AND price > 0 AND floor_sqft > 0";
$stmt2 = $db->prepare($sql2);

#tag_id:1015登録用
$sql3 = "INSERT INTO tag_mapping_sell SELECT 1015,usd_price,publish_id,map.meshcode1,map.meshcode2,map.meshcode3,map.meshcode6 FROM map_sell map WHERE currency_cd=:currency_cd AND meshcode3 > :mesh3_start AND meshcode3 < :mesh3_end AND usd_unit_price > 0";
$stmt3 = $db->prepare($sql3);

#tag_id:1016登録用
$sql4 = "INSERT INTO tag_mapping_sell SELECT 1016,floor(usd_unit_price),publish_id,map.meshcode1,map.meshcode2,map.meshcode3,map.meshcode6 FROM map_sell map WHERE currency_cd=:currency_cd AND meshcode3 > :mesh3_start AND meshcode3 < :mesh3_end AND usd_unit_price > 0";
$stmt4 = $db->prepare($sql4);

foreach($rows_currency as $row_currency){
	#currency_cdごとにまずはmapのusd_XXXをUPDATE
	$currency_cd = $row_currency['currency_cd'];
	$exchange_rate = $row_currency['exchange_rate'];
	$arr_meshdiv = $meshdiv_sell[$currency_cd];
	for($i=0; $i<count($arr_meshdiv) - 1; $i++){
//print($exchange_rate.'/'.$currency_cd.'/'.$mesh3_start.'/'.$mesh3_end);
		$mesh3_start = $arr_meshdiv[$i];
		$mesh3_end = $arr_meshdiv[$i+1];
		$stmt1->bindParam(':exchange_rate', $exchange_rate, PDO::PARAM_STR);
		$stmt1->bindParam(':currency_cd', $currency_cd, PDO::PARAM_STR);
		$stmt1->bindParam(':mesh3_start', $mesh3_start, PDO::PARAM_INT);
		$stmt1->bindParam(':mesh3_end', $mesh3_end, PDO::PARAM_INT);
		$stmt1->execute();
		$stmt2->bindParam(':exchange_rate', $exchange_rate, PDO::PARAM_STR);
		$stmt2->bindParam(':currency_cd', $currency_cd, PDO::PARAM_STR);
		$stmt2->bindParam(':mesh3_start', $mesh3_start, PDO::PARAM_INT);
		$stmt2->bindParam(':mesh3_end', $mesh3_end, PDO::PARAM_INT);
		$stmt2->execute();
		$stmt3->bindParam(':currency_cd', $currency_cd, PDO::PARAM_STR);
		$stmt3->bindParam(':mesh3_start', $mesh3_start, PDO::PARAM_INT);
		$stmt3->bindParam(':mesh3_end', $mesh3_end, PDO::PARAM_INT);
		$stmt3->execute();
		$stmt4->bindParam(':currency_cd', $currency_cd, PDO::PARAM_STR);
		$stmt4->bindParam(':mesh3_start', $mesh3_start, PDO::PARAM_INT);
		$stmt4->bindParam(':mesh3_end', $mesh3_end, PDO::PARAM_INT);
		$stmt4->execute();
	}
}


/* RENT処理 */

#rent件数取得
$sql = "SELECT count(*) AS cnt FROM map_rent";
$stmt = $db->query($sql);
$row = $stmt->fetch();
$cnt_map_rent = $row['cnt'];

#tag_mappingの 1015:ドル建て価格と 1016:ドル建て面積単価を全て削除する
$sql = "DELETE FROM tag_mapping_rent WHERE tag_id IN (1015,1016) LIMIT " . $offset;
for($i=0; $i<((int)($cnt_map_rent / $offset) + 1) * 2; $i++){  //1015と1016で最大で物件数の2倍のタグが存在
	$stmt = $db->query($sql);
}

#mapのusd,usd_unit_price更新用
$sql1 = "UPDATE map_rent SET usd_price = price * :exchange_rate WHERE currency_cd=:currency_cd AND meshcode3 > :mesh3_start AND meshcode3 < :mesh3_end AND price > 0 AND price * :exchange_rate < 2100000000";
$stmt1 = $db->prepare($sql1);

$sql2 = "UPDATE map_rent SET usd_unit_price = price * :exchange_rate / floor_sqft WHERE currency_cd=:currency_cd AND meshcode3 > :mesh3_start AND meshcode3 < :mesh3_end AND price > 0 AND floor_sqft > 0";
$stmt2 = $db->prepare($sql2);

#tag_id:1015登録用
$sql3 = "INSERT INTO tag_mapping_rent SELECT 1015,usd_price,publish_id,map.meshcode1,map.meshcode2,map.meshcode3,map.meshcode6 FROM map_rent map WHERE currency_cd=:currency_cd AND meshcode3 > :mesh3_start AND meshcode3 < :mesh3_end AND usd_price > 0";
$stmt3 = $db->prepare($sql3);

#tag_id:1016登録用
$sql4 = "INSERT INTO tag_mapping_rent SELECT 1016,floor(usd_unit_price),publish_id,map.meshcode1,map.meshcode2,map.meshcode3,map.meshcode6 FROM map_rent map WHERE currency_cd=:currency_cd AND meshcode3 > :mesh3_start AND meshcode3 < :mesh3_end AND usd_unit_price > 0";
$stmt4 = $db->prepare($sql4);

foreach($rows_currency as $row_currency){
	#currency_cdごとにまずはmapのusd_XXXをUPDATE
	$currency_cd = $row_currency['currency_cd'];
	$exchange_rate = $row_currency['exchange_rate'];
	$arr_meshdiv = $meshdiv_rent[$currency_cd];
	for($i=0; $i<count($arr_meshdiv) - 1; $i++){
		$mesh3_start = $arr_meshdiv[$i];
		$mesh3_end = $arr_meshdiv[$i+1];
		$stmt1->bindParam(':exchange_rate', $exchange_rate, PDO::PARAM_STR);
		$stmt1->bindParam(':currency_cd', $currency_cd, PDO::PARAM_STR);
		$stmt1->bindParam(':mesh3_start', $mesh3_start, PDO::PARAM_INT);
		$stmt1->bindParam(':mesh3_end', $mesh3_end, PDO::PARAM_INT);
		$stmt1->execute();
		$stmt2->bindParam(':exchange_rate', $exchange_rate, PDO::PARAM_STR);
		$stmt2->bindParam(':currency_cd', $currency_cd, PDO::PARAM_STR);
		$stmt2->bindParam(':mesh3_start', $mesh3_start, PDO::PARAM_INT);
		$stmt2->bindParam(':mesh3_end', $mesh3_end, PDO::PARAM_INT);
		$stmt2->execute();
		$stmt3->bindParam(':currency_cd', $currency_cd, PDO::PARAM_STR);
		$stmt3->bindParam(':mesh3_start', $mesh3_start, PDO::PARAM_INT);
		$stmt3->bindParam(':mesh3_end', $mesh3_end, PDO::PARAM_INT);
		$stmt3->execute();
		$stmt4->bindParam(':currency_cd', $currency_cd, PDO::PARAM_STR);
		$stmt4->bindParam(':mesh3_start', $mesh3_start, PDO::PARAM_INT);
		$stmt4->bindParam(':mesh3_end', $mesh3_end, PDO::PARAM_INT);
		$stmt4->execute();
	}
}

#ログ
error_log('[Batch] crawl_daily_finish.php実行');

?>
