<?php

/** 実行環境から見るので絶対パスで指定 */
require '/var/qc4.propre.com/import/dbclass.php';
require "/var/qc4.propre.com/import/log.php";

$db = DB::getInstance();

$offset = 10000;  //この件数単位にtag_mappingのINSERT処理をする

#sell件数取得
$sql = "SELECT count(*) AS cnt FROM map_sell";
$stmt = $db->query($sql);
$row = $stmt->fetch();
$cnt_map_sell = $row['cnt'];

#tag_mappingの 1015:ドル建て価格と 1016:ドル建て面積単価を全て削除する
$sql = "DELETE FROM tag_mapping_sell WHERE tag_id IN (1015,1016) LIMIT " . $offset;
for($i=0; $i<((int)($cnt_map_sell / $offset) + 1) * 2; $i++){  //1015と1016で最大物件数の2倍
	$stmt = $db->query($sql);
}

$meshdiv = array();
$meshdiv['CAD'] = array(0,9999999999);
$meshdiv['JPY'] = array(0,9999999999);
$meshdiv['MMK'] = array(0,9999999999);
$meshdiv['MYR'] = array(0,9999999999);
$meshdiv['NZD'] = array(0,9999999999);
$meshdiv['PHP'] = array(0,9999999999);
$meshdiv['SGD'] = array(0,9999999999);
$meshdiv['THB'] = array(0,9999999999);
$meshdiv['AUD'] = array(0,6052173784,9999999999);
$meshdiv['EUR'] = array(0,1065033721,1072071334,3057003519,9999999999);
$meshdiv['GBP'] = array(0,3080014562,9999999999);
$meshdiv['IDR'] = array(0,6009062719,6009064765,6010073463,6010127634,6012121568,6016230090,9999999999);
$meshdiv['USD'] = array(0,3045877631,3051841437,3056974151,3061722779,3063880223,4051091639,4069054689,9999999999);
$meshdiv['VND'] = array(0,2016063441,9999999999);

$sql1 = "INSERT INTO tag_mapping_sell SELECT 1015,usd_price,publish_id,map.meshcode1,map.meshcode2,map.meshcode3,map.meshcode6 FROM map_sell map WHERE currency_cd=:currency_cd AND meshcode3 > :mesh3_start AND meshcode3 < :mesh3_end";
$stmt1 = $db->prepare($sql1);

$sql2 = "INSERT INTO tag_mapping_sell SELECT 1016,floor(usd_unit_price * 100),publish_id,map.meshcode1,map.meshcode2,map.meshcode3,map.meshcode6 FROM map_sell map WHERE currency_cd=:currency_cd AND meshcode3 > :mesh3_start AND meshcode3 < :mesh3_end";
$stmt2 = $db->prepare($sql2);

foreach($meshdiv as $key => $value){
	#currency_cdごとにタグ付けする
	$currency_cd = $key;
	$arr_meshdiv = $value;
	for($i=0; $i<count($arr_meshdiv) - 1; $i++){
		$mesh3_start = $arr_meshdiv[$i];
		$mesh3_end = $arr_meshdiv[$i+1];
		$stmt1->bindParam(':currency_cd', $currency_cd, PDO::PARAM_STR);
		$stmt1->bindParam(':mesh3_start', $mesh3_start, PDO::PARAM_INT);
		$stmt1->bindParam(':mesh3_end', $mesh3_end, PDO::PARAM_INT);
		$stmt1->execute();
		$stmt2->bindParam(':currency_cd', $currency_cd, PDO::PARAM_STR);
		$stmt2->bindParam(':mesh3_start', $mesh3_start, PDO::PARAM_INT);
		$stmt2->bindParam(':mesh3_end', $mesh3_end, PDO::PARAM_INT);
		$stmt2->execute();
	}
}

?>
