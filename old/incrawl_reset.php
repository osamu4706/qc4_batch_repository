<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/qc4.propre.com/import/config.php";
require '/var/qc4.propre.com/import/dbclass.php';
require "/var/qc4.propre.com/import/log.php";

$db = DB::getInstance();

/**
■クロール中：status=10のレコードに対して、
指定時間の経過でクロール失敗とみなして、未クロール(dequeues排出対象)：status=0への変更をする。
合わせてtry_countに+1をする。
■再スクレイプ中：status=11のレコードに対して、
指定時間の経過でスクレイプ失敗とみなして、status=320（スクレイプ失敗／要再調査）に変更する。
■再クレンジング中：status=12のレコードに対して、
指定時間の経過でクレンジング失敗とみなして、status=330（クレンジング失敗／要再調査）に変更する。
■try_count >= 6のリストをクロール失敗（status=310）に変更する。
*/

$target_limit=1000;

$elasped_time = 30;  //min

#ymd作成
$dt = new DateTime();
$ymd = $dt->format('Ymd');
if($dt->format('G') < 6){  //6:00までは当日扱い(前日としてセット)
	$ymd = $ymd - 1;
}

#件数確認
$sql = "SELECT count(*) AS cnt FROM crawling_list WHERE status IN(10,11,12) AND
updated + interval + " . $elasped_time . " minute < current_timestamp AND 
(DATE_FORMAT('" . ($ymd + 1) . "060000','%Y/%m/%d %H:%i:%s') > SYSDATE)";
$stmt = $db->query($sql);
$row = $stmt->fetch();

#■クロール中：status=10のレコードに対して、、、
$sql = "UPDATE crawling_list SET status=0, try_count=try_count+1, updated=SYSDATE WHERE status=10 AND 
updated + interval + " . $elasped_time . " minute < current_timestamp AND 
(DATE_FORMAT('" . ($ymd + 1) . "060000','%Y/%m/%d %H:%i:%s') > SYSDATE) LIMIT " . $target_limit;
$stmt = $db->query($sql);

#■再スクレイプ中：status=11のレコードに対して、、、
$sql = "UPDATE crawling_list SET status=320, updated=SYSDATE WHERE status=11 AND 
updated + interval + " . $elasped_time . " minute < current_timestamp AND 
(DATE_FORMAT('" . ($ymd + 1) . "060000','%Y/%m/%d %H:%i:%s') > SYSDATE) LIMIT " . $target_limit;
$stmt = $db->query($sql);

#■再クレンジング中：status=12のレコードに対して、、、
$sql = "UPDATE crawling_list SET status=330, updated=SYSDATE WHERE status=12 AND 
updated + interval + " . $elasped_time . " minute < current_timestamp AND 
(DATE_FORMAT('" . ($ymd + 1)  . "060000','%Y/%m/%d %H:%i:%s') > SYSDATE) LIMIT " . $target_limit;
$stmt = $db->query($sql);

#■try_count >= 6のリストをクロール失敗（status=310）に変更する。
$sql = "UPDATE crawling_list SET status=310, updated=SYSDATE WHERE try_count>=6 LIMIT " . $target_limit;
$stmt = $db->query($sql);

#ログ
$log->freeform("incrawl_reset", 'status=10,11,12 AND elapsed: ' . $row['cnt'] . '件 -> 各処理待機時間終了');

?>
