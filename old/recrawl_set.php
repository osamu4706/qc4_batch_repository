<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/qc4.propre.com/import/config.php";
require '/var/qc4.propre.com/import/dbclass.php';
require "/var/qc4.propre.com/import/log.php";

$db = DB::getInstance();

/**
再クロール待ちのクロールリスト：status=30のレコードに対して、
指定時間の経過で未クロール(dequeues排出対象)への変更をする。
※クロール失敗時はstatus=30にして、30分後、1時間後、3時間後、6時間後、12時間後に再クロール(当日分は30:00まで)。
try_count=
1:30min
2:60min
3:180min
4:300min
5:720min
経過したものをstatus=0にする。
*/

$test_mode = true;
#テスト用テーブル設定
$table_prefix = '';
if($test_mode){
#	$table_prefix = 'propre_development.';
	$table_prefix = '';
}


$target_limit=10000;

#ymd作成
$dt = new DateTime();
$ymd = intval($dt->format('Ymd'));
if($dt->format('G') < 6){  //6:00までは当日扱い
	$ymd = $ymd - 1;
}

#件数確認
$sql = "SELECT count(*) AS cnt FROM " . $table_prefix . "crawling_list WHERE status=30 AND (
try_count=1 AND updated + interval + 30 minute < current_timestamp OR
try_count=2 AND updated + interval + 60 minute < current_timestamp OR
try_count=3 AND updated + interval + 180 minute < current_timestamp OR
try_count=4 AND updated + interval + 300 minute < current_timestamp OR
try_count=5 AND updated + interval + 720 minute < current_timestamp 
) AND (DATE_FORMAT('" . strval($ymd + 1) . "060000','%Y/%m/%d %H:%i:%s') > SYSDATE)";
$stmt = $db->query($sql);
$row = $stmt->fetch();
error_log('status=30: ' . $row['cnt'] . '件 -> update status=0');

$sql = "UPDATE " . $table_prefix . "crawling_list SET status=0 WHERE status=30 AND (
try_count=1 AND updated + interval + 30 minute < current_timestamp OR
try_count=2 AND updated + interval + 60 minute < current_timestamp OR
try_count=3 AND updated + interval + 180 minute < current_timestamp OR
try_count=4 AND updated + interval + 300 minute < current_timestamp OR
try_count=5 AND updated + interval + 720 minute < current_timestamp 
) AND (DATE_FORMAT('" . strval($ymd +1) . "060000','%Y/%m/%d %H:%i:%s') > SYSDATE) LIMIT " . $target_limit;

$stmt = $db->query($sql);

#ログ

?>
