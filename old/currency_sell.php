<?php

require '/var/qc4.propre.com/import/dbclass.php';
require "/var/qc4.propre.com/import/log.php";

$db = DB::getInstance();

$offset = 1000;  //この件数単位に処理をする


#sell件数取得
$sql = "SELECT count(*) AS cnt FROM map_sell";
$stmt = $db->query($sql);
$row = $stmt->fetch();
$cnt_map_sell = $row['cnt'];

#tag_mappingの 1015:ドル建て価格と 1016:ドル建て面積単価を全て削除する
/*
$sql = "DELETE FROM tag_mapping_sell WHERE tag_id IN (1015,1016) LIMIT " . ($offset * 10);
for($i=0; $i<((int)($cnt_map / ($offset * 10)) + 1) * 2; $i++){  //tag_mappingのDELETEはoffsetの10倍単位で消す。1015と1016で最大物件数の2倍
	$stmt = $db->query($sql);
}
*/


$sql = "SELECT * FROM mst_currency";
$stmt = $db->query($sql);
$rows = $stmt->fetchAll();

$sql = "UPDATE map_sell SET usd_price = price * :exchange_rate, usd_unit_price = price * :exchange_rate / floor_sqft WHERE currency_cd=:currency_cd AND meshcode3 > :mesh3_start AND meshcode3 < :mesh3_end";
$stmt = $db->prepare($sql);

/********
以下の件数がカウント100,000件以内ずつになるように
meshcode3で分割して処理をすること！
"SELECT map.currency_cd, c.exchange_rate, count(*) AS cnt FROM map_rent map INNER JOIN mst_currency c ON map.currency_cd = c.currency_cd GROUP BY currency_cd";
*********/

$meshdiv = array();
$meshdiv['CAD'] = array(0,9999999999);
$meshdiv['JPY'] = array(0,9999999999);
$meshdiv['MMK'] = array(0,9999999999);
$meshdiv['MYR'] = array(0,9999999999);
$meshdiv['NZD'] = array(0,9999999999);
$meshdiv['PHP'] = array(0,9999999999);
$meshdiv['SGD'] = array(0,9999999999);
$meshdiv['THB'] = array(0,9999999999);
$meshdiv['AUD'] = array(0,6052173784,9999999999);
$meshdiv['EUR'] = array(0,1065033721,1072071334,3057003519,9999999999);
$meshdiv['GBP'] = array(0,3080014562,9999999999);
$meshdiv['IDR'] = array(0,6009062719,6009064765,6010073463,6010127634,6012121568,6016230090,9999999999);
$meshdiv['USD'] = array(0,3045877631,3051841437,3056974151,3061722779,3063880223,4051091639,4069054689,9999999999);
$meshdiv['VND'] = array(0,2016063441,9999999999);

foreach($rows as $row){
	#currency_cdごとにまずはmapのusd_XXXをUPDATE
	$currency_cd = $row['currency_cd'];
	$exchange_rate = $row['exchange_rate'];
	$arr_meshdiv = $meshdiv[$currency_cd];
	for($i=0; $i<count($arr_meshdiv) - 1; $i++){
		$mesh3_start = $arr_meshdiv[$i];
		$mesh3_end = $arr_meshdiv[$i+1];
		$stmt->bindParam(':exchange_rate', $exchange_rate, PDO::PARAM_STR);
		$stmt->bindParam(':currency_cd', $currency_cd, PDO::PARAM_STR);
		$stmt->bindParam(':mesh3_start', $mesh3_start, PDO::PARAM_INT);
		$stmt->bindParam(':mesh3_end', $mesh3_end, PDO::PARAM_INT);
		$stmt->execute();
	}
}

?>
