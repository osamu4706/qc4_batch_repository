<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/qc4.propre.com/import/config.php";
require '/var/qc4.propre.com/import/dbclass.php';
require "/var/qc4.propre.com/import/log.php";

$db = DB::getInstance();
$log = new log();

//スクレイプデータの紐付けを修正する
//0,100以外の全レコードからスクレイプデータ（s3/scraped_data）をチェック、ステータス再付番する

$site_no=8;
$limit_num=100;

#$sql = "SELECT count(*) AS cnt FROM crawling_list WHERE site_no=:site_no AND yyyymmdd>=20190807 AND status < 3000";
#全件対象に変更
$sql = "SELECT count(*) AS cnt FROM crawling_list WHERE site_no=:site_no AND status < 3000 AND status<>100 AND status<>0";
$stmt = $db->prepare($sql);
$stmt->bindParam(':site_no', $site_no, PDO::PARAM_INT);
$stmt->execute();
$row = $stmt->fetch();
$cnt = $row['cnt'];

#$sql = "SELECT crawl_id,recrawl_count,status FROM crawling_list WHERE site_no=:site_no AND yyyymmdd>=20190807 AND status < 3000 LIMIT :limit_num";
$sql = "SELECT crawl_id,recrawl_count,status FROM crawling_list WHERE site_no=:site_no AND status < 3000 AND status<>100 AND status<>0 LIMIT :limit_num";
$stmt = $db->prepare($sql);

$sql2 = "SELECT changed_num,s3_path,scraped_data FROM scrape WHERE crawl_id = :crawl_id ORDER BY changed_num DESC LIMIT 1";
$stmt2 = $db->prepare($sql2);

$sql3 = "UPDATE crawling_list SET recrawl_count=:recrawl_count,status=:status WHERE crawl_id = :crawl_id";
$stmt3 = $db->prepare($sql3);

for ($i=0; $i<((int)$cnt -1) / $limit_num + 1; $i++){
	$stmt->bindParam(':site_no', $site_no, PDO::PARAM_INT);
	$stmt->bindParam(':limit_num', $limit_num, PDO::PARAM_INT);
	$stmt->execute();
	$rows = $stmt->fetchAll();
	foreach($rows as $row){
		$crawl_id = $row['crawl_id'];
		$recrawl_count = $row['recrawl_count'];
		$status = $row['status'];
		$stmt2->bindParam(':crawl_id', $crawl_id, PDO::PARAM_INT);
		$stmt2->execute();
		$row2 = $stmt2->fetch();
		$changed_num = $row2['changed_num'];
		$s3_path = $row2['s3_path'];
		$scraped_data = $row2['scraped_data'];
		$new_status=3330;
		$new_recrawl_count=$changed_num;
		if($new_recrawl_count == null){
			$new_recrawl_count=0;
		}
		if(strlen($scraped_data) > 10){
			$new_status=3330;
		}else if(strlen($s3_path) > 10){
			$new_status=3320;
		}else{
			$new_status=3321;
		}
		$stmt3->bindParam(':crawl_id', $crawl_id, PDO::PARAM_INT);
		$stmt3->bindParam(':recrawl_count', $new_recrawl_count, PDO::PARAM_INT);
		$stmt3->bindParam(':status', $new_status, PDO::PARAM_INT);
		$stmt3->execute();
		$log->freeform("correct_recrawlcount_8", $crawl_id . ':' . $new_recrawl_count . '/' . $status . '->' . $new_status);
	}
	if($i % 100 == 0){
		echo ($i * $limit_num) . '/' . $cnt . ' ';
	}
}

?>
