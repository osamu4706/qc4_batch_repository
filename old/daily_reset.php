<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/qc4.propre.com/import/config.php";
require '/var/qc4.propre.com/import/dbclass.php';
require "/var/qc4.propre.com/import/log.php";

$db = DB::getInstance();

/**
picker抽出ターゲット(picker_target)の当日取得件数(daily_num)と
クロール設定＞クロール件数の0リセット

**/

#当日取得数のログを保存

$sql = "UPDATE picker_target SET daily_num=0";
$stmt = $db->query($sql);

$sql = "UPDATE crawl_setting SET daily_num=0";
$stmt = $db->query($sql);


#ログ

?>
