<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/www/import/config.php";
require '/var/www/import/db_oracle.php';
require "/var/www/import/tools.php";
require "/var/www/import/log.php";

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);
$log = new log();

/*
update crawling_list set status=3330 where site_no=1 and status=100 and rownum<1000;
*/


//$site_no=7;
$site_no = '8,10';
$limit_num=10000;  //
$cnt50 = 10000;
$cnt100 = 9850000;
$cnt202 = 0;
$cnt330 = 3830000;
$cnt1330 = 10000;
$cnt2330 = 10000;

$sql50 = "update /*+INDEX(a CRAWLING_LIST_I01)*/ crawling_list a set status=3330 where site_no in(" . $site_no . ") and status=50 and rownum<=" . $limit_num;
$sql100 = "update /*+INDEX(a CRAWLING_LIST_I01)*/ crawling_list a set status=3330 where site_no in(" . $site_no . ") and status=100 and rownum<=" . $limit_num;
$sql202 = "update /*+INDEX(a CRAWLING_LIST_I01)*/ crawling_list a set status=3330 where site_no in(" . $site_no . ") and status=202 and rownum<=" . $limit_num;
$sql330 = "update /*+INDEX(a CRAWLING_LIST_I01)*/ crawling_list a set status=3330 where site_no in(" . $site_no . ") and status=330 and rownum<=" . $limit_num;
$sql1330 = "update /*+INDEX(a CRAWLING_LIST_I01)*/ crawling_list a set status=3330 where site_no in(" . $site_no . ") and status=1330 and rownum<=" . $limit_num;
$sql2330 = "update /*+INDEX(a CRAWLING_LIST_I01)*/ crawling_list a set status=3330 where site_no in(" . $site_no . ") and status=2330 and rownum<=" . $limit_num;

echo '50:'.$cnt50.' ';
for($i=0; $i<$cnt50/$limit_num + 1; $i++){
	$stmt = $pdo_tp->query($sql50);
	if($i % 10 ==0){
		echo $i . ' ';
	}
}
echo '50:end ';
echo '100:'.$cnt100.' ';
for($i=0; $i<$cnt100/$limit_num + 1; $i++){
	$stmt = $pdo_tp->query($sql100);
	if($i % 10 ==0){
		echo $i . ' ';
	}
}
echo '100:end ';
echo '202:'.$cnt202.' ';
for($i=0; $i<$cnt202/$limit_num + 1; $i++){
	$stmt = $pdo_tp->query($sql202);
	if($i % 10 ==0){
		echo $i . ' ';
	}
}
echo '202:end ';
echo '330:'.$cnt330.' ';
for($i=0; $i<$cnt330/$limit_num + 1; $i++){
	$stmt = $pdo_tp->query($sql330);
	if($i % 10 ==0){
		echo $i . ' ';
	}
}
echo '330:end ';

echo '1330:'.$cnt1330.' ';
for($i=0; $i<$cnt1330/$limit_num + 1; $i++){
	$stmt = $pdo_tp->query($sql1330);
	if($i % 10 ==0){
		echo $i . ' ';
	}
}
echo '1330:end ';

echo '2330:'.$cnt2330.' ';
for($i=0; $i<$cnt2330/$limit_num + 1; $i++){
	$stmt = $pdo_tp->query($sql2330);
	if($i % 10 ==0){
		echo $i . ' ';
	}
}
echo '2330:end ';

?>
