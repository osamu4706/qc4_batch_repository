<?php

/** 実行環境から見るので絶対パスで指定 */
require '/var/qc4.propre.com/import/dbclass.php';
require "/var/qc4.propre.com/import/log.php";

####日付と抽出範囲チェック####

$cnt1=0;
$cnt2=0;
$cnt3=0;
$cnt4=0;

$db = DB::getInstance();

$sql = "SELECT * FROM crawling_list WHERE site_no=3 and created>'2019-08-01 0:00:00'";  //新を取得
$stmt = $db->query($sql);
$rows = $stmt->fetchAll();

$sql2 = 'SELECT * FROM crawling_list WHERE curl=:curl AND crawl_id<>:crawl_id ';  //旧を取得
$stmt2 = $db->prepare($sql2);

$sql2_1 = 'SELECT crawl_id,changed_num FROM scrape WHERE crawl_id=:crawl_id ORDER BY changed_num ASC';  //新を全て取得
$stmt2_1 = $db->prepare($sql2_1);

$sql3 = 'UPDATE crawling_list SET yyyymmdd=:yyyymmdd,status=330,lastpublished=TO_DATE(:lastpublished, '.DB_DATE_FORMAT.'),searchorder=searchorder+:searchorder WHERE crawl_id=:crawl_id ';
$stmt3 = $db->prepare($sql3);

$sql4 = 'DELETE FROM crawling_list WHERE crawl_id=:crawl_id ';
$stmt4 = $db->prepare($sql4);

$sql5 = 'DELETE FROM scrape WHERE crawl_id=:crawl_id ';
$stmt5 = $db->prepare($sql5);

$sql6 = 'UPDATE crawling_list SET yyyymmdd=:yyyymmdd,status=330,recrawl_count=:new_recrawl_count,lastpublished=TO_DATE(:lastpublished, '.DB_DATE_FORMAT.'),searchorder=searchorder+:searchorder WHERE crawl_id=:crawl_id ';
$stmt6 = $db->prepare($sql6);

$sql7 = 'UPDATE scrape SET crawl_id=:crawl_id,changed_num=:changed_num WHERE crawl_id=:crawl_id_del AND changed_num=:target_changed_num ';
$stmt7 = $db->prepare($sql7);

foreach($rows as $row){
	$cnt1++;
	$stmt2->bindParam(':curl', $row['curl'], PDO::PARAM_STR);
	$stmt2->bindParam(':crawl_id', $row['crawl_id'], PDO::PARAM_INT);
	$stmt2->execute();
	$row2 = $stmt2->fetch();
	$yyyymmdd = $row['yyyymmdd'];  //新
	if($row2 != null){
		$stmt2_1->bindParam(':crawl_id', $row['crawl_id'], PDO::PARAM_INT);  //新
		$stmt2_1->execute();
		$rows2_1 = $stmt2_1->fetchAll();
		foreach($rows2_1 as $row2_1){
			$target_changed_num = $row2_1['changed_num'];
			$cnt2++;
			if($row['matching1'] == $row2['matching1']){ //単純日付更新
				$cnt3++;
				$stmt3->bindParam(':crawl_id', $row2['crawl_id'], PDO::PARAM_INT);  //旧
				$stmt3->bindParam(':lastpublished', $row2['lastpublished'], PDO::PARAM_STR);
				$stmt3->bindParam(':searchorder', $row2['searchorder'], PDO::PARAM_STR);
				$stmt3->bindParam(':yyyymmdd', $yyyymmdd, PDO::PARAM_INT);
				$stmt3->execute();
				$stmt4->bindParam(':crawl_id', $row['crawl_id'], PDO::PARAM_INT);  //新
				$stmt4->execute();
				$stmt5->bindParam(':crawl_id', $row['crawl_id'], PDO::PARAM_INT);  //新
				$stmt5->execute();
			}else{ //更新あり
				$cnt4++;
				$new_recrawl_count=(int)$row2['recrawl_count'] + 1;
				$new_changed_num = $new_recrawl_count + $target_changed_num;
				$stmt6->bindParam(':crawl_id', $row2['crawl_id'], PDO::PARAM_INT);  //旧
				$stmt6->bindParam(':lastpublished', $row2['lastpublished'], PDO::PARAM_STR);
				$stmt6->bindParam(':searchorder', $row2['searchorder'], PDO::PARAM_STR);
				$stmt6->bindParam(':new_recrawl_count', $new_recrawl_count , PDO::PARAM_INT);
				$stmt6->bindParam(':yyyymmdd', $yyyymmdd, PDO::PARAM_INT);
				$stmt6->execute();
				$stmt7->bindParam(':crawl_id', $row2['crawl_id'], PDO::PARAM_INT);  //旧
				$stmt7->bindParam(':changed_num', $new_changed_num, PDO::PARAM_INT);
				$stmt7->bindParam(':crawl_id_del', $row['crawl_id'], PDO::PARAM_INT);  //新
				$stmt7->bindParam(':target_changed_num', $target_changed_num, PDO::PARAM_INT);
				$stmt7->execute();
				$stmt4->bindParam(':crawl_id', $row['crawl_id'], PDO::PARAM_INT);  //新
				$stmt4->execute();
			}
		}
	}
}
print 'processing:' . $cnt1 . "\n";
print 'duplicate_hit:' . $cnt2 . "\n";
print 'stay:' . $cnt3 . "\n";
print 'change:' . $cnt4 . "\n";

#ログ

?>
