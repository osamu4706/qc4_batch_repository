<?php

/** 実行ファイルから見たパスで指定 */
require '/var/www/import/config.php';
require '/var/www/import/db_oracle.php';
require '/var/www/import/log.php';
require '/var/www/import/tools.php';

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);
//$pdo_high = DB::getPdo(DB_ORA_TNS_HIGH);
$dt = new DateTimeImmutable();

$log = new log();

/*
 *
 * 掲載終了処理バッチ
 *  publish_idのリストから終了処理をする
 *  publishに掲載終了セット
 *  同一物件crawling_listに掲載終了セット
 *  マップ／マップ用タグマッピング(未実装)から掲載終了の物件情報を削除、
 *
*/

//$country_cd=392; //index使用のため国コード指定必須
$country_cd=104;
$listing_type='rent'; //sell/rentどちらかのみ
$target_ymd=20201127; //これ以前をcloseとするymd
$log_suffix='_104rent_20201127'; //ログファイルのサフィックス
$limit_num=1000;
$break_num=100000000;
$loop_max = $break_num / $limit_num;
//$loop_max = 1;

$loop_num=0;
//$loop_num=22907;  //中断、途中からリスタート

$log->freeform('publish_close_by_id' . $log_suffix, 'START');

$sql = "SELECT publish_id FROM tmp_closetarget_" . $listing_type . "_" . $country_cd . " WHERE id BETWEEN :start_id AND :end_id ORDER BY id";
$stmt = $pdo_tp->prepare($sql);

$sql_get_publish_closed_rent = "SELECT status_flg,TO_CHAR(start_datetime," . DB_DATE_FORMAT . ") AS start_datetime FROM publish_rent WHERE publish_id=:publish_id";
$stmt_get_publish_closed_rent = $pdo_tp->prepare($sql_get_publish_closed_rent);
$sql_get_publish_closed_sell = "SELECT status_flg,TO_CHAR(start_datetime," . DB_DATE_FORMAT . ") AS start_datetime FROM publish_sell WHERE publish_id=:publish_id";
$stmt_get_publish_closed_sell = $pdo_tp->prepare($sql_get_publish_closed_sell);


while($loop_num < $loop_max){
	$loop_num ++;
	$dt_loop_1 = new DateTimeImmutable();

	$start_id = ($loop_num - 1) * $limit_num + 1;
	$end_id = $loop_num * $limit_num;
//$start_id=582001;
//$end_id =583000;
	$stmt->bindParam(':start_id', $start_id, PDO::PARAM_INT);
	$stmt->bindParam(':end_id', $end_id, PDO::PARAM_INT);
	$stmt->execute();
	$rows = $stmt->fetchAll();
	if(count($rows) == 0){
		exit;
	}

	foreach($rows as $row){
		$target_publish_id = (int)$row['publish_id'];
$log->freeform('publish_close_by_id' . $log_suffix, 'target_publish_id：'.$target_publish_id);
		$close_num = 0;
		$crawl_ids_ymd = array();  //min(yyyymmdd)と同一
		$crawl_ids_already = array();  //yyyymmddより大きく既に終了(<=$target_ymd)
		$crawl_ids_notyet = array();  //yyyymmddより大きい(まだ終了対象ではない)
		$same_crawl_ids = getSameCrawlIds($listing_type, $target_publish_id, $country_cd); //[crawl_id, yyyymmdd, created]]の配列(recrawl_count=changed_numとなっているもののみ。yyyymmdd昇順が保証されている)
//$log->freeform('_test', $target_publish_id . ':' .print_r($same_crawl_ids, true));
//echo $target_publish_id;
		$close_yyyymmdd = $same_crawl_ids[0][1]; //$target_crawl_idではなく同一物件の最小yyyymmddを格納
		$duplicate_num = count($same_crawl_ids);
		$publish_closed = getPublishClosed($listing_type, $target_publish_id);
		$flg_closed = $publish_closed[0];
		$min_start_datetime = $publish_closed[1];
		$bool_min_start_datetime_changed = false;
		#同一対象のyyyymmddにより分類
		if($duplicate_num == 0){  //元になるクロール情報がない(ゾンビデータ)場合、掲載情報を削除する ※Dailyバッチでは行っていない
//crawling_listのrecrawl_countが進んでいる場合$duplicate_num == 0になってしまうので削除しない。
//			deletePublish($listing_type, $target_publish_id);
			//ToDo：該当publish_idに対応するクロールID取得、ステータスUPDATEする
		}else{
			#同一対象のyyyymmddにより分類
			for($i=0; $i<$duplicate_num; $i++){
				$tmp_crawl_id = $same_crawl_ids[$i][0];
				$tmp_ymd = $same_crawl_ids[$i][1];
				$tmp_created = $same_crawl_ids[$i][2];
				if($tmp_created < $min_start_datetime){
					$min_start_datetime = $tmp_created;
					$bool_min_start_datetime_changed = true;
				}
				if($tmp_ymd > $target_ymd){
					array_push($crawl_ids_notyet, $tmp_crawl_id);
				}else if($tmp_ymd == $close_yyyymmdd){
					array_push($crawl_ids_ymd, $tmp_crawl_id);
				}else{
					array_push($crawl_ids_already, $tmp_crawl_id);
				}
			}
			#createdの更新が必要な場合先に更新しておく
			if($bool_min_start_datetime_changed){
				updatePublishStart($listing_type, $target_publish_id, $min_start_datetime);
			}
			$close_num = count($crawl_ids_ymd) + count($crawl_ids_already);
			#同一数により各ステータス変更＆掲載終了判定を実施
//$log->freeform('publish_close_by_id', 'count($same_crawl_ids):'.count($same_crawl_ids));
			if(($duplicate_num == 1 && $close_num == 1) || $close_num >=2){
				#同一なしで終了1件、または同一2件以上の場合は掲載終了2件以上で掲載終了処理
				$termination_flg=0;
				if($duplicate_num == 1){
					$termination_flg=2;  //2:1/1で掲載終了確認
				}else{
					$termination_flg=3;  //3:一定の割合で掲載終了確認
				}
				closePublish($listing_type, $target_publish_id, $duplicate_num, $close_num, $termination_flg, $close_yyyymmdd);
				#crawlステータス更新
				if(count($crawl_ids_notyet) >= 1){
					updateCrawlStatus($crawl_ids_notyet, 198);
				}
				if(count($crawl_ids_ymd) >= 1){
					updateCrawlStatus($crawl_ids_ymd, 200);
				}
				if(count($crawl_ids_already) >= 1){
					updateCrawlStatus($crawl_ids_already, 201);
				}
			}else{
				#掲載中処理(現在掲載中かどうかに関わらずカウントをアップデートする)
				openPublish($listing_type, $target_publish_id, $duplicate_num, $close_num);
				#crawlステータス更新
				if(count($crawl_ids_notyet) >= 1){
					updateCrawlStatus($crawl_ids_notyet, 100);
				}
				if(count($crawl_ids_ymd) >= 1){  //存在しないはず
					updateCrawlStatus($crawl_ids_ymd, 199);
				}
				if(count($crawl_ids_already) >= 1){
					updateCrawlStatus($crawl_ids_already, 199);
				}
			}
		}
	}
	$dt_loop_end = new DateTimeImmutable();
	$elapsed = $dt_loop_end->diff($dt_loop_1)->format("%H:%I:%S.%F");
	$log->freeform('publish_close_by_id' . $log_suffix, 'ループ終了(' . $loop_num .') -> ' . $elapsed);
	echo $loop_num . '->'. $elapsed . ' ';
}
$dt2 = new DateTime();
$elapsed = $dt2->diff($dt)->format("%H:%I:%S.%F");
$log->freeform("publish_close_by_id" . $log_suffix, '処理終了 -> ' . $elapsed);


/**
 * publishに対する同一のcrawl_idを返す
 * crawl_idはchanged_num=recrawl_countになっているもののみを抽出する。
 *
 * @param string $listing_type 'sell'または'rent'
 * @param int $publish_id
 * @return array{int $crawl_id, int $yyyymmdd, str $created}[]
 */
function getSameCrawlIds($listing_type, $publish_id, $country_cd){

	# グローバル変数を参照
	global $pdo_tp;
	global $log;
	global $log_suffix;

	$crawl_ids = array();
	$sql = "SELECT /*+INDEX (cl CLEANSING_" . strtoupper($listing_type) . "_CCD_AND_PUBID) INDEX (c CRAWLING_LIST_PK) */ c.crawl_id,c.yyyymmdd,TO_CHAR(c.created," . DB_DATE_FORMAT . ") AS created FROM cleansing_" . $listing_type . " cl INNER JOIN crawling_list c ON cl.crawl_id=c.crawl_id WHERE cl.publish_id=" . $publish_id . " AND cl.country_cd=" . $country_cd . " ORDER BY c.yyyymmdd ASC";
	$stmt = null;
	try {
		$stmt = $pdo_tp->query($sql);
	} catch (PDOException $e) {
		$log->freeform('publish_close_by_id' . $log_suffix, 'getSameCrawlIdsエラー' . $e->getMessage());
		exit;
	}
	$rows = $stmt->fetchAll();
	#取得したcrawl_idの件数分終了処理をする。
	foreach ($rows as $row) {
		$crawl_id_ymd = array((int)$row['crawl_id'], (int)$row['yyyymmdd'], $row['created']);
		array_push($crawl_ids, $crawl_id_ymd);
	}
	return $crawl_ids;

}

/**
 * crawling_listのstatusを更新する
 *
 * @param array int[] $crawl_ids
 * @param int $status
 * @return void
 */
function updateCrawlStatus($crawl_ids, $status){

	# グローバル変数を参照
	global $pdo_tp;
	global $log;
	global $log_suffix;

	#一括更新用SQL作成
	$in_clause = '';
	for($i=0; $i<count($crawl_ids); $i++){
		if($in_clause != ''){
			$in_clause .= ',';
		}
		$in_clause .= $crawl_ids[$i];
	}
	$sql = "UPDATE crawling_list SET status=" . $status . " WHERE crawl_id IN (" . $in_clause . ")";
$log->freeform('publish_close_by_id' . $log_suffix, $sql);

	try {
		$stmt = $pdo_tp->query($sql);
	} catch (PDOException $e) {
		$log->freeform('publish_close_by_id' . $log_suffix, 'updateCrawlStatusエラー' . $e->getMessage());
		exit;
	}
}

/**
 * publishデータが公開かどうかを返す
 *
 * @param string $listing_type 'sell'または'rent'
 * @param int $publish_id
 * @return array{boolean $flg_closed, str $start_datetime} 
 */
function getPublishClosed($listing_type, $publish_id){
	# グローバル変数を参照
	global $stmt_get_publish_closed_rent;
	global $stmt_get_publish_closed_sell;
	global $log;

	$flg_closed = false;
	$start_datetime = '';
	if($listing_type == 'rent'){
        $stmt_get_publish_closed_rent->bindParam(':publish_id', $publish_id, PDO::PARAM_INT);
		try {
			$stmt_get_publish_closed_rent->execute();
			$row = $stmt_get_publish_closed_rent->fetch();
			if($row['status_flg'] == 1){
				$flg_closed = true;
			}
			$start_datetime = $row['start_datetime'];
		} catch (PDOException $e) {
			$log->freeform('publish_close_' . $target_ymd, 'getPublishClosed(rent)->param:' . $publish_id . '|' . $e->getMessage());
		}
	}else if($listing_type == 'sell'){
        $stmt_get_publish_closed_sell->bindParam(':publish_id', $publish_id, PDO::PARAM_INT);
		try {
			$stmt_get_publish_closed_sell->execute();
			$row = $stmt_get_publish_closed_sell->fetch();
			if($row['status_flg'] == 1){
				$flg_closed = true;
			}
			$start_datetime = $row['start_datetime'];
		} catch (PDOException $e) {
			$log->freeform('publish_close_' . $target_ymd, 'getPublishClosed(sell)->param:' . $publish_id . '|' . $e->getMessage());
		}
	}
	$arr_return = array($flg_closed, $start_datetime);	
	return $arr_return;
}

/**
 * publishデータを掲載終了にする
 *
 * @param string $listing_type 'sell'または'rent'
 * @param int $publish_id
 * @param int $duplicate_num
 * @param int $status_flg
 * @param int $termination_flg
 * @param int $close_num
 * @param int $close_yyyymmdd
 * @return void
 */
function closePublish($listing_type, $publish_id, $duplicate_num, $close_num, $termination_flg, $close_yyyymmdd){
	
	# グローバル変数を参照
	global $pdo_tp;
	global $log;
	global $log_suffix;

	$format_ymd = 'Ymd H:i:s';
	$close_datetime = DateTime::createFromFormat($format_ymd, $close_yyyymmdd . ' 00:00:00');
	$close_datetime_str = $close_datetime->format('Y/m/d H:i:s');
	$sql = "UPDATE publish_" . $listing_type . " SET duplicate_num=" . $duplicate_num . 
		", close_num=" . $close_num . ", termination_flg=" . $termination_flg . 
		", yyyymmdd=" . $close_yyyymmdd . ",close_datetime=TO_DATE('" . $close_datetime_str . "', " . DB_DATE_FORMAT . ")" .
		", publish_days=TO_NUMBER(TRUNC(TO_DATE('" . $close_datetime_str . "', " . DB_DATE_FORMAT . ")) - TRUNC(start_datetime))" .
		", status_flg=1,complete_datetime=SYSDATE + 9/24 WHERE publish_id=" . $publish_id;
$log->freeform('publish_close_by_id' . $log_suffix, $sql);
	try {
		$stmt = $pdo_tp->query($sql);
	} catch (PDOException $e) {
		$log->freeform('publish_close_by_id' . $log_suffix, 'closePublishエラー->' . $e->getMessage());
		exit;
	}

}

/**
 * publishデータを掲載中にする
 *
 * @param string $listing_type 'sell'または'rent'
 * @param int $publish_id
 * @param int $duplicate_num
 * @param int $close_num
 * @return void
 */

function openPublish($listing_type, $publish_id, $duplicate_num, $close_num){
	
	# グローバル変数を参照
	global $pdo_tp;
	global $log;
	global $log_suffix;

	$sql = "UPDATE publish_" . $listing_type . " SET duplicate_num=" . $duplicate_num . 
		", close_num=" . $close_num . ", termination_flg=0" .
		", yyyymmdd=99999999,close_datetime=NULL" .
		", publish_days=NULL" .
		", status_flg=0,complete_datetime=NULL WHERE publish_id=" . $publish_id;
$log->freeform('publish_close_by_id' . $log_suffix, $sql);
	try {
		$stmt = $pdo_tp->query($sql);
	} catch (PDOException $e) {
		$log->freeform('publish_close_by_id' . $log_suffix, 'closePublishエラー->' . $e->getMessage());
		exit;
	}

}

/**
 * publishデータを削除する
 *
 * @param string $listing_type 'sell'または'rent'
 * @param int $publish_id
 * @return void
 */

function deletePublish($listing_type, $publish_id){
	
	# グローバル変数を参照
	global $pdo_tp;
	global $log;
	global $log_suffix;

	$sql = "DELETE FROM tag_mapping_" . $listing_type . " WHERE publish_id=" . $publish_id;
$log->freeform('publish_close_by_id' . $log_suffix, $sql);
	try {
		$stmt = $pdo_tp->query($sql);
	} catch (PDOException $e) {
		$log->freeform('publish_close_by_id' . $log_suffix, 'deletePublishエラー->' . $e->getMessage());
		exit;
	}
	$sql = "DELETE FROM publish_" . $listing_type . " WHERE publish_id=" . $publish_id;
$log->freeform('publish_close_by_id' . $log_suffix, $sql);
	try {
		$stmt = $pdo_tp->query($sql);
	} catch (PDOException $e) {
		$log->freeform('publish_close_by_id' . $log_suffix, 'deletePublishエラー->' . $e->getMessage());
		exit;
	}

}

/**
 * publishデータの開始日を更新する
 *
 * @param string $listing_type 'sell'または'rent'
 * @param int $publish_id
 * @param int $start_datetime
 * @return void
 */
function updatePublishStart($listing_type, $publish_id, $start_datetime){
	# グローバル変数を参照
	global $pdo_tp;
	global $log;
	global $log_suffix;

	$sql = "UPDATE publish_" . $listing_type . " SET start_datetime=TO_DATE('" . $start_datetime . "', " . DB_DATE_FORMAT . ") WHERE publish_id=" . $publish_id;
$log->freeform('publish_close_by_id' . $log_suffix, $sql);
	try {
		$stmt = $pdo_tp->query($sql);
	} catch (PDOException $e) {
		$log->freeform('publish_close_by_id' . $log_suffix, 'updatePublishStartエラー->param:' . $listing_type . '|' . $publish_id . '|' . $e->getMessage());
	}
}

?>
