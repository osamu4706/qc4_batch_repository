<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/www/import/config.php";
require '/var/www/import/db_oracle.php';
require "/var/www/import/tools.php";
require "/var/www/import/log.php";

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);
$log = new log();

/*
INSERT INTO scrape SELECT * FROM scrape_20191029_2 WHERE crawl_id IN (SELECT crawl_id FROM tmp_serial_scrape WHERE id BETWEEN :start_id AND :end_id");
*/

$limit_num=1000;

$again_list=array(12582001,12583001,12607001,13083001,13208001,13385001,13464001,13499001,13547001,13942001,14229001,14278001,14700001,14728001,14770001,14798001,14806001,14808001,14828001,14880001,14949001,15072001,15432001,15640001,15701001,15782001,16494001,16522001,16631001,16823001,16906001,17420001,17545001,17654001,17856001,17963001,18196001,18206001,18278001,18491001,18543001,18619001,18807001,19254001,19573001,19707001,21062001,21266001,21281001,21362001,21708001,22044001,22176001,22267001,22295001,22327001,22410001,22590001,22730001,22895001);

$sql = "INSERT INTO scrape SELECT * FROM scrape_20191029_2 WHERE crawl_id IN (SELECT crawl_id FROM tmp_serial_scrape WHERE id BETWEEN :start_id AND :end_id)";
$stmt = $pdo_medium->prepare($sql);

for($i=0; $i <count($again_list); $i++){
	$dt_s = new DateTime();
	$start_id = $again_list[$i];
	$end_id = $again_list[$i]+999;
	$stmt->bindParam(':start_id', $start_id, PDO::PARAM_INT);
	$stmt->bindParam(':end_id', $end_id, PDO::PARAM_INT);
	$flg_error = 0;
	try{
		$stmt->execute();
	}catch (Exception $e) {
		$log->freeform("insert_scrape_again_error", $start_id . ',' . $end_id  . ',' . $e->getMessage());
		$flg_error = 1;
	}
	$dt_e = new DateTime();
	$elapsed = $dt_e->diff($dt_s)->format("%s.%f");
	$log->freeform("insert_scrape_again", $flg_error . ',' . $start_id . ',' . $end_id  . ',' . $elapsed);

	if($i % 10 == 0){
		echo $i . ' ';
	}
}

?>
