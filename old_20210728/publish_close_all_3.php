<?php

/** 実行ファイルから見たパスで指定 */
require '/var/www/import/config.php';
require '/var/www/import/db_oracle.php';
require '/var/www/import/log.php';
require '/var/www/import/tools.php';

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);
//$pdo_high = DB::getPdo(DB_ORA_TNS_HIGH);

$log = new log();

//$target_num = 10;
//$target_max = 10;
$target_num = 1000;  //この件数ずつcrawling_listから抽出して処理
$target_max = 100000;  //1回のバッチで対象とする最大数
//$target_ymd=20200501;

/*
 *
 * 掲載終了処理Dailyバッチ
 *  publishに掲載終了セット
 *  同一物件crawling_listに掲載終了セット
 *  マップ／マップ用タグマッピング(未実装)から掲載終了の物件情報を削除、
 *  ※DailyバッチではCRAWLING_LIST_I03索引を使ってyyyymmddで対象を照会。再クレンジング等で過去分の処理多数の場合はCRAWLING_LIST_I01索引でstatus=100の過去yyyymmddを抽出する。
 *
 * ■16:00にスケジュール実行する。＜現行未設定＞
 * 0 16 * * * opc /usr/bin/php /var/www/batch/publish_close.php
 *
 * 【掲載終了日の設定ルール】
 * ①同一物件が複数の場合に、ある物件の2件目の掲載終了に対して、
 * 　最初に掲載終了した日を掲載終了日とする
 * ②同一物件が存在しない場合は、最初に終了した日とする
 *
 * 【掲載終了の処理】
 * (1)当日のバッチのターゲットとなるcrawl_idを抽出
 *    クロール～クレンジングが正常完了している、完了日付の更新(yyyymmdd)が指定日数止まっている、
 *    かつclosed設定されていないクロールID (status=100 かつ yyyymmddが指定日付)
 * (2)対応するpublishデータの同一物件と掲載終了数をチェック
 *   (2-1)同一物件が1件(自分だけ)の場合、掲載終了処理
 *     掲載物件をクローズ処理
 *     当URLをクローズ処理
 *   (2-2)同一物件が2件以上の場合
 *     (2-2-1)既に掲載物件が掲載終了している場合、当URLはクローズ処理 ※再クレンジングなどで発生
 *       当URLをクローズ処理(min(終了日)とyyyymmddが等しいURLはstatus:200、それ以外は202
 *     (2-2-2)掲載物件が掲載中の場合
 *       (2-2-2-1)既に掲載終了しているURLがある場合、掲載終了処理
 *         掲載物件をクローズ処理
 *         同一物件URLをクローズ処理(min(終了日)とyyyymmddが等しいURLはstatus:200、それ以外は202)
 *       (2-2-2-2)同一物件内で初めての掲載終了の場合、掲載物件は掲載中のまま、URLをクローズ処理(status:200)
 *         当URLをクローズ処理(min(終了日)とyyyymmddが等しいURLはstatus:200、それ以外は202)
 * 
 * 【物件掲載終了の処理】
 * <publish_rent/sell>に以下の値をセットする
 * ・PUBLISH_DAYS に値を入れる（start_datetimeからyyyymmddの日付の差分）
 * 　　⇒ start_datetimeに値がないものをどうするか
 * 　　　　　⇒　cleansing_rent/sellのreg_datetimeとか？
 * ・close_datetime(最終掲載確認日時) => yyyymmdd
 * ・complete_datetime-> sysdate
 * ・status_flg(掲載ステータスフラグ)：１
 * ・close_num（掲載終了数）
 * ・termination_flg(掲載終了理由)
 *
 * <map_rent/sell>の以下のレコードの削除
 * ・掲載終了対象の publilsh_id が一致
 *
 * <tag_mapping_map_rent/map_sell> ※map用tag_mappingテーブル作成＆運用後
 * ・掲載終了対象の publilsh_id が一致
 * 
 * <crawling_list> crawl_idが一致するレコードに以下の値をセット
 * ・status => 200
 * 　　⇒同一物件判定で掲載終了したものは、202をセット
 * ・closed => yyyymmdd の値
 * ・updated(更新日時) => SYSDATE + 9/24
 *
 * <scrape>
 * ・　現行そのまま
 *
 *【関連テーブル】
 * crawling_list
 * scrape
 * cleansing_xxxx
 * publish_xxxx
 * map_xxxx
 * tag_mapping_xxxx
 * tag_mapping_map_xxxx ※未作成
 *
 *
*/

$dt = new DateTimeImmutable();  //modify()をしても元の$dtの値は不変
// timezoneをQC用(Asia/Tokyo)に合わせて3日前ymd取得
//$arr_target_ymd = array(20190321,20190322,20190323,20190324,20190325,20190326,20190327,20190328,20190329,20190330,20190331,20190401,20190402,20190403,20190404,20190405,20190406,20190407,20190408,20190409,20190410,20190411,20190412,20190413,20190414,20190415,20190416,20190417,20190418,20190419,20190420,20190421,20190422,20190423,20190424,20190425,20190426,20190427,20190428,20190429,20190430,20190501,20190502,20190503,20190504,20190505,20190506,20190507,20190508,20190509,20190510,20190511,20190512,20190513,20190514,20190515,20190516,20190517,20190518,20190519,20190520,20190521,20190522,20190523,20190524,20190525,20190526,20190527,20190528,20190529,20190530,20190531,20190601,20190602,20190603,20190604,20190605,20190606,20190607,20190608,20190609,20190610,20190611,20190612,20190613,20190614,20190615,20190616,20190617,20190618,20190619,20190620,20190621,20190622,20190623,20190624,20190625,20190626,20190627,20190628,20190629,20190630,20190701,20190702,20190703,20190704,20190705,20190706,20190707,20190708,20190709,20190710,20190711,20190712,20190713,20190714,20190715,20190716,20190717,20190718,20190719,20190720,20190721,20190722,20190723,20190724,20190725,20190726,20190727,20190728,20190729,20190730,20190731,20190801,20190802,20190803,20190804,20190805,20190806,20190807,20190808,20190809,20190810,20190811,20190812,20190813,20190814,20190815,20190816,20190817,20190818,20190819,20190820,20190821,20190822,20190823,20190824,20190825,20190826,20190827,20190828,20190829,20190830,20190831,20190901,20190902,20190903,20190904,20190905,20190906,20190907,20190908,20190909,20190910,20190911,20190912,20190913,20190914,20190915,20190916,20190917,20190918,20190919,20190920,20190921,20190922,20190923,20190924,20190925,20190926,20190927,20190928,20190929,20190930,20191001,20191002,20191003,20191004,20191005,20191006,20191007,20191008,20191009,20191010,20191011,20191012,20191013,20191014,20191015,20191016,20191017,20191018,20191019,20191020,20191021,20191022,20191023,20191024,20191025,20191026,20191027,20191028,20191029,20191030,20191031,20191101,20191102,20191103,20191104,20191105,20191106,20191107,20191108,20191109,20191110,20191111,20191112,20191113,20191114,20191115,20191116,20191117,20191118,20191119,20191120,20191121,20191122,20191123,20191124,20191125,20191126,20191127,20191128,20191129,20191130,20191201,20191202,20191203,20191204,20191205,20191206,20191207,20191208,20191209,20191210,20191211,20191212,20191213,20191214,20191215,20191216,20191217,20191218,20191219,20191220,20191221,20191222,20191223,20191224,20191225,20191226,20191227,20191228,20191229,20191230,20191231,20200101,20200102,20200103,20200104,20200105,20200106,20200107,20200108,20200109,20200110,20200111,20200112,20200113,20200114,20200115,20200116,20200117,20200118,20200119,20200120,20200121,20200122,20200123,20200124,20200125,20200126,20200127,20200128,20200129,20200130,20200131,20200201,20200202,20200203,20200204,20200205,20200206,20200207,20200208,20200209,20200210,20200211,20200212,20200213,20200214,20200215,20200216,20200217,20200218,20200219,20200220,20200221,20200222,20200223,20200224,20200225,20200226,20200227,20200228,20200229,20200301,20200302,20200303,20200304,20200305,20200306,20200307,20200308,20200309,20200310,20200311,20200312,20200313,20200314,20200315,20200316,20200317,20200318,20200319,20200320,20200321,20200322,20200323,20200324,20200325,20200326,20200327,20200328,20200329,20200330,20200331,20200401,20200402,20200403,20200404,20200405,20200406,20200407,20200408,20200409,20200410,20200411,20200412,20200413,20200414,20200415,20200416,20200417,20200418,20200419,20200420,20200421,20200422,20200423,20200424,20200425,20200426,20200427,20200428,20200429,20200430,20200501,20200502,20200503,20200504,20200505,20200506,20200507,20200508,20200509,20200510,20200511,20200512,20200513,20200514,20200515,20200516,20200517,20200518,20200519,20200520,20200521,20200522,20200523,20200524,20200525,20200526,20200527,20200528,20200529,20200530,20200531,20200601,20200602,20200603,20200604,20200605,20200606,20200607,20200608,20200609,20200610,20200611,20200612,20200613,20200614,20200615,20200616,20200617,20200618,20200619,20200620,20200621,20200622,20200623,20200624,20200625,20200626,20200627,20200628,20200629,20200630,20200701,20200702,20200703,20200704,20200705,20200706,20200707,20200708,20200709,20200710,20200711,20200712,20200713,20200714,20200715,20200716,20200717,20200718,20200719,20200720,20200721,20200722,20200723,20200724,20200725,20200726,20200727,20200728,20200729,20200730,20200731,20200801,20200802,20200803,20200804,20200805,20200806,20200807,20200808,20200809,20200810,20200811,20200812,20200813,20200814,20200815,20200816,20200817,20200818,20200819,20200820,20200821,20200822,20200823,20200824,20200825,20200826,20200827,20200828,20200829,20200830,20200831,20200901,20200902,20200903,20200904,20200905,20200906,20200907);
$arr_target_ymd = array(20200506,20200507,20200508,20200509,20200510,20200511,20200512,20200513,20200514,20200515,20200516,20200517,20200518,20200519,20200520,20200521,20200522,20200523,20200524,20200525,20200526,20200527,20200528,20200529,20200530,20200531,20200601,20200602,20200603,20200604,20200605,20200606,20200607,20200608,20200609,20200610,20200611,20200612,20200613,20200614,20200615,20200616,20200617,20200618,20200619,20200620,20200621,20200622,20200623,20200624,20200625,20200626,20200627,20200628,20200629,20200630,20200701,20200702,20200703,20200704,20200705,20200706,20200707,20200708,20200709,20200710,20200711,20200712,20200713,20200714,20200715,20200716,20200717,20200718,20200719,20200720,20200721,20200722,20200723,20200724,20200725,20200726,20200727,20200728,20200729,20200730,20200731,20200801,20200802,20200803,20200804,20200805,20200806,20200807,20200808,20200809,20200810,20200811,20200812,20200813,20200814,20200815,20200816,20200817,20200818,20200819,20200820,20200821,20200822,20200823,20200824,20200825,20200826,20200827,20200828,20200829,20200830,20200831,20200901,20200902,20200903,20200904,20200905,20200906,20200907);


/*
$dt2 = new DateTime();
$elapsed = $dt2->diff($dt)->format("%H:%I:%S.%F");
$log->freeform("publish_close", 'テンポラリーテーブルINSERT -> ' . $elapsed);
*/

#########################
##※無限ループで回す。指定件数超の場合終了。
## (1)当日のバッチのターゲットとなるcrawl_idを抽出
##   クロール～クレンジングが正常完了している、完了日付の更新(yyyymmdd)が指定日数止まっている、
##   かつclosed設定されていないクロールID (yyyymmddが指定日付でstatus=100 or 198のもの)
#########################
$sql_get_publish_id_rent = "SELECT country_cd,publish_id FROM cleansing_rent WHERE crawl_id=:crawl_id AND changed_num=:changed_num";
$stmt_get_publish_id_rent = $pdo_tp->prepare($sql_get_publish_id_rent);
$sql_get_publish_id_sell = "SELECT country_cd,publish_id FROM cleansing_sell WHERE crawl_id=:crawl_id AND changed_num=:changed_num";
$stmt_get_publish_id_sell = $pdo_tp->prepare($sql_get_publish_id_sell);
$sql_update_publish_count_rent = "UPDATE publish_rent SET duplicate_num = :duplicate_num, close_num = :close_num WHERE publish_id=:publish_id";
$stmt_update_publish_count_rent = $pdo_tp->prepare($sql_update_publish_count_rent);
$sql_update_publish_count_sell = "UPDATE publish_sell SET duplicate_num = :duplicate_num, close_num = :close_num WHERE publish_id=:publish_id";
$stmt_update_publish_count_sell = $pdo_tp->prepare($sql_update_publish_count_sell);
$sql_get_publish_closed_rent = "SELECT status_flg,TO_CHAR(start_datetime," . DB_DATE_FORMAT . ") AS start_datetime FROM publish_rent WHERE publish_id=:publish_id";
$stmt_get_publish_closed_rent = $pdo_tp->prepare($sql_get_publish_closed_rent);
$sql_get_publish_closed_sell = "SELECT status_flg,TO_CHAR(start_datetime," . DB_DATE_FORMAT . ") AS start_datetime FROM publish_sell WHERE publish_id=:publish_id";
$stmt_get_publish_closed_sell = $pdo_tp->prepare($sql_get_publish_closed_sell);


for($d=0; $d<count($arr_target_ymd); $d++){

$loop_num = 0;
$loop_max = $target_max / $target_num;

$target_ymd=$arr_target_ymd[$d];

$sql = "SELECT /*+INDEX(a CRAWLING_LIST_I03)*/ a.crawl_id, a.recrawl_count, a.status" . 
	" FROM crawling_list a " . 
	" WHERE a.yyyymmdd = " . $target_ymd . " AND status =101 AND site_no in (1,3) AND rownum <= " . $target_num;

$log->freeform('publish_close_all_3_' . $target_ymd, $sql);

$log->freeform('publish_close_all_3_' . $target_ymd, '(バッチスタート) 対象yyyymmdd : ' . $target_ymd);
$row_cnt=0;

while($loop_num < $loop_max){
	$loop_num ++;
	$dt_loop_1 = new DateTimeImmutable();
	$processed_ids = array(); //当ループ内で同一物件として処理済みのcrawl_id(ループ外の同一物件はこのループでstatus変更されて以降のループで抽出されない)
	try {
		$stmt = $pdo_medium->query($sql);
	} catch (PDOException $e) {
		$log->freeform('publish_close_all_3_' . $target_ymd, 'crawling_list対象抽出エラー' . $e->getMessage());
		exit;
	}
	$dt_loop_2 = new DateTimeImmutable();
	$rows = $stmt->fetchAll();
	if(count($rows) == 0 || (count($rows)<$target_num && $row_cnt==count($rows)) ){  //101のまま対象がなく残ってしまった場合
		#対象がなくなったらループを抜ける
		break;
	}
	$row_cnt=count($rows);
	#取得したcrawl_idの件数分終了処理をする。
	foreach ($rows as $row) {
		$target_crawl_id = (int)$row['crawl_id'];
		$target_changed_num = (int)$row['recrawl_count'];
		$target_status = (int)$row['status'];
		$target_publish_id_rent = 0;
		$target_publish_id_sell = 0;
		#処理済みチェック
		if(in_array($target_crawl_id, $processed_ids)){
$log->freeform('publish_close_all_3_' . $target_ymd, '処理済み：$target_crawl_id->' . $target_crawl_id);
			#処理済みのため何もしない
		}else{
$log->freeform('publish_close_all_3_' . $target_ymd, '処理開始：$target_crawl_id->' . $target_crawl_id);
			$target_rent_data = getPublishId('rent', $target_crawl_id, $target_changed_num);
			$target_publish_id_rent = $target_rent_data[0];
			$target_country_cd_rent = $target_rent_data[1];
			$target_sell_data = getPublishId('sell', $target_crawl_id, $target_changed_num);
			$target_publish_id_sell = $target_sell_data[0];
			$target_country_cd_sell = $target_sell_data[1];
			#rent/sellそれぞれ存在チェック＆同一抽出
			//rent/sellに両方存在していても(再クレンジング等で発生する可能性あり)異常データにはせずそのまま両方処理する
			#cleansing_rentに存在チェック＆同一抽出
//			if($target_publish_id_rent > 1){
$log->freeform('publish_close_all_3_' . $target_ymd, 'target_publish_id_rent：'.$target_publish_id_rent.'/'.'target_publish_id_sell：'.$target_publish_id_sell);
			if($target_publish_id_rent > 50000000){
				$close_num = 0;
				$crawl_ids_ymd = array();  //min(yyyymmdd)と同一
				$crawl_ids_already = array();  //yyyymmddより大きく既に終了(<=$target_ymd)
				$crawl_ids_notyet = array();  //yyyymmddより大きい(まだ終了対象ではない)
				$same_crawl_ids = getSameCrawlIds('rent', $target_publish_id_rent, $target_country_cd_rent); //[crawl_id, yyyymmdd, created]の配列(recrawl_count=changed_numとなっているもののみ。yyyymmdd昇順が保証されている)
				$close_yyyymmdd = $same_crawl_ids[0][1]; //$target_crawl_idではなく同一物件の最小yyyymmddを格納
				$duplicate_num = count($same_crawl_ids);
				$publish_closed = getPublishClosed('rent', $target_publish_id_rent);
				$flg_closed = $publish_closed[0];
				$min_start_datetime = $publish_closed[1];
				$bool_min_start_datetime_changed = false;
				#同一対象のyyyymmddにより分類
				for($i=0; $i<$duplicate_num; $i++){
					$tmp_crawl_id = $same_crawl_ids[$i][0];
					$tmp_ymd = $same_crawl_ids[$i][1];
					$tmp_created = $same_crawl_ids[$i][2];
					if($tmp_created < $min_start_datetime){
						$min_start_datetime = $tmp_created;
						$bool_min_start_datetime_changed = true;
					}
//$log->freeform('publish_close_all_3_' . $target_ymd, 'tmp_crawl_id：'.$tmp_crawl_id.'/'.'tmp_ymd：'.$tmp_ymd.'/target_ymd:'.$target_ymd.'/close_yyyymmdd:'.$close_yyyymmdd);
					if($tmp_ymd > $target_ymd){
//$log->freeform('publish_close_all_3_' . $target_ymd, '$crawl_ids_notyet->push');
						array_push($crawl_ids_notyet, $tmp_crawl_id);
					}else if($tmp_ymd == $close_yyyymmdd){
//$log->freeform('publish_close_all_3_' . $target_ymd, '$crawl_ids_ymd->push');
						array_push($crawl_ids_ymd, $tmp_crawl_id);
					}else{
//$log->freeform('publish_close_all_3_' . $target_ymd, '$crawl_ids_already->push');
						array_push($crawl_ids_already, $tmp_crawl_id);
					}
				}
				#createdの更新が必要な場合先に更新しておく
				if($bool_min_start_datetime_changed){
					updatePublishStart('rent', $target_publish_id_rent, $min_start_datetime);
				}
				$close_num = count($crawl_ids_ymd) + count($crawl_ids_already);
				#同一数により各ステータス変更＆掲載終了判定を実施
//$log->freeform('publish_close_all_3_' . $target_ymd, 'count($same_crawl_ids):'.count($same_crawl_ids));
				if(count($same_crawl_ids) == 1){
					#同一なしの場合は掲載終了処理
					$termination_flg=2;  //2:1/1で掲載終了確認
					closePublish('rent', $target_publish_id_rent, $duplicate_num, $close_num, $termination_flg, $close_yyyymmdd);
					#crawlステータス更新
					if(count($crawl_ids_ymd) >= 1){
						updateCrawlStatus($crawl_ids_ymd, 200);
					}
				}else if(count($same_crawl_ids) >= 2){
					if($flg_closed){
						#既に終了している場合(カウントのみ更新)
						updatePublishCount('rent', $target_publish_id_rent, $duplicate_num, $close_num); //終了には関与せずカウントのみupdate
						#crawlステータス更新
						if(count($crawl_ids_notyet) >= 1){
							updateCrawlStatus($crawl_ids_notyet, 198);
						}
						if(count($crawl_ids_ymd) >= 1){
							updateCrawlStatus($crawl_ids_ymd, 200);
						}
						if(count($crawl_ids_already) >= 1){
							updateCrawlStatus($crawl_ids_already, 201);
						}
					}else{
						#終了判定
						if($close_num == 1){
							#自分だけ終了の場合publishはカウントのみアップデート、終了済みURLは掲載終了待ち199
							updatePublishCount('rent', $target_publish_id_rent, $duplicate_num, $close_num); //終了には関与せずカウントのみupdate
							#crawlステータス更新
							if(count($crawl_ids_ymd) >= 1){
								updateCrawlStatus($crawl_ids_ymd, 199);
							}
							if(count($crawl_ids_already) >= 1){  //現仕様では当条件には$crawl_ids_ymdに1件のみしか入らないはずだけど掲載終了済みのステータス処理として記載しておく
								updateCrawlStatus($crawl_ids_already, 199);
							}
						}else if($close_num >=2){
							#同一2件以上の場合は掲載終了2件以上で掲載終了処理
							$termination_flg=3;  //3:一定の割合で掲載終了確認
							closePublish('rent', $target_publish_id_rent, $duplicate_num, $close_num, $termination_flg, $close_yyyymmdd);
							#crawlステータス更新
							if(count($crawl_ids_notyet) >= 1){
								updateCrawlStatus($crawl_ids_notyet, 198);
							}
							if(count($crawl_ids_ymd) >= 1){
								updateCrawlStatus($crawl_ids_ymd, 200);
							}
							if(count($crawl_ids_already) >= 1){
								updateCrawlStatus($crawl_ids_already, 201);
							}
						}
					}
				}
			}
//			if($target_publish_id_sell > 1){
			if($target_publish_id_sell > 50000000){
				$close_num = 0;
				$crawl_ids_ymd = array();  //min(yyyymmdd)と同一
				$crawl_ids_already = array();  //yyyymmddより大きく既に終了(<=$target_ymd)
				$crawl_ids_notyet = array();  //yyyymmddより大きい(まだ終了対象ではない)
				$same_crawl_ids = getSameCrawlIds('sell', $target_publish_id_sell, $target_country_cd_sell); //[crawl_id, yyyymmdd]の配列(recrawl_count=changed_numとなっているもののみ。yyyymmdd昇順が保証されている)
				$close_yyyymmdd = $same_crawl_ids[0][1]; //$target_crawl_idではなく同一物件の最小yyyymmddを格納
				$duplicate_num = count($same_crawl_ids);
				$publish_closed = getPublishClosed('sell', $target_publish_id_sell);
				$flg_closed = $publish_closed[0];
				$min_start_datetime = $publish_closed[1];
				$bool_min_start_datetime_changed = false;
				#同一対象のyyyymmddにより分類
				for($i=0; $i<$duplicate_num; $i++){
					$tmp_crawl_id = $same_crawl_ids[$i][0];
					$tmp_ymd = $same_crawl_ids[$i][1];
					$tmp_created = $same_crawl_ids[$i][2];
					if($tmp_created < $min_start_datetime){
						$min_start_datetime = $tmp_created;
						$bool_min_start_datetime_changed = true;
					}
					if($tmp_ymd > $target_ymd){
						array_push($crawl_ids_notyet, $tmp_crawl_id);
					}else if($tmp_ymd == $close_yyyymmdd){
						array_push($crawl_ids_ymd, $tmp_crawl_id);
					}else{
						array_push($crawl_ids_already, $tmp_crawl_id);
					}
				}
				#createdの更新が必要な場合先に更新しておく
				if($bool_min_start_datetime_changed){
					updatePublishStart('sell', $target_publish_id_sell, $min_start_datetime);
				}
				$close_num = count($crawl_ids_ymd) + count($crawl_ids_already);
				#同一数により各ステータス変更＆掲載終了判定を実施
				if(count($same_crawl_ids) == 1){
					#同一なしの場合は掲載終了処理
					$termination_flg=2;  //2:1/1で掲載終了確認
					closePublish('sell', $target_publish_id_sell, $duplicate_num, $close_num, $termination_flg, $close_yyyymmdd);
					#crawlステータス更新
					if(count($crawl_ids_ymd) >= 1){
						updateCrawlStatus($crawl_ids_ymd, 200);
					}
				}else if(count($same_crawl_ids) >= 2){
					if($flg_closed){
						#既に終了している場合(カウントのみ更新)
						updatePublishCount('sell', $target_publish_id_sell, $duplicate_num, $close_num); //終了には関与せずカウントのみupdate
						#crawlステータス更新
						if(count($crawl_ids_notyet) >= 1){
							updateCrawlStatus($crawl_ids_notyet, 198);
						}
						if(count($crawl_ids_ymd) >= 1){
							updateCrawlStatus($crawl_ids_ymd, 200);
						}
						if(count($crawl_ids_already) >= 1){
							updateCrawlStatus($crawl_ids_already, 201);
						}
					}else{
						#終了判定
						if($close_num == 1){
							#自分だけ終了の場合publishはカウントのみアップデート、終了済みURLは掲載終了待ち199
							updatePublishCount('sell', $target_publish_id_sell, $duplicate_num, $close_num); //終了には関与せずカウントのみupdate
							#crawlステータス更新
							if(count($crawl_ids_ymd) >= 1){
								updateCrawlStatus($crawl_ids_ymd, 199);
							}
							if(count($crawl_ids_already) >= 1){  //現仕様では当条件には$crawl_ids_ymdに1件のみしか入らないはずだけど掲載終了済みのステータス処理として記載しておく
								updateCrawlStatus($crawl_ids_already, 199);
							}
						}else if($close_num >=2){
							#同一2件以上の場合は掲載終了2件以上で掲載終了処理
							$termination_flg=3;  //3:一定の割合で掲載終了確認
							closePublish('sell', $target_publish_id_sell, $duplicate_num, $close_num, $termination_flg, $close_yyyymmdd);
							#crawlステータス更新
							if(count($crawl_ids_notyet) >= 1){
								updateCrawlStatus($crawl_ids_notyet, 198);
							}
							if(count($crawl_ids_ymd) >= 1){
								updateCrawlStatus($crawl_ids_ymd, 200);
							}
							if(count($crawl_ids_already) >= 1){
								updateCrawlStatus($crawl_ids_already, 201);
							}
						}
					}
				}
			}
			#処理済みに追加
			array_push($processed_ids, $target_crawl_id);
		}
	}
	$dt_loop_end = new DateTimeImmutable();
	$elapsed = $dt_loop_end->diff($dt_loop_1)->format("%H:%I:%S.%F");
	$log->freeform('publish_close_all_3_' . $target_ymd, 'ループ終了(' . $loop_num .') -> ' . $elapsed);
echo $loop_num . '->'. $elapsed . ' ';
}
$dt2 = new DateTime();
$elapsed = $dt2->diff($dt)->format("%H:%I:%S.%F");
$log->freeform('publish_close_all_3_' . $target_ymd, 'バッチ終了 -> ' . $elapsed);

}

/**
 * crawl_idに対応するpublish_idを取得する
 * cleansing_XXXXテーブルにpublish_idのみのINDEXがないためcountry_cdも合わせて返す
 *
 * @param string $listing_type 'sell'または'rent'
 * @param int $crawl_id
 * @param int $recrawl_count
 * @return array{int $country_cd, int $publish_id}
 */
function getPublishId($listing_type, $crawl_id, $recrawl_count){

	# グローバル変数を参照
	global $stmt_get_publish_id_rent;
	global $stmt_get_publish_id_sell;
	global $log;
	global $target_ymd;

	$publish_id = 0;
	$country_cd = 0;

	if($listing_type == 'rent'){
        $stmt_get_publish_id_rent->bindParam(':crawl_id', $crawl_id, PDO::PARAM_INT);
        $stmt_get_publish_id_rent->bindParam(':changed_num', $recrawl_count, PDO::PARAM_INT);
		try {
			$stmt_get_publish_id_rent->execute();
		} catch (PDOException $e) {
			$log->freeform('publish_close_all_3_' . $target_ymd, 'getPublishIdエラー(rent)->param:' . $crawl_id . '/' . $recrawl_count . '|' . $e->getMessage());
		}
        $row = $stmt_get_publish_id_rent->fetch();
		if($row != null){
			$country_cd = $row['country_cd'];
			$publish_id = $row['publish_id'];
		}
	}else if($listing_type == 'sell'){
        $stmt_get_publish_id_sell->bindParam(':crawl_id', $crawl_id, PDO::PARAM_INT);
        $stmt_get_publish_id_sell->bindParam(':changed_num', $recrawl_count, PDO::PARAM_INT);
		try {
			$stmt_get_publish_id_sell->execute();
		} catch (PDOException $e) {
			$log->freeform('publish_close_all_3_' . $target_ymd, 'getPublishIdエラー(sell)->param:' . $crawl_id . '/' . $recrawl_count . '|' . $e->getMessage());
		}
        $row = $stmt_get_publish_id_sell->fetch();
		if($row != null){
			$publish_id = $row['publish_id'];
			$country_cd = $row['country_cd'];
		}
	}
	$return_data=array($publish_id, $country_cd);
	return $return_data;
}

/**
 * publishに対する同一のcrawl_idを返す
 * crawl_idはchanged_num=recrawl_countになっているもののみを抽出する。
 *
 * @param string $listing_type 'sell'または'rent'
 * @param int $publish_id
 * @return array{int $crawl_id, int $yyyymmdd, str $created}[]
 */
function getSameCrawlIds($listing_type, $publish_id, $country_cd){

	# グローバル変数を参照
	global $pdo_tp;
	global $log;

	$crawl_ids = array();
	$sql = "SELECT /*+INDEX (cl CLEANSING_" . strtoupper($listing_type) . "_CCD_AND_PUBID) INDEX (c CRAWLING_LIST_PK) */ c.crawl_id,c.yyyymmdd,TO_CHAR(c.created," . DB_DATE_FORMAT . ") AS created FROM cleansing_" . $listing_type . " cl INNER JOIN crawling_list c ON cl.crawl_id=c.crawl_id AND cl.changed_num=c.recrawl_count WHERE cl.publish_id=" . $publish_id . " AND cl.country_cd=" . $country_cd . " ORDER BY c.yyyymmdd ASC";
	$stmt = null;
	try {
		$stmt = $pdo_tp->query($sql);
	} catch (PDOException $e) {
		$log->freeform('publish_close_all_3_' . $target_ymd, 'getSameCrawlIdsエラー' . $e->getMessage());
		exit;
	}
	$rows = $stmt->fetchAll();
	#取得したcrawl_idの件数分終了処理をする。
	foreach ($rows as $row) {
		$crawl_id_ymd = array((int)$row['crawl_id'], (int)$row['yyyymmdd'], $row['created']);
		array_push($crawl_ids, $crawl_id_ymd);
	}
	return $crawl_ids;

}

/**
 * crawling_listのstatusを更新する
 *
 * @param array int[] $crawl_ids
 * @param int $status
 * @return void
 */
function updateCrawlStatus($crawl_ids, $status){

	# グローバル変数を参照
	global $pdo_tp;
	global $log;
	global $target_ymd;

	#一括更新用SQL作成
	$in_clause = '';
	for($i=0; $i<count($crawl_ids); $i++){
		if($in_clause != ''){
			$in_clause .= ',';
		}
		$in_clause .= $crawl_ids[$i];
	}
	$sql = "UPDATE crawling_list SET status=" . $status . " WHERE crawl_id IN (" . $in_clause . ")";
$log->freeform('publish_close_all_3_' . $target_ymd, $sql);

	try {
		$stmt = $pdo_tp->query($sql);
	} catch (PDOException $e) {
		$log->freeform('publish_close_all_3_' . $target_ymd, 'updateCrawlStatusエラー' . $e->getMessage());
		exit;
	}
}

/**
 * publishデータが公開かどうかを返す
 *
 * @param string $listing_type 'sell'または'rent'
 * @param int $publish_id
 * @return array{boolean $flg_closed, str $start_datetime} 
 */
function getPublishClosed($listing_type, $publish_id){
	# グローバル変数を参照
	global $stmt_get_publish_closed_rent;
	global $stmt_get_publish_closed_sell;
	global $log;

	$flg_closed = false;
	$start_datetime = '';
	if($listing_type == 'rent'){
        $stmt_get_publish_closed_rent->bindParam(':publish_id', $publish_id, PDO::PARAM_INT);
		try {
			$stmt_get_publish_closed_rent->execute();
			$row = $stmt_get_publish_closed_rent->fetch();
			if($row['status_flg'] == 1){
				$flg_closed = true;
			}
			$start_datetime = $row['start_datetime'];
		} catch (PDOException $e) {
			$log->freeform('publish_close_all_3_' . $target_ymd, 'getPublishClosed(rent)->param:' . $publish_id . '|' . $e->getMessage());
		}
	}else if($listing_type == 'sell'){
        $stmt_get_publish_closed_sell->bindParam(':publish_id', $publish_id, PDO::PARAM_INT);
		try {
			$stmt_get_publish_closed_sell->execute();
			$row = $stmt_get_publish_closed_sell->fetch();
			if($row['status_flg'] == 1){
				$flg_closed = true;
			}
			$start_datetime = $row['start_datetime'];
		} catch (PDOException $e) {
			$log->freeform('publish_close_all_3_' . $target_ymd, 'getPublishClosed(sell)->param:' . $publish_id . '|' . $e->getMessage());
		}
	}
	$arr_return = array($flg_closed, $start_datetime);	
	return $arr_return;
}

/**
 * publishデータを掲載終了にする
 *
 * @param string $listing_type 'sell'または'rent'
 * @param int $publish_id
 * @param int $duplicate_num
 * @param int $status_flg
 * @param int $termination_flg
 * @param int $close_num
 * @param int $close_yyyymmdd
 * @return void
 */
function closePublish($listing_type, $publish_id, $duplicate_num, $close_num, $termination_flg, $close_yyyymmdd){
	
	# グローバル変数を参照
	global $pdo_tp;
	global $log;
	global $target_ymd;

	$format_ymd = 'Ymd H:i:s';
	$close_datetime = DateTime::createFromFormat($format_ymd, $close_yyyymmdd . ' 00:00:00');
	$close_datetime_str = $close_datetime->format('Y/m/d H:i:s');
	$sql = "UPDATE publish_" . $listing_type . " SET duplicate_num=" . $duplicate_num . 
		", close_num=" . $close_num . ", termination_flg=" . $termination_flg . 
		", yyyymmdd=" . $close_yyyymmdd . ",close_datetime=TO_DATE('" . $close_datetime_str . "', " . DB_DATE_FORMAT . ")" .
		", publish_days=TO_NUMBER(TRUNC(TO_DATE('" . $close_datetime_str . "', " . DB_DATE_FORMAT . ")) - TRUNC(start_datetime))" .
		", status_flg=1,complete_datetime=SYSDATE + 9/24 WHERE publish_id=" . $publish_id;
$log->freeform('publish_close_all_3_' . $target_ymd, $sql);
	try {
		$stmt = $pdo_tp->query($sql);
	} catch (PDOException $e) {
		$log->freeform('publish_close_all_3_' . $target_ymd, 'closePublishエラー->' . $e->getMessage());
		exit;
	}

}
/**
 * publishデータを更新する(件数のみ)
 *
 * @param string $listing_type 'sell'または'rent'
 * @param int $publish_id
 * @param int $duplicate_num
 * @param int $close_num
 * @return void
 */
function updatePublishCount($listing_type, $publish_id, $duplicate_num, $close_num){
	# グローバル変数を参照
	global $stmt_update_publish_count_rent;
	global $stmt_update_publish_count_sell;
	global $log;
	global $target_ymd;

	if($listing_type == 'rent'){
        $stmt_update_publish_count_rent->bindParam(':publish_id', $publish_id, PDO::PARAM_INT);
        $stmt_update_publish_count_rent->bindParam(':duplicate_num', $duplicate_num, PDO::PARAM_INT);
        $stmt_update_publish_count_rent->bindParam(':close_num', $close_num, PDO::PARAM_INT);
		try {
			$stmt_update_publish_count_rent->execute();
		} catch (PDOException $e) {
			$log->freeform('publish_close_all_3_' . $target_ymd, 'updatePublishCountエラー(rent)->param:' . $publish_id . '|' . $e->getMessage());
		}
	}else if($listing_type == 'sell'){
        $stmt_update_publish_count_sell->bindParam(':publish_id', $publish_id, PDO::PARAM_INT);
        $stmt_update_publish_count_sell->bindParam(':duplicate_num', $duplicate_num, PDO::PARAM_INT);
        $stmt_update_publish_count_sell->bindParam(':close_num', $close_num, PDO::PARAM_INT);
		try {
			$stmt_update_publish_count_sell->execute();
		} catch (PDOException $e) {
			$log->freeform('publish_close_all_3_' . $target_ymd, 'updatePublishCountエラー(sell)->param:' . $publish_id . '|' . $e->getMessage());
		}
	}
}

/**
 * publishデータの開始日を更新する
 *
 * @param string $listing_type 'sell'または'rent'
 * @param int $publish_id
 * @param int $start_datetime
 * @return void
 */
function updatePublishStart($listing_type, $publish_id, $start_datetime){
	# グローバル変数を参照
	global $pdo_tp;
	global $log;
	global $target_ymd;

	$sql = "UPDATE publish_" . $listing_type . " SET start_datetime=TO_DATE('" . $start_datetime . "', " . DB_DATE_FORMAT . ") WHERE publish_id=" . $publish_id;
$log->freeform('publish_close_all_3_' . $target_ymd, $sql);
	try {
		$stmt = $pdo_tp->query($sql);
	} catch (PDOException $e) {
		$log->freeform('publish_close_all_3_' . $target_ymd, 'updatePublishStartエラー->param:' . $listing_type . '|' . $publish_id . '|' . $e->getMessage());
	}
}

?>
