<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/www/import/config.php";
require '/var/www/import/db_oracle.php';
require "/var/www/import/tools.php";
require "/var/www/import/log.php";

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);
$log = new log();

$target_yyyymmdd=array('20190304','20190306','20190308');
$close_yyyymmdd='20200412';
$listing_type = 'rent';

$sql0 = "SELECT count(publish_id) AS cnt FROM publish_" . $listing_type . " WHERE country_cd=392 AND TO_CHAR(start_datetime, 'YYYYMMDD')=:target_yyyymmdd AND publish_id>50000000";
$stmt0 = $pdo_medium->prepare($sql0);

$sql1 = "SELECT publish_id FROM publish_" . $listing_type . " WHERE country_cd=392 AND TO_CHAR(start_datetime, 'YYYYMMDD')=:target_yyyymmdd AND publish_id>50000000";
$stmt1 = $pdo_medium->prepare($sql1);

$sql2 = "SELECT publish_id, TO_CHAR(min(created), 'YYYY/MM/DD') AS min_created, min(yyyymmdd) AS min_ymd,count(*) AS cnt, sum(CASE WHEN yyyymmdd <= " . $close_yyyymmdd . " THEN 1 ELSE 0 END) AS cnt_close FROM cleansing_" . $listing_type . " cl INNER JOIN crawling_list c ON c.crawl_id=cl.crawl_id WHERE publish_id =:publish_id GROUP BY publish_id";
$stmt2 = $pdo_tp->prepare($sql2);

$sql3 = "UPDATE publish_" . $listing_type . " SET local_flg4=1, status_flg=:status_flg, termination_flg=:termination_flg, duplicate_num=:duplicate_num, close_num=:close_num, start_datetime=TO_DATE(:start_datetime, " . DB_DATE_FORMAT . "), close_datetime=TO_DATE(:close_datetime, " . DB_DATE_FORMAT . "), complete_datetime=SYSDATE + 9/24, publish_days=:publish_days, yyyymmdd=:yyyymmdd WHERE publish_id =:publish_id";
$stmt3 = $pdo_tp->prepare($sql3);

$sql4 = "UPDATE publish_" . $listing_type . " SET local_flg4=2, status_flg=0, termination_flg=0, duplicate_num=:duplicate_num, close_num=:close_num, start_datetime=TO_DATE(:start_datetime, " . DB_DATE_FORMAT . "), close_datetime=null, complete_datetime=null, publish_days=null, yyyymmdd=99999999 WHERE publish_id =:publish_id";
$stmt4 = $pdo_tp->prepare($sql4);

for($i=0; $i <count($target_yyyymmdd); $i++){
	$dt_s = new DateTime();
	$stmt0->bindParam(':target_yyyymmdd', $target_yyyymmdd[$i], PDO::PARAM_STR);
	$stmt0->execute();
	$row0 = $stmt0->fetch();
	$rowcnt = $row0['cnt'];
//echo $rowcnt . "\n";
	$stmt1->bindParam(':target_yyyymmdd', $target_yyyymmdd[$i], PDO::PARAM_STR);
	$stmt1->execute();
	$log->freeform("set_close", $target_yyyymmdd[$i]  . ','  . $rowcnt . ',START');
	for($j=0; $j<$rowcnt; $j++){
		$dt_row_s = new DateTime();
		$row1 = $stmt1->fetch();
		$target_publish_id = $row1['publish_id'];
//echo $target_publish_id . "\n";
		$stmt2->bindParam(':publish_id', $target_publish_id, PDO::PARAM_INT);
		$stmt2->execute();
		$row2 = $stmt2->fetch();
		$min_created = $row2['min_created'];  //YYYY/MM/DD
		$min_ymd = $row2['min_ymd'];  //yyyymmdd
//echo $min_ymd . "\n";
		$cnt = $row2['cnt'];
		$cnt_close = $row2['cnt_close'];
		$status_flg=0;
		$termination_flg=0;
		$start_datetime = new DateTime($min_created);
		$format_ymd = 'Ymd H:i:s';
		$close_datetime = DateTime::createFromFormat($format_ymd, $min_ymd . ' 00:00:00');
//echo $start_datetime->format('Y/m/d H:i:s') . "\n";
//echo $cnt . "\n";
		$publish_days = $close_datetime->diff($start_datetime)->days;
		$yyyymmdd = $min_ymd;
		$start_datetime_str = $start_datetime->format('Y/m/d H:i:s');
		$close_datetime_str = $close_datetime->format('Y/m/d H:i:s');
		if($cnt == 0){
			//何もしないでループを抜ける
			break;
		}else if(($cnt == 1 && $cnt_close == 1) || ($cnt>=2 && $cnt_close>=2)){  //クローズ処理
			$status_flg=1;
			if($cnt==1){
				$termination_flg=2;
			}else{
				$termination_flg=3;
			}
			$stmt3->bindParam(':status_flg', $status_flg, PDO::PARAM_INT);
			$stmt3->bindParam(':termination_flg', $termination_flg, PDO::PARAM_INT);
			$stmt3->bindParam(':duplicate_num', $cnt, PDO::PARAM_INT);
			$stmt3->bindParam(':close_num', $cnt_close, PDO::PARAM_INT);
			$stmt3->bindParam(':start_datetime', $start_datetime_str, PDO::PARAM_STR);
			$stmt3->bindParam(':close_datetime', $close_datetime_str, PDO::PARAM_STR);
			$stmt3->bindParam(':publish_days', $publish_days, PDO::PARAM_INT);
			$stmt3->bindParam(':yyyymmdd', $yyyymmdd, PDO::PARAM_INT);
			$stmt3->bindParam(':publish_id', $target_publish_id, PDO::PARAM_INT);
			try{
				$stmt3->execute();
			}catch (Exception $e) {
				$log->freeform("set_close_".$listing_type."_error", $target_publish_id . ',stmt3,' . $e->getMessage());
			}
		}else{  //継続中処理（カウントupdate、他はデフォルトにセット）
			$close_datetime_str = null;
			$publish_days = null;
			$yyyymmdd = 99999999;
			$stmt4->bindParam(':duplicate_num', $cnt, PDO::PARAM_INT);
			$stmt4->bindParam(':close_num', $cnt_close, PDO::PARAM_INT);
			$stmt4->bindParam(':start_datetime', $start_datetime_str, PDO::PARAM_STR);
			$stmt4->bindParam(':publish_id', $target_publish_id, PDO::PARAM_INT);
			try{
				$stmt4->execute();
			}catch (Exception $e) {
				$log->freeform("set_close_".$listing_type."error", $target_publish_id . ',stmt4,' . $e->getMessage());
			}
		}
		$dt_row_e = new DateTime();
		$dt_row_diff = $dt_row_e->diff($dt_row_s);
		$dt_row_elapsed = ((int)$dt_row_diff->i * 60 + (int)$dt_row_diff->s) . $dt_row_diff->f;
		$log->freeform("set_close_".$listing_type, $target_yyyymmdd[$i] . ',' . $target_publish_id . ',' . $cnt . ',' . $cnt_close . ',' . $start_datetime_str . ',' . $close_datetime_str . ',' . $publish_days . ',' . $yyyymmdd . ',' . $dt_row_elapsed);
	}
	$dt_e = new DateTime();
	$elapsed = $dt_e->diff($dt_s)->format("%H:%i:%s.%f");
	$log->freeform("set_close_".$listing_type, $target_yyyymmdd[$i]  . ','  . $rowcnt . ',[FINISH],' . $elapsed);
	echo $target_yyyymmdd[$i] . ' ';
}

?>
