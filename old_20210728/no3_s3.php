<?php

/** 実行環境から見るので絶対パスで指定 */
require '/var/qc4.propre.com/import/dbclass.php';
require "/var/qc4.propre.com/import/log.php";

$db = DB::getInstance();
$log = new log();

$sql = "SELECT crawl_id,changed_num,s3_path FROM scrape_3 WHERE crawl_id>13640188";
$stmt = $db->query($sql);

$rows = $stmt->fetchAll();

$sql2 = "UPDATE scrape SET s3_path=:s3_path WHERE crawl_id=:crawl_id AND changed_num=:changed_num";
$stmt2 = $db->prepare($sql2);

$sql3 = "UPDATE crawling_list SET status=399 WHERE crawl_id=:crawl_id";
$stmt3 = $db->prepare($sql3);

foreach($rows as $row){
	$log->freeform("no3_s3", $row['crawl_id'] . '/' . $row['changed_num']);
	$stmt2->bindParam(':crawl_id', $row['crawl_id'], PDO::PARAM_INT);
	$stmt2->bindParam(':changed_num', $row['changed_num'], PDO::PARAM_INT);
	$stmt2->bindParam(':s3_path', $row['s3_path'], PDO::PARAM_STR);
	$stmt2->execute();
	$stmt3->bindParam(':crawl_id', $row['crawl_id'], PDO::PARAM_INT);
	$stmt3->execute();
}
#ログ

?>
