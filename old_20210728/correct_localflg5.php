<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/qc4.propre.com/import/config.php";
require '/var/qc4.propre.com/import/dbclass.php';
require "/var/qc4.propre.com/import/log.php";

$db = DB::getInstance();
$log = new log();

/*
住所タグ付与バッチのプログラムミスでlocal_flg5=-1のままになっているrent物件のうち
タグが付いているものを1に修正
タグが付いていないものを-2に修正→要調査
*/

$limit_num=100;

$sql = " select publish_id from propre_development.publish_rent where local_flg5=-1 limit " . $limit_num;

$sql2 = "SELECT count(*) AS cnt FROM propre_development.tag_mapping_rent WHERE tag_id>8000000000 AND publish_id=:publish_id ";
$stmt2 = $db->prepare($sql2);

$sql3 = "UPDATE propre_development.publish_rent SET local_flg5=:local_flg5 WHERE publish_id = :publish_id";
$stmt3 = $db->prepare($sql3);

//for ($i=0; $i<355; $i++){
for ($i=0; $i<4; $i++){
	$stmt = $db->query($sql);
	$rows = $stmt->fetchAll();
	foreach($rows as $row){
		$publish_id = $row['publish_id'];
		$stmt2->bindParam(':publish_id', $publish_id, PDO::PARAM_INT);
		$stmt2->execute();
		$row2 = $stmt2->fetch();
		$cnt = $row2['cnt'];
		$new_local_flg5=-2;
		if($cnt > 0){
			$new_local_flg5=1;
		}
		$stmt3->bindParam(':local_flg5', $new_local_flg5, PDO::PARAM_INT);
		$stmt3->bindParam(':publish_id', $publish_id, PDO::PARAM_INT);
		$stmt3->execute();
		$log->freeform("correct_localflg5", $publish_id . ':' . $new_local_flg5);
	}
	echo $i * $limit_num . '/';
}

?>
