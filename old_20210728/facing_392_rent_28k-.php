<?php

/** 実行ファイルから見たパスで指定 */
require '/var/www/import/config.php';
require '/var/www/import/db_oracle.php';
require '/var/www/import/log.php';
require '/var/www/import/tools.php';

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);
//$pdo_high = DB::getPdo(DB_ORA_TNS_HIGH);
$dt = new DateTimeImmutable();

$log = new log();

/*
 *
 *  facing設定バッチ(country_cd:392のみ)
 *  publish_idのリストから同一クレンジングデータ取得
 *  publishにfacingデータ(local_int2)セット
 *  cleansingにfacingデータ(local_int2)セット
 *  値はmst_facingによるがプログラム内でハードコーディング
 *  東：1
 *  西：2
 *  南：3
 *  北：4
 *  南東：5
 *  北東：6
 *  南西：7
 *  北西：8
 *  ない場合：99
 *  同一物件の場合は文字数の多いもの(南東～北西)、crawl_idの大きい方(後に見つけたURL)を採用
*/

$country_cd=392; //index使用のため国コード指定必須
$listing_type='rent';
$limit_num=1000;
$break_num=100000000;
$loop_max = $break_num / $limit_num;
//$loop_max = 1;
$loop_num=28000; //$limit_num * $loop_num + 1 →スタート値

$table_suffix = $country_cd . '_' . $listing_type; //sqlでも使用
$log_suffix = $country_cd . '_' . $listing_type . '_28k-';

$log->freeform('facing_' . $log_suffix, 'START');

$sql = "SELECT publish_id FROM tmp_localint2_" . $table_suffix . " WHERE id BETWEEN :start_id AND :end_id ORDER BY id";
$stmt = $pdo_tp->prepare($sql);

$sql_get_localint2 = "SELECT cl.crawl_id,cl.changed_num,sc.scraped_data.facing AS facing FROM cleansing_" . $listing_type . " cl LEFT OUTER JOIN scrape sc ON cl.crawl_id=sc.crawl_id AND cl.changed_num=sc.changed_num WHERE cl.publish_id=:publish_id AND cl.country_cd=" . $country_cd . " ORDER BY crawl_id ASC";
$stmt_get_localint2 = $pdo_tp->prepare($sql_get_localint2);

$sql_update_cleansing_localint2 = "UPDATE cleansing_" . $listing_type . " SET local_int2=:local_int2 WHERE crawl_id=:crawl_id AND changed_num=:changed_num";
$stmt_update_cleansing_localint2 = $pdo_tp->prepare($sql_update_cleansing_localint2);

$sql_update_publish_localint2 = "UPDATE publish_" . $listing_type . " SET local_int2=:local_int2 WHERE publish_id=:publish_id";
$stmt_update_publish_localint2 = $pdo_tp->prepare($sql_update_publish_localint2);


while($loop_num < $loop_max){
	$loop_num ++;
	$dt_loop_1 = new DateTimeImmutable();

	$start_id = ($loop_num - 1) * $limit_num + 1;
	$end_id = $loop_num * $limit_num;
//$start_id=582001;
//$end_id =583000;
	$stmt->bindParam(':start_id', $start_id, PDO::PARAM_INT);
	$stmt->bindParam(':end_id', $end_id, PDO::PARAM_INT);
	$stmt->execute();
	$rows = $stmt->fetchAll();
	if(count($rows) == 0){
		exit;
	}

	foreach($rows as $row){
		$target_publish_id = (int)$row['publish_id'];
$log->freeform('facing_' . $log_suffix, 'target_publish_id：'.$target_publish_id);
        $stmt_get_localint2->bindParam(':publish_id', $target_publish_id, PDO::PARAM_INT);
		$stmt_get_localint2->execute();
		$rows2 = $stmt_get_localint2->fetchAll();
		$adopt_localint2 = 0;
		foreach($rows2 as $row2){
			$target_crawl_id = (int)$row2['crawl_id'];
			$target_changed_num = (int)$row2['changed_num'];
			$target_facing = $row2['facing'];
			$target_localint2 = 0;
			if($target_facing=='東'){
				$target_localint2 = 1;
			}else if($target_facing=='西'){
				$target_localint2 = 2;
			}else if($target_facing=='南'){
				$target_localint2 = 3;
			}else if($target_facing=='北'){
				$target_localint2 = 4;
			}else if($target_facing=='南東'){
				$target_localint2 = 5;
			}else if($target_facing=='北東'){
				$target_localint2 = 6;
			}else if($target_facing=='南西'){
				$target_localint2 = 7;
			}else if($target_facing=='北西'){
				$target_localint2 = 8;
			}
			if($adopt_localint2 == 0){  //必ず新しい方を採用
				$adopt_localint2 = $target_localint2;
			}else if($adopt_localint2 >= 1 && $adopt_localint2 <= 4 ){  //1～8なら上書き採用
				if($target_localint2 >=1 && $target_localint2 <=8){
					$adopt_localint2 = $target_localint2;
				}
			}else if($adopt_localint2 >= 5 && $adopt_localint2 <= 8 ){  //5～8なら上書き採用
				if($target_localint2 >=5 && $target_localint2 <=8){
					$adopt_localint2 = $target_localint2;
				}
			}
			//該当クレンジングデータをアップデート
$log->freeform('facing_' . $log_suffix, 'update_cleansing：'.$target_crawl_id. '/'.$target_changed_num.'/'.$target_localint2.'/'.$target_facing);
        	$stmt_update_cleansing_localint2->bindParam(':crawl_id', $target_crawl_id, PDO::PARAM_INT);
        	$stmt_update_cleansing_localint2->bindParam(':changed_num', $target_changed_num, PDO::PARAM_INT);
        	$stmt_update_cleansing_localint2->bindParam(':local_int2', $target_localint2, PDO::PARAM_INT);
			$stmt_update_cleansing_localint2->execute();
		}
		//該当publishデータをアップデート
$log->freeform('facing_' . $log_suffix, 'update_publish：'.$target_publish_id. '/'.$adopt_localint2);
       	$stmt_update_publish_localint2->bindParam(':publish_id', $target_publish_id, PDO::PARAM_INT);
       	$stmt_update_publish_localint2->bindParam(':local_int2', $adopt_localint2, PDO::PARAM_INT);
		$stmt_update_publish_localint2->execute();
	}
	$dt_loop_end = new DateTimeImmutable();
	$elapsed = $dt_loop_end->diff($dt_loop_1)->format("%H:%I:%S.%F");
	$log->freeform('facing_' . $log_suffix, 'ループ終了(' . $loop_num .') -> ' . $elapsed);
	echo $loop_num . '->'. $elapsed . ' ';
}
$dt2 = new DateTime();
$elapsed = $dt2->diff($dt)->format("%H:%I:%S.%F");
$log->freeform('facing_' . $log_suffix, '処理終了 -> ' . $elapsed);

?>
