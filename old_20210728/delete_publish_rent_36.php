<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/www/import/config.php";
require '/var/www/import/db_oracle.php';
require "/var/www/import/tools.php";
require "/var/www/import/log.php";

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);
$log = new log();
$logfile='delete_rent_36';
$log->freeform($logfile, 'START');

$dt_start = new DateTime();

/*
日本とUS以外の全publish関連レコードを削除する
(1)publishテーブルからpublish_id抽出
(2)tag_mapping削除
(3)cleansing削除
(4)publish削除
*/

$country_cd=36;
$limit_num=100;
$target_num=1400000;

$loop_max = $target_num / $target_num;
$loop_num=0;
$dt_loop_in = new DateTime();
$dt_loop_out = new DateTime();
for($i=0; $i<=$target_num/$limit_num; $i++){
	$loop_num++;
	$dt_loop_in = new DateTime();
	$sql = "SELECT publish_id FROM publish_rent WHERE country_cd=" . $country_cd . " AND rownum<=" . $limit_num;
//$log->freeform($logfile, $sql);
	$stmt = $pdo_medium->query($sql);
	$rows = $stmt->fetchAll();
	$in_clause = '';
	$cnt=0;
	foreach ($rows as $row) {
		$cnt++;
		$publish_id = $row['publish_id'];
		if($in_clause != ''){
			$in_clause .= ',';
		}
		$in_clause .= $publish_id;
	}
	if($cnt==0){
$log->freeform($logfile, "exit:loop_num=".$loop_num);
		exit;
	}
	$sql = "DELETE FROM tag_mapping_rent WHERE publish_id IN (" . $in_clause . ")";
//$log->freeform($logfile, $sql);
$dt1 = new DateTime();
	$stmt = $pdo_medium->query($sql);
$dt2 = new DateTime();
$elapsed = $dt2->diff($dt1)->format("%I:%S.%F");
$log->freeform($logfile, $elapsed);
	$sql = "DELETE FROM cleansing_rent WHERE country_cd=" . $country_cd . " AND publish_id IN (" . $in_clause . ")";
//$log->freeform($logfile, $sql);
	$stmt = $pdo_tp->query($sql);
$dt3 = new DateTime();
$elapsed = $dt3->diff($dt2)->format("%I:%S.%F");
$log->freeform($logfile, $elapsed);
	$sql = "DELETE FROM publish_rent WHERE publish_id IN (" . $in_clause . ")";
//$log->freeform($logfile, $sql);
	$stmt = $pdo_tp->query($sql);
$dt4 = new DateTime();
$elapsed = $dt4->diff($dt3)->format("%I:%S.%F");
$log->freeform($logfile, $elapsed);
	$dt_loop_out = new DateTime();
	$elapsed = $dt_loop_out->diff($dt_loop_in)->format("%H:%I:%S.%F");
	$log->freeform($logfile, 'ループ終了(' . $loop_num .') -> ' . $elapsed);
	if($loop_num % 100 == 0){
		echo $loop_num * $limit_num . ' ';
	}

}

$dt_end = new DateTime();
$elapsed = $dt_end->diff($dt_start)->format("%H:%I:%S.%F");
$log->freeform($logfile, '処理終了 -> ' . $elapsed);

?>
