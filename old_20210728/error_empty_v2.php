<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/qc4.propre.com/import/config.php";
require '/var/qc4.propre.com/import/dbclass.php';
require "/var/qc4.propre.com/import/log.php";

$db = DB::getInstance();
$log = new log();

/*
エラー情報を空にする
*/

$limit_num=1000;

$sql = "update crawling_list set error='' where crawl_id between :from AND :to ";
$stmt = $db->prepare($sql);

for ($i=0; $i<55167; $i++){
	try{
		$from = $i * $limit_num;
		$to = $from + $limit_num;
		$stmt->bindParam(':from', $from, PDO::PARAM_INT);
		$stmt->bindParam(':to', $to, PDO::PARAM_INT);
		$stmt->execute();
	}catch (PDOException $e) {
		echo $e->getMessage();
		continue;
	}
	echo $i . '/';
}

?>
