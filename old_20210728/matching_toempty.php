<?php

/** 実行環境から見るので絶対パスで指定 */
require '/var/qc4.propre.com/import/dbclass.php';
require "/var/qc4.propre.com/import/log.php";

$db = DB::getInstance();

/* SQLヒント
update crawling_list set error='' where error=0 and error<>'' limit 100;
mysql> select error from crawling_list where error=0 limit 10;
+-------+
| error |
+-------+
| 00    |
| 00    |
|       |
| 00    |
| 00    |
| 00    |
| 00    |
|       |
| 00    |
| 00    |
+-------+
10 rows in set (0.01 sec)

mysql> select error from crawling_list where error=0 and error<>'' limit 10;
+-------+
| error |
+-------+
| 000   |
| 000   |
| 0000  |
| 000   |
| 00    |
| 00    |
| 0     |
| 00    |
| 00    |
| 0000  |
+-------+
10 rows in set (0.00 sec)
*/

$sql = "UPDATE crawling_list SET matching1=if(matching1='0','',matching1),matching2=if(matching2='0','',matching2),matching3=if(matching3='0','',matching3),matching4=if(matching4='0','',matching4) WHERE crawl_id BETWEEN :start AND :last";
$stmt = $db->prepare($sql);

for ($i=10; $i<502; $i++){
	$start=$i*100000 + 1;
	$last=($i+1)*100000;
	$stmt->bindParam(':start', $start, PDO::PARAM_INT);
	$stmt->bindParam(':last', $last, PDO::PARAM_INT);
	$stmt->execute();
}


#ログ

?>
