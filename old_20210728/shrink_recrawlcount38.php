<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/qc4.propre.com/import/config.php";
require '/var/qc4.propre.com/import/dbclass.php';
require "/var/qc4.propre.com/import/log.php";

$db = DB::getInstance();
$log = new log();

/*
recrawl_countが変更でなく誤って増えてしまっているレコードの修正
13,17,19,26,28,38,44,51の8サイトを対象
recrawl_count >= 1を抽出
対象クロールIDの全スクレイプデータをチェック

scrapeのchanged_numをユニークレコードで詰める
＆cleansingのchanged_numをユニークレコードで詰める
crawling_listのrecrawl_countを詰めたレコード数でセット
publishのchanged_numを詰めたレコード数でセット
*/

$site_no=38;
$limit_num=100;
$target_status='10,11,12,30,40,50,300,310,320,321,330,3311,3320,3330';

$sql = "CREATE TEMPORARY TABLE tmptbl(id INT NOT NULL AUTO_INCREMENT, crawl_id BIGINT NOT NULL, recrawl_count INT NOT NULL, PRIMARY KEY (id))" ;
$stmt = $db->query($sql);

$sql = "INSERT INTO tmptbl (crawl_id,recrawl_count) SELECT crawl_id,recrawl_count FROM crawling_list WHERE site_no=:site_no AND recrawl_count >= 1" ;
$stmt = $db->prepare($sql);
$stmt->bindParam(':site_no', $site_no, PDO::PARAM_INT);
$stmt->execute();

$sql = "SELECT max(id) AS cnt FROM tmptbl" ;
$stmt = $db->query($sql);
$row = $stmt->fetch();
$cnt = $row['cnt'];

$log->freeform("shrink_recrawlcount_" . $site_no, 'count:' . $cnt . '->start!!');

$sql = "SELECT crawl_id,recrawl_count,status FROM crawling_list WHERE crawl_id IN (SELECT crawl_id FROM tmptbl WHERE id BETWEEN :from_no AND :to_no)";
$stmt = $db->prepare($sql);

$sql2 = 'SELECT scraped_data->"$.price1" AS price1, scraped_data->"$.listing_type" AS listing_type, min(changed_num) AS min_changed_num FROM propre.scrape WHERE crawl_id=:crawl_id GROUP BY price1,listing_type ORDER BY min_changed_num ASC';
$stmt2 = $db->prepare($sql2);

/*
sql2で複数スクレイプデータが取得される。（listing_type混在の可能性あり=>変更として記録されている）
+------------+-------------------+-----------------+
| price1     | listing_type      | min_changed_num |
+------------+-------------------+-----------------+
| NULL       | NULL              |               9 |
| "10000000" | "Condos for Sale" |              11 |
| "10500000" | "Condos for Sale" |              81 |
+------------+-------------------+-----------------+
のように取得される。
changed_num:9→0
changed_num:11→1
changed_num:81→2
recrawl_count=2
にする。
(1)i=0
・scrape
  0-8(0=<x<9)削除
  9→0(iの値)
・cleansing_sell/cleansing_rent
  0-8(0=<x<9)削除
  changed_num:9(あれば)→0
(2)i=1
・scrape
  10(9<x<11)削除
  11→1(iの値)
・cleansing_sell/cleansing_rent
  10(9<x<11)削除
  11→1
(3)i=2
・scrape
  12-80(11<x<81)削除
  81→2(iの値)
・cleansing_sell/cleansing_rent
  12-80(11<x<81)削除
  81→2
*/

$sql3 = "DELETE FROM scrape WHERE crawl_id=:crawl_id AND changed_num > :del_from AND changed_num < :del_to";
$stmt3 = $db->prepare($sql3);

$sql4 = "UPDATE scrape SET changed_num=:shrink_num WHERE crawl_id=:crawl_id AND changed_num=:target_num";
$stmt4 = $db->prepare($sql4);

$sql5 = "DELETE FROM cleansing_sell WHERE crawl_id=:crawl_id AND changed_num > :del_from AND changed_num < :del_to";
$stmt5 = $db->prepare($sql5);

$sql6 = "UPDATE cleansing_sell SET changed_num=:shrink_num WHERE crawl_id=:crawl_id AND changed_num=:target_num";
$stmt6 = $db->prepare($sql6);

$sql7 = "DELETE FROM cleansing_rent WHERE crawl_id=:crawl_id AND changed_num > :del_from AND changed_num < :del_to";
$stmt7 = $db->prepare($sql7);

$sql8 = "UPDATE cleansing_rent SET changed_num=:shrink_num WHERE crawl_id=:crawl_id AND changed_num=:target_num";
$stmt8 = $db->prepare($sql8);

$sql9 = "UPDATE crawling_list SET changed_num=:shrink_num WHERE crawl_id=:crawl_id";
$stmt9 = $db->prepare($sql9);

for ($i=0; $i<((int)$cnt -1) / $limit_num + 1; $i++){
	$from_no = $i * $limit_num + 1;
	$to_no = $i * $limit_num + $limit_num;
	$stmt->bindParam(':from_no', $from_no, PDO::PARAM_INT);
	$stmt->bindParam(':to_no', $to_no, PDO::PARAM_INT);
	$stmt->execute();
	$rows = $stmt->fetchAll();
	foreach($rows as $row){
		$csr_num = -1;
		$shrink_num = 0;
		$crawl_id = $row['crawl_id'];
		$recrawl_count = $row['recrawl_count'];
		$status = $row['status'];
		$stmt2->bindParam(':crawl_id', $crawl_id, PDO::PARAM_INT);
		$stmt2->execute();
		$rows2 = $stmt2->fetchAll();
		foreach($rows2 as $row2){
			$price1 = $row2['price1'];
			$listing_type = $row2['listing_type'];
			$min_changed_num = $row2['min_changed_num'];
			$stmt3->bindParam(':crawl_id', $crawl_id, PDO::PARAM_INT);
			$stmt3->bindParam(':del_from', $csr_num, PDO::PARAM_INT);
			$stmt3->bindParam(':del_to', $min_changed_num, PDO::PARAM_INT);
			$stmt3->execute();
			$stmt4->bindParam(':crawl_id', $crawl_id, PDO::PARAM_INT);
			$stmt4->bindParam(':target_num', $min_changed_num, PDO::PARAM_INT);
			$stmt4->bindParam(':shrink_num', $shrink_num, PDO::PARAM_INT);
			$stmt4->execute();
			$stmt5->bindParam(':crawl_id', $crawl_id, PDO::PARAM_INT);
			$stmt5->bindParam(':del_from', $csr_num, PDO::PARAM_INT);
			$stmt5->bindParam(':del_to', $min_changed_num, PDO::PARAM_INT);
			$stmt5->execute();
			$stmt6->bindParam(':crawl_id', $crawl_id, PDO::PARAM_INT);
			$stmt6->bindParam(':target_num', $min_changed_num, PDO::PARAM_INT);
			$stmt6->bindParam(':shrink_num', $shrink_num, PDO::PARAM_INT);
			$stmt6->execute();
			$stmt7->bindParam(':crawl_id', $crawl_id, PDO::PARAM_INT);
			$stmt7->bindParam(':del_from', $csr_num, PDO::PARAM_INT);
			$stmt7->bindParam(':del_to', $min_changed_num, PDO::PARAM_INT);
			$stmt7->execute();
			$stmt8->bindParam(':crawl_id', $crawl_id, PDO::PARAM_INT);
			$stmt8->bindParam(':target_num', $min_changed_num, PDO::PARAM_INT);
			$stmt8->bindParam(':shrink_num', $shrink_num, PDO::PARAM_INT);
			$stmt8->execute();
			$stmt8->bindParam(':crawl_id', $crawl_id, PDO::PARAM_INT);
			$stmt8->bindParam(':shrink_num', $shrink_num, PDO::PARAM_INT);
			$stmt8->execute();
			$log->freeform("shrink_recrawlcount_" . $site_no, $crawl_id . ':' . $min_changed_num . '->' . $shrink_num);
			$csr_num = $min_changed_num;
			$shrink_num ++;
		}
	}
	if($i % 100 == 0){
		echo ($i * $limit_num) . '/' . $cnt . ' ';
	}
}

?>
