<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/www/import/config.php";
require '/var/www/import/db_oracle.php';
require "/var/www/import/tools.php";
require "/var/www/import/log.php";

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);
$log = new log();

/*
USDタグの付け直しのため1015,1016を全部削除
*/

$tag_id=1016;
$listing_type="sell";
$limit_num=1000;
$target_num=42974000;
/*
rent1015:
rent1016:
sell1015:42974000
sell1016:
*/

echo $target_num/$limit_num . ":start";
$sql = "INSERT INTO tag_mapping_".$listing_type." SELECT 1016,0,publish_id,meshcode1,meshcode2,meshcode3,meshcode6 FROM tmp_tag_".$listing_type." WHERE id BETWEEN :start_id AND :end_id AND floor_sqft>0";
$stmt = $pdo_tp->prepare($sql);
for($i=0; $i<=$target_num/$limit_num; $i++){
	$start_id = $i * $limit_num + 1;
	$end_id = ($i + 1) * $limit_num;
	$stmt->bindParam(':start_id', $start_id, PDO::PARAM_INT);
	$stmt->bindParam(':end_id', $end_id, PDO::PARAM_INT);
	$stmt->execute();
	if($i % 10 == 0){
		echo $i."/";
	}
}

?>
