<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/www/import/config.php";
require '/var/www/import/db_oracle.php';
require "/var/www/import/tools.php";
require "/var/www/import/log.php";

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);

/*
*/

$listing_type='sell';
$limit_num=1000;
$target_num=22657;

$log = new log();
$logfile='delete_duplicate_1016_'.$listing_type;
$log->freeform($logfile, 'START');
$dt_start = new DateTime();

$loop_max = $target_num / $target_num;
$loop_num=0;
$dt_loop_in = new DateTime();
$dt_loop_out = new DateTime();
//対象publish_id抽出
$sql = "SELECT publish_id FROM tmp_ryota_1016_" . $listing_type . " WHERE  id BETWEEN :start_id AND :end_id";
$stmt = $pdo_tp->prepare($sql);
//1件ずつ処理する
$sql1 = "DELETE /*+INDEX(a TAG_MAPPING_" . $listing_type . "_I03)*/ FROM tag_mapping_" . $listing_type . " WHERE publish_id=:publish_id AND tag_id=1016";
$sql2 = "INSERT INTO tag_mapping_" . $listing_type . " SELECT 1016,floor(c.exchange_rate * p.price * 100),p.publish_id,p.meshcode1,p.meshcode2,p.meshcode3,p.meshcode6 FROM publish_" . $listing_type . " p " .
		" INNER JOIN mst_currency c ON p.currency_cd = c.currency_cd " .
		" WHERE p.publish_id=:publish_id AND p.price>0 AND p.floor_sqft>0" ;
$stmt1 = $pdo_tp->prepare($sql1);
$stmt2 = $pdo_tp->prepare($sql2);

for($i=0; $i<=$target_num/$limit_num; $i++){
	$loop_num++;
	$dt_loop_in = new DateTime();
	$start_id = ($loop_num - 1) * $limit_num + 1;
	$end_id = $loop_num * $limit_num;
	$stmt->bindParam(':start_id', $start_id, PDO::PARAM_INT);
	$stmt->bindParam(':end_id', $end_id, PDO::PARAM_INT);
	$stmt->execute();
	$dt_loop_exec = new DateTime();
	$elapsed = $dt_loop_exec->diff($dt_loop_in)->format("%I:%S.%F");
	$log->freeform($logfile, 'ループ開始(' . $loop_num .'件) publish_id取得時間-> ' . $elapsed);
	$rows = $stmt->fetchAll();
	foreach ($rows as $row) {
		$publish_id = $row['publish_id'];
		if($publish_id != '1'){
			$stmt1->bindParam(':publish_id', $publish_id, PDO::PARAM_INT);
			$stmt2->bindParam(':publish_id', $publish_id, PDO::PARAM_INT);
			$dt1 = new DateTime;
			$stmt1->execute();
			$dt2 = new DateTime;
			$stmt2->execute();
			$dt3 = new DateTime;
			$elapsed1 = $dt2->diff($dt1)->format("%I:%S.%F");
			$elapsed2 = $dt3->diff($dt2)->format("%I:%S.%F");
			$log->freeform($logfile, $publish_id . '/' . $elapsed1 . '/' . $elapsed2);
		}
	}
	$dt_loop_out = new DateTime();
	$elapsed = $dt_loop_out->diff($dt_loop_in)->format("%H:%I:%S.%F");
	$log->freeform($logfile, 'ループ終了(' . $loop_num .'件) -> ' . $elapsed);
	if($loop_num % 10 == 0){
		echo $loop_num * $limit_num . ' ';
	}
}
$dt_end = new DateTime();
$elapsed = $dt_end->diff($dt_start)->format("%H:%I:%S.%F");
$log->freeform($logfile, '処理終了 -> ' . $elapsed);

?>
