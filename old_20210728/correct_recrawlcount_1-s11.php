<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/qc4.propre.com/import/config.php";
require '/var/qc4.propre.com/import/dbclass.php';
require "/var/qc4.propre.com/import/log.php";

$db = DB::getInstance();
$log = new log();

/*
スクレイプデータの紐付けを修正する
0,100以外の全レコードからスクレイプデータ（s3/scraped_data）をチェック、ステータスとrecrawl_countを再付番する
スクレイプデータの紐付けを修正する
scrapeにレコードなし→3311
scrapeにレコードあり、スクレイプデータ（s3）のみあり→3320
scrapeにレコードあり、スクレイプデータ（scraped_data）あり→3330
当日のpicker完了後に
crawl_setting で対象サイトのspecified_crawlerに文字列などを入れてdequeues対象外にしてから実行する
処理完了後、
status=3311 → 当日分は0、過去は200など
status=3320 → 優先順の高い順（当日分）から40→スクレイプ＆クレンジングへ
status=3330 → 優先順の高い順（当日分）から50→クレンジングへ
*/

$today=20190831;
$site_no=1;
$limit_num=100;

#$sql = "SELECT count(*) AS cnt FROM crawling_list WHERE site_no=:site_no AND yyyymmdd>=20190807 AND status < 3000";
#全件対象に変更
# status < 3000 AND status<>100 AND status<>0 だと同じrangeを使いながら20secくらいかかるので修正
$sql = "SELECT count(*) AS cnt FROM crawling_list WHERE site_no=:site_no AND status = 11";
$stmt = $db->prepare($sql);
$stmt->bindParam(':site_no', $site_no, PDO::PARAM_INT);
$stmt->execute();
$row = $stmt->fetch();
$cnt = $row['cnt'];

#$sql = "SELECT crawl_id,recrawl_count,status FROM crawling_list WHERE site_no=:site_no AND yyyymmdd>=20190807 AND status < 3000 LIMIT :limit_num";
$sql = "SELECT crawl_id,recrawl_count,status,yyyymmdd FROM crawling_list WHERE site_no=:site_no AND status = 11 LIMIT :limit_num";
$stmt = $db->prepare($sql);

$sql2 = 'SELECT changed_num,s3_path,scraped_data,scraped_data->"$.price1" AS price1 FROM scrape WHERE crawl_id = :crawl_id ORDER BY changed_num DESC LIMIT 1';
$stmt2 = $db->prepare($sql2);

$sql3 = "UPDATE crawling_list SET recrawl_count=:recrawl_count,status=:status WHERE crawl_id = :crawl_id";
$stmt3 = $db->prepare($sql3);

for ($i=0; $i<((int)$cnt -1) / $limit_num + 1; $i++){
	$stmt->bindParam(':site_no', $site_no, PDO::PARAM_INT);
	$stmt->bindParam(':limit_num', $limit_num, PDO::PARAM_INT);
	$stmt->execute();
	$rows = $stmt->fetchAll();
	foreach($rows as $row){
		$crawl_id = $row['crawl_id'];
		$recrawl_count = $row['recrawl_count'];
		$status = $row['status'];
		$yyyymmdd = $row['yyyymmdd'];
		$stmt2->bindParam(':crawl_id', $crawl_id, PDO::PARAM_INT);
		$stmt2->execute();
		$row2 = $stmt2->fetch();
		$changed_num = null;
		$s3_path = null;
		$scraped_data = null;
		$price1 = null;
		if($row2 != null){
			$changed_num = $row2['changed_num'];
			$s3_path = $row2['s3_path'];
			$scraped_data = $row2['scraped_data'];
			$price1 = $row2['price1'];
		}
		$new_status=3999;
		$new_recrawl_count=0;
		if($changed_num !== null){
			//yyyymmdd>=20190831でrecrawl_count>max(changed_num)の場合は最新のscrapeなしということでクロールに回してもらいます（3311）
			if($recrawl_count > $changed_num && $yyyymmdd>=$today){
				$new_recrawl_count = $changed_num + 1; //歯抜けをなくして再付番
				$new_status=3311;  //scrapeにレコードなし扱い
			}else{
				$new_recrawl_count=$changed_num;
			}
		}
		if($new_status != 3311){
			if(strlen($scraped_data) > 10){
				$new_status=3330;  //scrapeにレコードあり、スクレイプデータ(scrape_data)あり
			}else if(strlen($s3_path) > 10){
				$new_status=3320;  //scrapeにレコードあり、スクレイプデータなし（クロールデータs3のみ）
			}else{
				$new_status=3311;  //scrapeにレコードなし
			}
		}
		$stmt3->bindParam(':crawl_id', $crawl_id, PDO::PARAM_INT);
		$stmt3->bindParam(':recrawl_count', $new_recrawl_count, PDO::PARAM_INT);
		$stmt3->bindParam(':status', $new_status, PDO::PARAM_INT);
		$stmt3->execute();
		$log->freeform("correct_recrawlcount_1-s11", $crawl_id . ':' . $recrawl_count . '->' . $new_recrawl_count . '/' . $status . '->' . $new_status);
	}
	if($i % 100 == 0){
		echo ($i * $limit_num) . '/' . $cnt . ' ';
	}
}

?>
