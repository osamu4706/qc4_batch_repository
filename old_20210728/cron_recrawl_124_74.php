<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/www/import/config.php";
require '/var/www/import/db_oracle.php';
require "/var/www/import/tools.php";
require "/var/www/import/log.php";

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);
$log = new log();

/*
cronで定期的に稼働することが前提のプログラム
日付ごとにステータスを変えていく
3330→0(クロール＆スクレイプ＆クレンジング)
(0)一旦、0のものがあれば、全て3330にする（DBクエリで）
(1)0のmin(created)を取得
(2)min(created)を対象に3330→0を全件実行
これ不要　(3)→40の場合crawl_setting.rescrape_num=1,crawl_setting.recleansing_num=0にセット
*/

$logfile='recrawl_124-74';
//$site_no=7;
$site_no = array(74);
$site_no_sql = '';
if(count($site_no)==0){
	exit;
}else if(count($site_no)==1){
	$site_no_sql = ' = ' . $site_no[0] . ' ';
}else{
	$site_no_sql = ' in(';
	for($i=0; $i<count($site_no); $i++){
		if($i>0){
			$site_no_sql .= ',';
		}
		$site_no_sql .= $site_no[$i];
	}
	$site_no_sql .= ') ';
}
/*ターゲットのsite_noを決めて日付取得、min日付ごとにセットで回す必要あり*/

$limit_num=1000;

$from_status=9999;
$to_status=9999;

//$sql = "SELECT count(*) AS cnt FROM crawling_list WHERE site_no=" . $site_no . " AND status in (40,50)";
$sql = "SELECT  /*+INDEX(a CRAWLING_LIST_I01)*/ count(*) AS cnt FROM crawling_list WHERE site_no " . $site_no_sql . " AND status = 0";
//echo $sql;

$stmt = $pdo_medium->query($sql);

$row = $stmt->fetch();

if($row['cnt'] > 0){
	//前のバッチでの修正分の再スクレイプが完了していないため終了
	$log->freeform($logfile, '前バッチ分のクロール処理完了待ちで終了');
	exit;
}

$sql_sessioncheck = 'SELECT count(*) AS sesscnt FROM v$session s, v$sqlarea a, v$process p ' .
	"WHERE s.SCHEMANAME = 'PROPRE' AND s.PREV_HASH_VALUE = a.hash_value AND s.PREV_SQL_ADDR = a.address AND s.paddr = p.addr AND a.sql_text like '%3330%'";
$stmt_sessioncheck = $pdo_tp->prepare($sql_sessioncheck);
$stmt_sessioncheck->execute();
$row_sessioncheck = $stmt_sessioncheck->fetch();
$sesscnt = $row_sessioncheck['sesscnt'];
$log->freeform($logfile, '起動時session数(3330)：' . $sesscnt);
if($sesscnt > 10){
	//セッション数10以上ならバッチ終了
	//sleep(300);
	$log->freeform($logfile, '起動時セッション数によりバッチ終了');
	exit;
}

$min_3330 = 99999999;

//site_no in()だとrange scanになって実行に時間がかかるので1サイトずつチェック
//$min_50 = 99999999;
$sql = "SELECT /*+INDEX(a CRAWLING_LIST_I01)*/ min(TO_CHAR(created, 'YYYYMMDD')) AS created_yyyymmdd FROM crawling_list WHERE site_no=:site_no AND status = 3330";
$stmt = $pdo_medium->prepare($sql);
for($i=0; $i<count($site_no); $i++){
	$stmt->bindParam(':site_no', $site_no[$i], PDO::PARAM_INT);
	$stmt->execute();
	$row = $stmt->fetch();
	if(isset($row['created_yyyymmdd'])){
		if($min_3330 >$row['created_yyyymmdd']){
			$min_3330 = $row['created_yyyymmdd'];
		}
	}
}

/* 日付範囲でスクレイプ内容を変更する
①20190924～20200702までの分:旧版で対応
②2020703～20201128までのの分:旧版で対応
③20201129～現在の分:新版で対応
*/
/*
if($min_40 >= 20201128){
	$log->freeform($logfile, '20201128までのバッチ終了');
	exit;
}
*/

$ymd=99999999;

//$log->freeform($logfile, $min_40 . ':' . $min_50);

if($min_3330 == 99999999){
	//両方結果なし
	$log->freeform($logfile, '再クロール全処理完了！');
//	$sql = "UPDATE crawl_setting SET rescrape_num=0, recleansing_num=0 WHERE site_no" . $site_no_sql;
//	$stmt = $pdo_tp->query($sql);
	exit;
}else{
	$sql2="";
	$from_status='(3330)';
	$to_status=0;
	$ymd = $min_3330;

	$sql = "SELECT  /*+INDEX(a CRAWLING_LIST_I01)*/ site_no,count(*) AS cnt FROM  crawling_list WHERE site_no" . $site_no_sql . " AND status in " . $from_status . " AND TO_CHAR(created, 'YYYYMMDD')=:ymd GROUP BY site_no";
	$stmt = $pdo_medium->prepare($sql);
	$stmt->bindParam(':ymd', $ymd, PDO::PARAM_STR);
	$stmt->execute();
	$rows = $stmt->fetchAll();
	//同一日でヒットしたサイトの数だけ回す
	foreach ($rows as $row) {
		$target_site_no = $row['site_no'];
		$cnt = $row['cnt'];
		$sql = "UPDATE  /*+INDEX(a CRAWLING_LIST_I01)*/ crawling_list SET status=" . $to_status . " WHERE site_no=" . $target_site_no . " AND status in " . $from_status . " AND TO_CHAR(created, 'YYYYMMDD')=:ymd AND rownum<=:limit_num";
		$stmt = $pdo_medium->prepare($sql);
		for ($i=0; $i<(int)(($cnt -1) / $limit_num) + 1; $i++){
			$stmt->bindParam(':limit_num', $limit_num, PDO::PARAM_INT);
			$stmt->bindParam(':ymd', $ymd, PDO::PARAM_STR);
			$stmt->execute();
			$log->freeform($logfile, $ymd . ':' . $to_status . '->' . ($i + 1)*$limit_num . '/' . $cnt);
//			if($i ==0){
//				$sql2 = "UPDATE crawl_setting SET rescrape_num=0, recleansing_num=0 WHERE site_no=" . $target_site_no;
//				$stmt2 = $pdo_tp->query($sql2);  //rescrape_num,recleansing_num数は1件目の時点で変えてすぐにdequeues処理に入れるようにする
//				$log->freeform($logfile, 'crawl_setting変更完了');
//			}
		}
		$log->freeform($logfile, $ymd . '(' . $target_site_no . '):' . $to_status . '->' . $cnt . '件ステータス変更(再クロール指示)処理完了');
	}
}
exit;
?>
