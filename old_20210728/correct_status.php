<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/qc4.propre.com/import/config.php";
require '/var/qc4.propre.com/import/dbclass.php';
require "/var/qc4.propre.com/import/log.php";

$db = DB::getInstance();
$log = new log();

/*
未クロールも一緒に330にしてしまったクロール対象のステータス修正
status:12,50,320,330に対して、
yyyymmdd>=20190810
  スクレイプデータなし→0
  スクレイプデータ（s3）あり→40
  スクレイプデータ（scraped_data）あり→3050
yyyymmdd<20190810→3330
  スクレイプデータなし→3000 → 後ほど篠原さん確認
  スクレイプデータ（s3）あり→3320
  スクレイプデータ（scraped_data）あり→3330
crawl_setting で対象サイトのspecified_crawlerに文字列などを入れてdequeues対象外にしてから実行する
処理完了後、
status=3050 → 50
status=3320 → 320
status=3330 → 330
に変更、
status=3000 → 過去のクロールデータなし：後で確認する！
*/

$site_no=10;
$limit_num=100;
//$limit_num=1; //稼働テスト用
$yyyymmdd=20190810; //クロール完了しているはずの(昨日あたりの)yyyymmdd

$sql = "SELECT count(*) AS cnt FROM crawling_list WHERE site_no=:site_no AND status in(12,50,320,330)";
$stmt = $db->prepare($sql);
$stmt->bindParam(':site_no', $site_no, PDO::PARAM_INT);
$stmt->execute();
$row = $stmt->fetch();
$cnt = $row['cnt'];
//$cnt = 10; //稼働テスト用

$sql = "SELECT crawl_id,recrawl_count,status,yyyymmdd FROM crawling_list WHERE site_no=:site_no AND status in(12,50,320,330) LIMIT :limit_num";
$stmt = $db->prepare($sql);

$sql2 = "SELECT s3_path,scraped_data FROM scrape WHERE crawl_id = :crawl_id AND changed_num=:changed_num";
$stmt2 = $db->prepare($sql2);

$sql3 = "UPDATE crawling_list SET status=:status WHERE crawl_id = :crawl_id";
$stmt3 = $db->prepare($sql3);

for ($i=0; $i<((int)$cnt -1) / $limit_num + 1; $i++){
	$stmt->bindParam(':site_no', $site_no, PDO::PARAM_INT);
	$stmt->bindParam(':limit_num', $limit_num, PDO::PARAM_INT);
	$stmt->execute();
	$rows = $stmt->fetchAll();
	foreach($rows as $row){
		$crawl_id = (int)$row['crawl_id'];
		$recrawl_count = (int)$row['recrawl_count'];
		$status = (int)$row['status'];
		$target_yyyymmdd = (int)$row['yyyymmdd'];
		$stmt2->bindParam(':crawl_id', $crawl_id, PDO::PARAM_INT);
		$stmt2->bindParam(':changed_num', $recrawl_count, PDO::PARAM_INT);
		$stmt2->execute();
		$row2 = $stmt2->fetch();
		$s3_path = $row2['s3_path'];
		$scraped_data = $row2['scraped_data'];
		$new_status=3000;
		if($target_yyyymmdd >= $yyyymmdd){  //優先処理
			if($s3_path == null){
				$new_status=0;  //スクレイプデータなし
			}else{
				if(strlen($scraped_data) > 10){
					$new_status=3050;  //スクレイプデータあり
				}else if(strlen($s3_path) > 10){
					$new_status=40;  //スクレイプデータなし（クロールデータs3のみ）
				}
				//上記以外（scrapeレコードはあるけど内容なし）は要調査：3000
			}
		}else{  //一旦保留
			if($s3_path == null){
				$new_status=3000;  //過去データでスクレイプデータなし→要調査：3000（掲載終了：200？）
			}else{
				if(strlen($scraped_data) > 10){
					$new_status=3330;  //スクレイプデータあり
				}else if(strlen($s3_path) > 10){
					$new_status=3320;  //スクレイプデータなし（クロールデータs3のみ）
				}
				//上記以外（scrapeレコードはあるけど内容なし）は要調査：3000
			}
		}
		$stmt3->bindParam(':crawl_id', $crawl_id, PDO::PARAM_INT);
		$stmt3->bindParam(':status', $new_status, PDO::PARAM_INT);
		$stmt3->execute();
		$log->freeform("correct_status", $crawl_id . ':' . $status . '->' . $new_status);
	}
}

?>
