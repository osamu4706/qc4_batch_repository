<?php

/** 実行ファイルから見たパスで指定 */
require __DIR__.'/../import/config.php';
require __DIR__.'/../import/db_oracle.php';
require __DIR__.'/../import/log.php';


# エラーメールの送付先
define('TO_MAIL_ADDR', 'develop@propre.com');
# 一度の処理件数
//define('PROCESS_NUM', 500);

# グローバル変数
$pdo = DB::getPdo(DB_ORA_TNS_TPURGENT);
//$pdo = DB::getPdo(DB_ORA_TNS_MEDIUM);
$log = new log();

# バッチ処理のトータル時間
$time_start_batch = microtime(true);
$log->freeform("null_update_rent", "null_update_sell.batch start");

//$tag_id = 1016;
//$time_start = microtime(true);

## tag_id が 1016 のデータの更新処理
//$sql_1016 = "MERGE /*+ PARALLEL(12) */
$sql_1016 = "MERGE /*+ PARALLEL(18) */ 
INTO tag_mapping_rent
  USING (
    SELECT
       1016 tag_id
      , case when floor(
        publish_rent.unit_price * mst_currency.exchange_rate * 100
      ) is null then 0
      else  floor(
        publish_rent.unit_price * mst_currency.exchange_rate * 100
      ) end tag_level
      , publish_rent.publish_id
      , publish_rent.meshcode1
      , publish_rent.meshcode2
      , publish_rent.meshcode3
      , publish_rent.meshcode6
    FROM
      publish_rent
      LEFT JOIN tag_mapping_rent
        ON (tag_mapping_rent.publish_id = publish_rent.publish_id and tag_id = 1016)
      LEFT JOIN mst_currency
        ON publish_rent.currency_cd = mst_currency.currency_cd
    WHERE
      (tag_mapping_rent.tag_level IS NULL or tag_mapping_rent.tag_level = 0)
      and rownum < 1000
      --	AND publish_rent.currency_cd != 'USD'
      and publish_rent.unit_price <> 0
      and publish_rent.unit_price is not null
      and (publish_rent.unit_price * mst_currency.exchange_rate * 100) > 1
      and publish_rent.unit_price < 999999999
  ) aa
    ON (
      tag_mapping_rent.tag_id = aa.tag_id
      AND tag_mapping_rent.publish_id = aa.publish_id
     -- AND tag_mapping_rent.publish_id = aa.publish_id
    ) WHEN MATCHED THEN UPDATE
SET
  tag_mapping_rent.tag_level = aa.tag_level WHEN NOT MATCHED THEN
INSERT (
  tag_id
  , tag_level
  , publish_id
  , meshcode1
  , meshcode2
  , meshcode3
  , meshcode6
)
VALUES (
  aa.tag_id
  , aa.tag_level
  , aa.publish_id
  , aa.meshcode1
  , aa.meshcode2
  , aa.meshcode3
  , aa.meshcode6
)";
//echo "{$sql}\n";
//$log->freeform("null_update_rent", $sql_1016);

## tag_id が 1015 のデータの更新処理
//$sql_1015 = "MERGE /*+ PARALLEL(12) */
$sql_1015 = "MERGE /*+ PARALLEL(18) */
INTO tag_mapping_rent 
  USING ( 
    SELECT
       1015 tag_id
      , case when ( 
        publish_rent.price * mst_currency.exchange_rate
      ) is null then 0
      else  ( 
        publish_rent.price * mst_currency.exchange_rate
      ) end tag_level
      , publish_rent.publish_id
      , publish_rent.meshcode1
      , publish_rent.meshcode2
      , publish_rent.meshcode3
      , publish_rent.meshcode6 
    FROM
      publish_rent  
      LEFT JOIN tag_mapping_rent 
        ON (tag_mapping_rent.publish_id = publish_rent.publish_id and tag_id = 1015)
      LEFT JOIN mst_currency 
        ON publish_rent.currency_cd = mst_currency.currency_cd 
    WHERE
      (tag_mapping_rent.tag_level IS NULL or tag_mapping_rent.tag_level = 0)
      and rownum < 1000
      -- AND publish_rent.currency_cd != 'USD'
      and publish_rent.price <> 0
      and publish_rent.price is not null
      and (publish_rent.price * mst_currency.exchange_rate) > 1
      and publish_rent.price < 999999999
  ) aa 
    ON ( 
      tag_mapping_rent.tag_id = aa.tag_id 
      AND tag_mapping_rent.publish_id = aa.publish_id    
    ) WHEN MATCHED THEN UPDATE 
SET
  tag_mapping_rent.tag_level = aa.tag_level WHEN NOT MATCHED THEN 
INSERT ( 
  tag_id
  , tag_level
  , publish_id
  , meshcode1
  , meshcode2
  , meshcode3
  , meshcode6
) 
VALUES ( 
  aa.tag_id
  , aa.tag_level
  , aa.publish_id
  , aa.meshcode1
  , aa.meshcode2
  , aa.meshcode3
  , aa.meshcode6
)";
//echo "{$sql}\n";
//$log->freeform("null_update_rent", $sql_1015);


# 設定された回数ずつ処理を実行する
# 例外が発生した場合も、処理を継続する Exit しない
$loop_cnt = 10000;		// ループの回数
for ($i=1; $i<=$loop_cnt; $i++) {

	//echo "/" . $i; //これは進行をチェックするログ（画面に出力）
	$Msg = "{$i}";

	// 1016 のデータを更新するSQLを実行する
    try {
        $time_start = microtime(true);
		$stmt_1016 = $pdo->query($sql_1016);
        $time = microtime(true) - $time_start;
        $Msg = $Msg.",".sprintf("%.20f", $time);
    } catch (PDOException $e) {
		$msg = "MERGE(1016):".$e->getMessage();
        $log->freeform("null_update_rent", $msg);
        sendErrMail($msg);
		$Msg = $Msg.",".sprintf("ERROR");
    }

	// 1015 のデータを更新するSQLを実行する
	try {
		$time_start = microtime(true);
		$stmt_1015 = $pdo->query($sql_1015);
		$time = microtime(true) - $time_start;
		$Msg = $Msg.",".sprintf("%.20f", $time);

	} catch (PDOException $e) {
		$msg = "MERGE(1015):".$e->getMessage();
		$log->freeform("null_update_rent", $msg);
		sendErrMail($msg);
		$Msg = $Msg.",".sprintf("ERROR");
	}

	// ループが終わるごとに、処理時間をログに出力する
	$log->freeform("null_update_time_rent", $Msg);
}

/**
 * Errorメール送信処理：　例外発生時に、管理者へエラーメールを送信する。
 *
 */
function sendErrMail($body)
{

	mb_language("Japanese");
	mb_internal_encoding("UTF-8");


	$to = TO_MAIL_ADDR;     //エラーメールの送付先
	$header = "From: $to\nReply-To: $to\n";
	$title = '【ERROR】USD価格、NULLデータ再更新変更バッチ';

	try {
		mb_send_mail($to, $title, $body, $header);
		//echo "処理エラーメールを送信しました";
		return true;
	} catch (Exception $e) {
		//echo "エラーメールの送信に失敗しました";
		return false;
	}
}

# トータルの処理時間を取得
$time_total = microtime(true) - $time_start_batch;

#log
$log->freeform("null_update_rent", "total time:{$time_total} end");


?>