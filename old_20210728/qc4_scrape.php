<?php

/** 実行環境から見るので絶対パスで指定 */
require '/var/qc4.propre.com/import/dbclass.php';
require "/var/qc4.propre.com/import/log.php";

$db = DB::getInstance();

$sql = "SELECT crawl_id,recrawl_count FROM crawling_list WHERE site_no not in(1,3,31) AND crawl_id BETWEEN :start AND :last";
$stmt = $db->prepare($sql);

for ($i=1; $i<203; $i++){
	$start=$i*100000 + 1;
	$last=($i+1)*100000;
	$stmt->bindParam(':start', $start, PDO::PARAM_INT);
	$stmt->bindParam(':last', $last, PDO::PARAM_INT);
	$stmt->execute();

$rows = $stmt->fetchAll();

$sql2 = 'SELECT *,scraped_data->"$.price1" AS m1 FROM qc4test_scrape WHERE crawl_id=:crawl_id ORDER BY changed_num ASC';
$stmt2 = $db->prepare($sql2);

$sql3 = 'INSERT INTO scrape_batch VALUES (:crawl_id,:changed_num,:crawler_id,:site_no,:s3_path,:scraped_data,:publish_id,TO_DATE(:reg_datetime, '.DB_DATE_FORMAT.'))';
$stmt3 = $db->prepare($sql3);

foreach($rows as $row){
	$stmt2->bindParam(':crawl_id', $row['crawl_id'], PDO::PARAM_INT);
	$stmt2->execute();
	$rows2 = $stmt2->fetchAll();
	$cnt=0;
	$m1=0;
	foreach($rows2 as $row2){
		if($cnt==0 || ($row2['m1'] != $m1)){  #1件目は登録
			$stmt3->bindParam(':crawl_id', $row2['crawl_id'], PDO::PARAM_INT);
			$stmt3->bindParam(':changed_num', $row2['changed_num'], PDO::PARAM_INT);
			$stmt3->bindParam(':crawler_id', $row2['crawler_id'], PDO::PARAM_INT);
			$stmt3->bindParam(':site_no', $row2['site_no'], PDO::PARAM_INT);
			$stmt3->bindParam(':s3_path', $row2['s3_path'], PDO::PARAM_STR);
			$stmt3->bindParam(':scraped_data', $row2['scraped_data'], PDO::PARAM_STR);
			$stmt3->bindParam(':publish_id', $row2['publish_id'], PDO::PARAM_INT);
			$stmt3->bindParam(':reg_datetime', $row2['reg_datetime'], PDO::PARAM_STR);
			$stmt3->execute();
		}
		$cnt++;
		$m1 = $row2['m1'];
	}
}

}

#ログ

?>
