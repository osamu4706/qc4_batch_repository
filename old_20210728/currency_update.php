<?php

/**
 * currency_update.php　(USD価格変更バッチ)
 *
 * mst_currencyの通貨レートに従い、物件の対USD価格情報を更新する。
 * 引数として、「更新対象通貨コード」と、「売買または賃貸」の情報を指定する。
 *
 * バッチの処理概要は以下の通り
 * ① バッチ実行時の引数にあわせ、mst_currencyから通貨レートを取得
 * ② テンポラリテーブルの作成
 * 　　tag_mapping_xxxx　へINSERTする、tag_id=1015のデータ用（TMP_1015_$currency_cd_$table_pref）
 * 　　tag_mapping_xxxx　へINSERTする、tag_id=1016のデータ用（TMP_1016_$currency_cd_$table_pref）
 * 　　更新対象の publilsh_id 一覧用（TMP_TABLE_$currency_cd_$table_pref）
 * ③　offsetで指定している件数ずつ、対象の publilsh_id のデータに対して以下の処理をループで実行する
 * 　　・　map_xxxx　の　USD_PRICE のUPDATE処理
 * 　　・　map_xxxx　の　USD_unit_PRICE のUPDATE処理
 * 　　・ tag_mapping_xxxxにINSERTするための1015用データをテンポラリテーブルにINSERT処理をする。
 * 　　・ tag_mapping_xxxxにINSERTするための1016用データをテンポラリテーブルにINSERT処理をする。
 * 　　・　tag_mapping_xxxxの、tag_id が1015と1016のレコードDELETE処理
 * 　　・　tag_mapping_xxxxに、テンポラリテーブル1015のデータのINSERT処理
 * 　　・　tag_mapping_xxxxに、テンポラリテーブル1016のデータのINSERT処理
 * ④ テンポラリテーブルの削除
 *
 *　※例外発生時は、'development@propre.com'　宛てにエラーメールを送付する。
 *
 *
 **/

/** 実行ファイルから見たパスで指定 */
require __DIR__.'/../import/config.php';
require __DIR__.'/../import/db_oracle.php';
require __DIR__.'/../import/log.php';


# エラーメールの送付先
define('TO_MAIL_ADDR', 'development@propre.com');
# 一度の処理件数
define('PROCESS_NUM', 200);

# グローバル変数
$pdo = DB::getPdo(DB_ORA_TNS_TP);
$log = new log();

# バッチ処理のトータル時間
$time_start_batch = microtime(true);

/*
* phpファイル実行時に引数を渡す
* 　$argv[1]：更新対象の通貨コード名
* 　$argv[2]：更新対象のテーブル
* 　　　 'sell' または 'rent' を指定する
*
*/
$currency_cd = $argv[1];
$table_pref = $argv[2];
//$test_pref = $argv[3];

#引数がない場合は終了
if ($currency_cd == NULL || $table_pref == NULL) {
	$log->freeform("currency_update", "[ERROR]引数不正");
	exit();
}


# mst_currencyから通貨レートの取得
$sql = "SELECT exchange_rate FROM mst_currency WHERE currency_cd = '".$currency_cd."'";
$stmt = $pdo->query($sql);
$row = $stmt->fetch();
$exchange_rate = $row['exchange_rate'];
//echo $exchange_rate;

$log->freeform("currency_update", "exchange_rate:{$exchange_rate},currency_cd:{$currency_cd} start");

#　1015と1016データを暫定的に入れておくためのテンポラリーテーブルを作成する。
//$sql1 = "CREATE global temporary TABLE TMP_1015_".$currency_cd."_".$table_pref. " (
$sql1 = "CREATE TABLE TMP_1015_".$currency_cd."_".$table_pref. " (
	tag_id NUMBER(19, 0) NOT NULL,
    tag_level NUMBER(10, 0) DEFAULT 0 NOT NULL,
    publish_id NUMBER(19, 0) NOT NULL,
    meshcode1 NUMBER(10, 0) DEFAULT 0 NOT NULL,
    meshcode2 NUMBER(10, 0) DEFAULT 0 NOT NULL,
    meshcode3 NUMBER(19, 0) DEFAULT 0 NOT NULL,
    meshcode6 NUMBER(19, 0) DEFAULT 0 NOT NULL,
    primary key(tag_id,tag_level,publish_id)
)";
//) on commit preserve rows";
//echo "{$sql1}\n";

//$sql2 = "CREATE global temporary TABLE TMP_1016_".$currency_cd."_".$table_pref. " (
$sql2 = "CREATE TABLE TMP_1016_".$currency_cd."_".$table_pref. " (
	tag_id NUMBER(19, 0) NOT NULL,
    tag_level NUMBER(10, 0) DEFAULT 0 NOT NULL,
    publish_id NUMBER(19, 0) NOT NULL,
    meshcode1 NUMBER(10, 0) DEFAULT 0 NOT NULL,
    meshcode2 NUMBER(10, 0) DEFAULT 0 NOT NULL,
    meshcode3 NUMBER(19, 0) DEFAULT 0 NOT NULL,
    meshcode6 NUMBER(19, 0) DEFAULT 0 NOT NULL,
    primary key(tag_id,tag_level,publish_id)
)";
//) on commit preserve rows";
//echo "{$sql2}\n";

# 通貨ごとにテーブルを作成する。
//$sql3 = "CREATE GLOBAL TEMPORARY TABLE TMP_TABLE_".$currency_cd."_".$table_pref. "
$sql3 = "CREATE TABLE TMP_TABLE_".$currency_cd."_".$table_pref. "
		 (id NUMBER GENERATED ALWAYS AS IDENTITY NOT NULL, PUBLISH_ID NUMBER(19, 0) NOT NULL,
		 PRIMARY KEY (ID)
		 USING INDEX (CREATE UNIQUE INDEX TMP_INDEX_".$currency_cd."_".$table_pref. "
		  ON TMP_TABLE_".$currency_cd."_".$table_pref. " (ID ASC))
		 )";
//		 ) ON COMMIT PRESERVE ROWS";
//echo "{$sql3}\n";

# テンポラリーテーブルの作成処理実行
try {
    $stmt = $pdo->query($sql1);
    $stmt = $pdo->query($sql2);
    $stmt = $pdo->query($sql3);
} catch (PDOException $e) {
    $msg = "{$currency_cd}:create ".$e->getMessage();
    $log->freeform("currency_update", $msg);
    sendErrMail($msg);
    //echo $e->getMessage();
    exit;
}

// map_xxx ⇒ publish_xxx テーブルの publish_id のデータを取得するように変更 #2020/11/30
$sql = "INSERT INTO TMP_TABLE_".$currency_cd."_".$table_pref. " (publish_id) 
		SELECT publish_id FROM publish_".$table_pref." WHERE currency_cd='".$currency_cd."' AND  price > 0";
//echo "{$sql}\n";
$time_start = microtime(true);

try {
    $stmt = $pdo->query($sql);
} catch (PDOException $e) {
    $msg = "{$currency_cd}:INSERT data ".$e->getMessage();
    $log->freeform("currency_update", $msg);
    sendErrMail($msg);
    //echo $e->getMessage();
    drop_table ($currency_cd, $table_pref);
    exit;
}

$time = microtime(true) - $time_start;
$Msg = "create table:{$currency_cd} ". sprintf("%.20f", $time) . "sec";
$log->freeform("currency_update", $Msg);

#ループ処理の単位
$offset = PROCESS_NUM;

// map_sell のテーブル更新をいったんコメントアウト  #2020/11/30
# map_sellの usd_price更新用
//
//$sql1 = "UPDATE map_".$table_pref." m SET usd_price = price * ".$exchange_rate."
// WHERE EXISTS (
//    SELECT /*+INDEX(t TMP_INDEX_".$currency_cd."_".$table_pref. ")*/ 1 FROM TMP_TABLE_".$currency_cd."_".$table_pref. " t
//     WHERE m.publish_id=t.publish_id AND t.id between :start_cnt AND :end_cnt)";
//echo "{$sql1}\n";
//$log->freeform("currency_update", $sql1);
//$stmt1 = $pdo->prepare($sql1);

// map_sell のテーブル更新をいったんコメントアウト  #2020/11/27
#　mapの usd_unit_price更新用
//$sql2 = "UPDATE map_".$table_pref." m SET usd_unit_price = price * ".$exchange_rate." / floor_sqft
// WHERE EXISTS (
//    SELECT /*+INDEX(t TMP_INDEX_".$currency_cd."_".$table_pref.")*/ 1 FROM TMP_TABLE_".$currency_cd."_".$table_pref. " t
//      WHERE m.publish_id=t.publish_id AND t.id between :start_cnt AND :end_cnt) AND floor_sqft > 0";
//echo "{$sql2}\n";
//$log->freeform("currency_update", $sql2);
//$stmt2 = $pdo->prepare($sql2);

#　map_sellのデータをもとに、tag_mapping_sellにINSERTするためのデータをテンポラリーテーブルに登録する
// データ取得テーブルを map_xxx ⇒ publish_xxx テーブルに変更 #2020/11/27
#tag_id:1015登録用
$sql3 = "INSERT INTO TMP_1015_".$currency_cd."_".$table_pref. " 
    SELECT 1015, price * ".$exchange_rate.",publish_id,meshcode1,meshcode2,meshcode3,meshcode6 FROM publish_".$table_pref." 
    WHERE publish_id IN (SELECT /*+INDEX(t TMP_INDEX_".$currency_cd."_".$table_pref.")*/ publish_id
     FROM TMP_TABLE_".$currency_cd."_".$table_pref. " t
     WHERE t.id between :start_cnt AND :end_cnt)
     AND price > 0";
//echo "{$sql3}\n";
//$log->freeform("currency_update", $sql3);
$stmt3 = $pdo->prepare($sql3);

#tag_id:1016登録用
$sql4 = "INSERT INTO TMP_1016_".$currency_cd."_".$table_pref. " 
    SELECT 1016,floor(unit_price * ".$exchange_rate." * 100),publish_id,meshcode1,meshcode2,meshcode3,meshcode6 FROM publish_".$table_pref." 
    WHERE publish_id IN (SELECT /*+INDEX(t TMP_INDEX_".$currency_cd."_".$table_pref.")*/ publish_id
     FROM TMP_TABLE_".$currency_cd."_".$table_pref. " t
     WHERE t.id between :start_cnt AND :end_cnt)
     AND unit_price > 0";
//echo "{$sql4}\n";
//$log->freeform("currency_update", $sql4);
$stmt4 = $pdo->prepare($sql4);

//# tag_idが1015,1016のデータの削除
//$sql5 = "DELETE FROM tag_mapping_".$table_pref." tag WHERE tag.publish_id IN
//    (SELECT /*+INDEX(t TMP_INDEX_".$currency_cd."_".$table_pref.")*/ publish_id FROM TMP_TABLE_".$currency_cd."_".$table_pref. " t
//    WHERE id between :start_cnt AND :end_cnt)
//     AND tag.tag_id IN (1015,1016)";
//
////echo "{$sql5}\n";
////$log->freeform("currency_update", $sql5);
//$stmt5 = $pdo->prepare($sql5);

# 削除したpublilsh_idに該当する、tag_idが1015のデータをテンポラリーテーブルからINSERTする。
//$sql6 = "INSERT INTO tag_mapping_".$table_pref." SELECT * FROM TMP_1015_".$currency_cd."_".$table_pref. " tmp
//    WHERE tmp.publish_ID IN (SELECT /*+INDEX(t TMP_INDEX_".$currency_cd."_".$table_pref.")*/ publish_id
//     FROM TMP_TABLE_".$currency_cd."_".$table_pref. " t
//     WHERE t.id between :start_cnt AND :end_cnt)";
$sql6 = makeTagMappingMergeSQL("1015",$table_pref,$currency_cd);

//echo "{$sql6}\n";
$log->freeform("currency_update", $sql6);
$stmt6 = $pdo->prepare($sql6);

# 削除したpublilsh_idに該当する、tag_idが1016のデータをテンポラリーテーブルからINSERTする。
//$sql7 = "INSERT INTO tag_mapping_".$table_pref." SELECT * FROM TMP_1016_".$currency_cd."_".$table_pref. " tmp
//    WHERE tmp.publish_ID IN (SELECT /*+INDEX(t TMP_INDEX_".$currency_cd."_".$table_pref.")*/ publish_id
//     FROM TMP_TABLE_".$currency_cd."_".$table_pref. " t
//     WHERE t.id between :start_cnt AND :end_cnt)";
$sql7 = makeTagMappingMergeSQL("1016",$table_pref,$currency_cd);

//echo "{$sql7}\n";
$log->freeform("currency_update", $sql7);
$stmt7 = $pdo->prepare($sql7);


#最小値と最大値の差から通貨コード毎の合計件数を割り出し、ループのカウントを変更する
#　スタートはすべて1から
$start_cnt = 1;
$sql = "select max(id) max_num FROM TMP_TABLE_".$currency_cd."_".$table_pref;
$stmt = $pdo->query($sql);
$row = $stmt->fetch();
$max_num = $row['max_num'];

# テーブルが空の場合は、処理を抜ける
if ($max_num==NULL || $max_num == ''){
    drop_table ($currency_cd, $table_pref);
    $log->freeform("currency_update", "[batch end]currency_cd:{$currency_cd} has no data.");
    exit;
}

#　ループ回数を取得する
$loop_cnt = (int)($max_num / $offset) + 1;
$end_cnt= $start_cnt + $offset;

$log->freeform("currency_update", "({$currency_cd}) start_cnt:{$start_cnt},max_num:{$max_num},loop_cnt:{$loop_cnt}");

$time_start_cd = microtime(true);

for ($i=1; $i<=$loop_cnt; $i++) {
    //echo "/" . $i; //これは進行をチェックするログ（画面に出力）
    $Msg = "{$currency_cd},{$table_pref},{$i},start_cnt:{$start_cnt},end_cnt:{$end_cnt}";
    //$log->freeform("currency_update", $Msg);

    // map_xxx への更新処理なのでコメントアウト
    /*
    $Msg = sendSQL($stmt1,$start_cnt,$end_cnt,$currency_cd,"update","usd_price",$table_pref,$Msg,$log);
//    try {
//        $time_start = microtime(true);
//        $stmt1->bindParam(':start_cnt', $start_cnt, PDO::PARAM_INT);
//        $stmt1->bindParam(':end_cnt', $end_cnt, PDO::PARAM_INT);
//        $stmt1->execute();
//        $time1 = microtime(true) - $time_start;
//        $Msg = $Msg.",".sprintf("%.20f", $time1);
//    } catch (PDOException $e) {
//        $msg = "{$currency_cd}:update usd_price:{$start_cnt} to {$end_cnt}".$e->getMessage();
//        $log->freeform("currency_update", $msg);
//        sendErrMail($msg);
//        drop_table ($currency_cd, $table_pref);
//        //echo $e->getMessage();
//        exit;
//    }
    $Msg = sendSQL($stmt2,$start_cnt,$end_cnt,$currency_cd,"update","usd_unit_price",$table_pref,$Msg,$log);
//    try {
//        $time_start = microtime(true);
//        $stmt2->bindParam(':start_cnt', $start_cnt, PDO::PARAM_INT);
//        $stmt2->bindParam(':end_cnt', $end_cnt, PDO::PARAM_INT);
//        $stmt2->execute();
//        $time2 = microtime(true) - $time_start;
//        $Msg = $Msg.",".sprintf("%.20f", $time2);
//    } catch (PDOException $e) {
//        $msg = "{$currency_cd}:update usd_unit_price:{$start_cnt} to {$end_cnt}".$e->getMessage();
//        $log->freeform("currency_update", $msg);
//        sendErrMail($msg);
//        drop_table ($currency_cd, $table_pref);
//        //echo $e->getMessage();
//        exit;
//    }
    */
    $Msg = sendSQL($stmt3,$start_cnt,$end_cnt,$currency_cd,"insert","tmp1015",$table_pref,$Msg,$log);
//    try {
//        $time_start = microtime(true);
//        $stmt3->bindParam(':start_cnt', $start_cnt, PDO::PARAM_INT);
//        $stmt3->bindParam(':end_cnt', $end_cnt, PDO::PARAM_INT);
//        $stmt3->execute();
//        $time3 = microtime(true) - $time_start;
//        $Msg = $Msg.",".sprintf("%.20f", $time3);
//    } catch (PDOException $e) {
//        $msg = "{$currency_cd}:insert tmp1015:{$start_cnt} to {$end_cnt}".$e->getMessage();
//        $log->freeform("currency_update", $msg);
//        sendErrMail($msg);
//        drop_table ($currency_cd, $table_pref);
//        //echo $e->getMessage();
//        exit;
//    }
    $Msg = sendSQL($stmt4,$start_cnt,$end_cnt,$currency_cd,"insert","tmp1016",$table_pref,$Msg,$log);
//    try {
//        $time_start = microtime(true);
//        $stmt4->bindParam(':start_cnt', $start_cnt, PDO::PARAM_INT);
//        $stmt4->bindParam(':end_cnt', $end_cnt, PDO::PARAM_INT);
//        $stmt4->execute();
//        $time4 = microtime(true) - $time_start;
//        $Msg = $Msg.",".sprintf("%.20f", $time4);
//    } catch (PDOException $e) {
//        $msg = "{$currency_cd}:insert tmp1016:{$start_cnt} to {$end_cnt}".$e->getMessage();
//        $log->freeform("currency_update", $msg);
//        sendErrMail($msg);
//        drop_table ($currency_cd, $table_pref);
//        //echo $e->getMessage();
//        exit;
//    }
//    $Msg = sendSQL($stmt5,$start_cnt,$end_cnt,$currency_cd,"delete","",$table_pref,$Msg,$log);
//    try {
//        $time_start = microtime(true);
//        $stmt5->bindParam(':start_cnt', $start_cnt, PDO::PARAM_INT);
//        $stmt5->bindParam(':end_cnt', $end_cnt, PDO::PARAM_INT);
//        $stmt5->execute();
//        $time5 = microtime(true) - $time_start;
//        $Msg = $Msg.",".sprintf("%.20f", $time5);
//    } catch (PDOException $e) {
//        $msg = "{$currency_cd}:delete:{$start_cnt} to {$end_cnt}".$e->getMessage();
//        $log->freeform("currency_update", $msg);
//        sendErrMail($msg);
//        drop_table ($currency_cd, $table_pref);
//        //echo $e->getMessage();
//        exit;
//    }
    $Msg = sendSQL($stmt6,$start_cnt,$end_cnt,$currency_cd,"MERGE","1015",$table_pref,$Msg,$log);
//    try {
//        $time_start = microtime(true);
//        $stmt6->bindParam(':start_cnt', $start_cnt, PDO::PARAM_INT);
//        $stmt6->bindParam(':end_cnt', $end_cnt, PDO::PARAM_INT);
//        $stmt6->execute();
//        $time6 = microtime(true) - $time_start;
//        $Msg = $Msg.",".sprintf("%.20f", $time6);
//    } catch (PDOException $e) {
//        $msg = "{$currency_cd}:insert 1015:{$start_cnt} to {$end_cnt}".$e->getMessage();
//        $log->freeform("currency_update", $msg);
//        sendErrMail($msg);
//        drop_table ($currency_cd, $table_pref);
//        //echo $e->getMessage();
//        exit;
//    }
    $Msg = sendSQL($stmt7,$start_cnt,$end_cnt,$currency_cd,"MERGE","1016",$table_pref,$Msg,$log,true);
//    try {
//        $time_start = microtime(true);
//        $stmt7->bindParam(':start_cnt', $start_cnt, PDO::PARAM_INT);
//        $stmt7->bindParam(':end_cnt', $end_cnt, PDO::PARAM_INT);
//        $stmt7->execute();
//        $time7 = microtime(true) - $time_start;
//        $Msg = $Msg.",".sprintf("%.20f", $time7);
//        //echo $Msg;
//        $log->freeform("currency_update_time", $Msg);
//
//    } catch (PDOException $e) {
//        $msg = "{$currency_cd}:insert 1016:{$start_cnt} to {$end_cnt}".$e->getMessage();
//        $log->freeform("currency_update", $msg);
//        sendErrMail($msg);
//        drop_table ($currency_cd, $table_pref);
//        //echo $e->getMessage();
//        exit;
//    }
    $start_cnt = $end_cnt + 1;
    $end_cnt = $end_cnt + $offset;
}

$time_cd = microtime(true) - $time_start_cd;
$Msg = "exec time ({$currency_cd}):" . sprintf("%.20f", $time_cd) . 'sec';
$log->freeform("currency_update", $Msg);

# テンポラリーテーブル削除を実施する
//drop_table ($currency_cd, $table_pref);

// SQL 送信＆ログ記録、最後のSQLの場合、行ログをファイルに出力する
// 失敗した場合メール送って、行ログに「error」で吐き出す。
function sendSQL($stmt,$start_cnt,$end_cnt,$currency_cd,$function_type,$target_name,$table_pref,$Msg,$log,$last_sql_write_log = false) {
    try {
        $time_start = microtime(true);
        $stmt->bindParam(':start_cnt', $start_cnt, PDO::PARAM_INT);
        $stmt->bindParam(':end_cnt', $end_cnt, PDO::PARAM_INT);
        $stmt->execute();
        $time = microtime(true) - $time_start;
        $Msg = $Msg.",".sprintf("%.20f", $time);
    } catch (PDOException $e) {
        $msg = "{$currency_cd}:{$function_type} {$target_name}:{$start_cnt} to {$end_cnt}".$e->getMessage();
        $log->freeform("currency_update", $msg);
        sendErrMail($msg);
        // drop_table ($currency_cd, $table_pref);
        // エラーの場合errorで出力する
        $Msg = $Msg.","."error";
        //echo $e->getMessage();
    }
    // 行ログをファイルに出力する
    if ($last_sql_write_log) {
        $log->freeform("currency_update_time", $Msg);
    }
    return $Msg;
}

/**
 * tag_mapping_XX へのmerge SQLを作成
 * @param $tag_id
 * @param $table_pref
 * @param $currency_cd
 * @return string
 */
function makeTagMappingMergeSQL($tag_id,$table_pref,$currency_cd) {
    $sql = "MERGE INTO tag_mapping_$table_pref
         USING 
            ( 
              SELECT
                tag_id, tag_level, publish_id, meshcode1, meshcode2, meshcode3, meshcode6 
              FROM
                tmp_".$tag_id."_".$currency_cd."_".$table_pref." tmp 
              WHERE
                tmp.publish_id IN ( 
                  SELECT
                    /*+INDEX(t TMP_INDEX_".$currency_cd."_".$table_pref.")*/ publish_id 
                  FROM
                    TMP_TABLE_".$currency_cd."_".$table_pref. " t
                  WHERE
                    t.id BETWEEN :start_cnt AND :end_cnt
                )
            ) aa 
         ON ( 
              tag_mapping_$table_pref.tag_id = aa.tag_id 
              AND tag_mapping_$table_pref.publish_id = aa.tag_id
         ) 
         WHEN MATCHED THEN 
            UPDATE SET tag_mapping_$table_pref.tag_level = aa.tag_level 
         WHEN NOT MATCHED THEN 
            INSERT ( tag_id, tag_level, publish_id, meshcode1, meshcode2, meshcode3, meshcode6) 
            VALUES (aa.tag_id, aa.tag_level, aa.publish_id, aa.meshcode1, aa.meshcode2, aa.meshcode3, aa.meshcode6)";

    return $sql;
}

/**
 * テンポラリテーブル削除処理：　バッチ処理用に作成したテンポラリテーブルを削除する。
 *
 * 　・更新処理終了時
 * 　・テンポラリテーブル作成後のエラー発生時
 *
 */
function drop_table ($currency_cd, $table_pref) {

    $currency_cd = $currency_cd;
    $table_pref = $table_pref;

    # グローバル変数を参照
    global $pdo;
    global $log;

    # バッチ用に作成したテンポラリーテーブルをデータを削除後、ドロップする
    $sql1 = "TRUNCATE TABLE TMP_1015_".$currency_cd."_".$table_pref;
    $log->freeform("currency_update", $sql1);
    $sql2 = "TRUNCATE TABLE TMP_1016_".$currency_cd."_".$table_pref;
    $log->freeform("currency_update", $sql2);
    $sql3 = "TRUNCATE TABLE TMP_TABLE_".$currency_cd."_".$table_pref;
    $log->freeform("currency_update", $sql3);

    $sql4 = "DROP TABLE TMP_1015_".$currency_cd."_".$table_pref;
    $log->freeform("currency_update", $sql4);
    $sql5 = "DROP TABLE TMP_1016_".$currency_cd."_".$table_pref;
    $log->freeform("currency_update", $sql5);
    $sql6 = "DROP TABLE TMP_TABLE_".$currency_cd."_".$table_pref;
    $log->freeform("currency_update", $sql6);

    try {
        $stmt = $pdo->query($sql1);
        $stmt = $pdo->query($sql2);
        $stmt = $pdo->query($sql3);
        $stmt = $pdo->query($sql4);
        $stmt = $pdo->query($sql5);
        $stmt = $pdo->query($sql6);
    } catch (PDOException $e) {
        $msg = "{$currency_cd}:drop table:".$e->getMessage();
        $log->freeform("currency_update", $msg);
        sendErrMail($msg);
        //echo $e->getMessage();
        exit;
    }
    return;
}

/**
 * Errorメール送信処理：　例外発生時に、管理者へエラーメールを送信する。
 *
 */
function sendErrMail($body)
{

    mb_language("Japanese");
    mb_internal_encoding("UTF-8");


    $to = TO_MAIL_ADDR;     //エラーメールの送付先
    $header = "From: $to\nReply-To: $to\n";
    $title = '【ERROR】USD価格変更バッチ';

    try {
        mb_send_mail($to, $title, $body, $header);
        //echo "処理エラーメールを送信しました";
        return true;
    } catch (Exception $e) {
        //echo "エラーメールの送信に失敗しました";
        return false;
    }
}

# トータルの処理時間を取得
$time_total = microtime(true) - $time_start_batch;

#log
$log->freeform("currency_update", "exchange_rate:{$exchange_rate},currency_cd:{$currency_cd},total time:{$time_total} end");
?>
