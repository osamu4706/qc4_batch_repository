<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/www/import/config.php";
require '/var/www/import/db_oracle.php';
require "/var/www/import/tools.php";
require "/var/www/import/log.php";

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);
$log = new log();

/*
INSERT INTO scrape SELECT * FROM scrape_20191029_2 WHERE crawl_id IN (SELECT crawl_id FROM tmp_serial_scrape WHERE id BETWEEN :start_id AND :end_id");
*/

$limit_num=1000;

$min_id=1;
$max_id=8872514;  //23099167

$sql = "INSERT INTO scrape SELECT * FROM scrape_20191029_2 WHERE crawl_id IN (SELECT crawl_id FROM tmp_serial_scrape2 WHERE id BETWEEN :start_id AND :end_id)";
$stmt = $pdo_medium->prepare($sql);

$sql2 = "SELECT DISTINCT c.crawl_id FROM crawling_list c LEFT OUTER JOIN scrape s ON c.crawl_id=s.crawl_id AND s.crawl_id IN (SELECT crawl_id FROM tmp_serial_scrape2 WHERE id BETWEEN :start_id AND :end_id) WHERE s.scraped_data IS NOT null";
$stmt2 = $pdo_medium->prepare($sql2);

for($i= (int)($min_id/$limit_num); $i <= (int)($max_id/$limit_num) + 1; $i++){
	$dt_s = new DateTime();
	$start_id = $i * $limit_num + 1;
	$end_id = ($i+1) * $limit_num;
	$stmt->bindParam(':start_id', $start_id, PDO::PARAM_INT);
	$stmt->bindParam(':end_id', $end_id, PDO::PARAM_INT);
	$flg_error = 0;
	try{
		$stmt->execute();
	}catch (Exception $e) {
		$flg_error = 1;
		$log->freeform("insert_scrape_error", "エラー1," . $start_id . ',' . $end_id  . ',' . $e->getMessage());
		//duplicateのcrawl_idを除外して再実行
		$stmt2->bindParam(':start_id', $start_id, PDO::PARAM_INT);
		$stmt2->bindParam(':end_id', $end_id, PDO::PARAM_INT);
		$stmt2->execute();
		$rows = $stmt2->fetchAll();
		$crawl_ids = '';
		foreach($rows as $row){
			$crawl_id = $row['crawl_id'];
			if($crawl_ids != ''){
				$crawl_ids .=',';
			}
			$crawl_ids .=$crawl_id;
		}
		if($crawl_ids != ''){
			$sql3 = "DELETE FROM tmp_serial_scrape WHERE crawl_id IN(" . $crawl_ids . ")";
			$stmt3->query($sql3);
			try{
				//再実行
				$stmt->execute();
			}catch (Exception $e) {
				$flg_error = 2;
				$log->freeform("insert_scrape_error", "エラー2," . $start_id . ',' . $end_id  . ',' . $e->getMessage());
			}
		}
	}
	$dt_e = new DateTime();
	$elapsed = $dt_e->diff($dt_s)->format("%s.%f");
	$log->freeform("insert_scrape_" . $min_id, $flg_error . ',' . $start_id . ',' . $end_id  . ',' . $elapsed);

	if($i % 10 == 0){
		echo $i . ' ';
	}
}

?>
