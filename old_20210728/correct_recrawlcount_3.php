<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/www/import/config.php";
require '/var/www/import/db_oracle.php';
require "/var/www/import/tools.php";
require "/var/www/import/log.php";

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$log = new log();

/*
スクレイプデータの紐付けを修正する
0,100以外の全レコードからスクレイプデータ（s3/scraped_data）をチェック、ステータスとrecrawl_countを再付番する
スクレイプデータの紐付けを修正する
scrapeにレコードなし＆元ステータス100ではない→3311
scrapeにレコードなし＆元ステータス100→3300
scrapeにレコードあり、スクレイプデータ（s3）のみあり→3320
scrapeにレコードあり、スクレイプデータ（scraped_data）あり→3330
当日のpicker完了後に
crawl_setting で対象サイトのspecified_crawlerに文字列などを入れてdequeues対象外にしてから実行する
処理完了後、
status=3311 → 当日分は0、過去は200など
status=3320 → 優先順の高い順（当日分）から40→スクレイプ＆クレンジングへ
status=3330 → 優先順の高い順（当日分）から50→クレンジングへ
*/


$site_no=3;
$limit_num=1000;  //0.7sec
$cnt = 748132;  //site_no:1

#$sql = "SELECT count(*) AS cnt FROM crawling_list WHERE site_no=:site_no AND yyyymmdd>=20190807 AND status < 3000";
#全件対象に変更
# status < 3000 AND status<>100 AND status<>0 だと同じrangeを使いながら20secくらいかかるので修正
/*$sql = "SELECT count(*) AS cnt FROM crawling_list WHERE crawl_id=25900561";
$stmt = $db->prepare($sql);
//$stmt->bindParam(':site_no', $site_no, PDO::PARAM_INT);
$stmt->execute();
$row = $stmt->fetch();
$cnt = $row['cnt'];
*/
#$sql = "SELECT crawl_id,recrawl_count,status FROM crawling_list WHERE site_no=:site_no AND yyyymmdd>=20190807 AND status < 3000 LIMIT :limit_num";
#$sql = "SELECT crawl_id,recrawl_count,status FROM crawling_list WHERE crawl_id=25900561";
$sql = "SELECT crawl_id,recrawl_count,status FROM crawling_list WHERE site_no=:site_no AND status >=100 AND status <= 3300 AND rownum<= :limit_num";

$stmt = $pdo_tp->prepare($sql);

$sql2 = "SELECT changed_num,s3_path,TO_CHAR(scraped_data) as scraped_data FROM scrape WHERE crawl_id = :crawl_id AND rownum=1 ORDER BY changed_num DESC";
$stmt2 = $pdo_tp->prepare($sql2);

$sql3 = "UPDATE crawling_list SET recrawl_count=:recrawl_count,status=:status WHERE crawl_id = :crawl_id";
$stmt3 = $pdo_tp->prepare($sql3);

for ($i=0; $i<((int)$cnt -1) / $limit_num + 1; $i++){
	$stmt->bindParam(':site_no', $site_no, PDO::PARAM_INT);
	$stmt->bindParam(':limit_num', $limit_num, PDO::PARAM_INT);
	$stmt->execute();
	$rows = $stmt->fetchAll();
	foreach($rows as $row){
		$crawl_id = $row['crawl_id'];
		$recrawl_count = $row['recrawl_count'];
		$status = $row['status'];
		$stmt2->bindParam(':crawl_id', $crawl_id, PDO::PARAM_INT);
		$stmt2->execute();
		$row2 = $stmt2->fetch();
		$changed_num = $row2['changed_num'];
		$s3_path = $row2['s3_path'];
		$scraped_data = $row2['scraped_data'];
		$new_status=3330;
		$new_recrawl_count=$changed_num;
		if($new_recrawl_count == null){
			$new_recrawl_count=0;
		}
		$flg_noset=true;
		if($scraped_data !=null){
			if(strlen($scraped_data) > 10){
				$new_status=3330;  //scrapeにレコードあり、スクレイプデータ(scrape_data)あり
				$flg_noset = false;
			}
		}
		if($flg_noset && $s3_path !=null){
			if(strlen($s3_path) > 10){
				$new_status=3320;  //scrapeにレコードあり、スクレイプデータなし（クロールデータs3のみ）
				$flg_noset = false;
			}
		}
		if($flg_noset){
//			if($status == 100){
//				$new_status=3300;  //scrapeにレコードなし/元100
//			}else{
				$new_status=3311;  //scrapeにレコードなし
//			}
		}
		$stmt3->bindParam(':crawl_id', $crawl_id, PDO::PARAM_INT);
		$stmt3->bindParam(':recrawl_count', $new_recrawl_count, PDO::PARAM_INT);
		$stmt3->bindParam(':status', $new_status, PDO::PARAM_INT);
		$stmt3->execute();
		$log->freeform("correct_recrawlcount_3", $crawl_id . ':' . $new_recrawl_count . '/' . $status . '->' . $new_status);
	}
	if($i % 10 ==0){
		echo $i/10;
	}
}

?>
