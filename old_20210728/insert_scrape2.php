<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/www/import/config.php";
require '/var/www/import/db_oracle.php';
require "/var/www/import/tools.php";
require "/var/www/import/log.php";

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);
$log = new log();

/*
INSERT INTO scrape2 SELECT * FROM scrape WHERE crawl_id IN (SELECT crawl_id FROM tmp_scrape2_id WHERE id BETWEEN :start_id AND :end_id);
*/

$limit_num=1000;

$min_id=1;
$max_id=882000;



//$sql1 = "INSERT INTO scrape2 SELECT crawl_id,changed_num,crawler_id,site_no,s3_path,CASE WHEN TO_CHAR(scraped_data) = 'null' OR  TO_CHAR(scraped_data) IS NULL THEN '{}' ELSE TO_CHAR(scraped_data) END,reg_datetime FROM scrape WHERE (crawl_id,changed_num) in (SELECT crawl_id,changed_num FROM tmp_scrape2_id WHERE id BETWEEN :start_id AND :end_id)";
$sql1 = "INSERT INTO scrape SELECT crawl_id,changed_num,crawler_id,site_no,s3_path,CASE WHEN TO_CHAR(scraped_data) = 'null' OR  TO_CHAR(scraped_data) IS NULL THEN '{}' ELSE TO_CHAR(scraped_data) END,reg_datetime FROM scrape_old WHERE (crawl_id,changed_num) in (SELECT crawl_id,changed_num FROM tmp_scrape2_id WHERE id BETWEEN :start_id AND :end_id)";
$stmt1 = $pdo_medium->prepare($sql1);

//$sql2 = "INSERT INTO scrape2 SELECT crawl_id,changed_num,crawler_id,site_no,s3_path,CASE WHEN TO_CHAR(scraped_data) = 'null' OR  TO_CHAR(scraped_data) IS NULL THEN '{}' ELSE TO_CHAR(scraped_data) END,reg_datetime FROM scrape WHERE (crawl_id,changed_num) in (SELECT crawl_id,changed_num FROM tmp_scrape2_id WHERE id = :id)";
$sql2 = "INSERT INTO scrape SELECT crawl_id,changed_num,crawler_id,site_no,s3_path,CASE WHEN TO_CHAR(scraped_data) = 'null' OR  TO_CHAR(scraped_data) IS NULL THEN '{}' ELSE TO_CHAR(scraped_data) END,reg_datetime FROM scrape_old WHERE (crawl_id,changed_num) in (SELECT crawl_id,changed_num FROM tmp_scrape2_id WHERE id = :id)";
$stmt2 = $pdo_tp->prepare($sql2);

//$sql3 = "INSERT INTO scrape2 SELECT crawl_id,changed_num,crawler_id,site_no,s3_path,scraped_data,reg_datetime FROM scrape WHERE (crawl_id,changed_num) in (SELECT crawl_id,changed_num FROM tmp_scrape2_id WHERE id = :id)";
$sql3 = "INSERT INTO scrape SELECT crawl_id,changed_num,crawler_id,site_no,s3_path,scraped_data,reg_datetime FROM scrape WHERE (crawl_id,changed_num) in (SELECT crawl_id,changed_num FROM tmp_scrape2_id WHERE id = :id)";
$stmt3 = $pdo_tp->prepare($sql3);

for($i= (int)($min_id/$limit_num); $i < (int)($max_id/$limit_num); $i++){
	$dt_s = new DateTime();
	$start_id = $i * $limit_num + 1;
	$end_id = ($i+1) * $limit_num;
	$stmt1->bindParam(':start_id', $start_id, PDO::PARAM_INT);
	$stmt1->bindParam(':end_id', $end_id, PDO::PARAM_INT);
	$flg_error = 0;
	try{
		$stmt1->execute();
	}catch (Exception $e) {
		//1行ずつ再実行
		for($j=$start_id; $j<=$end_id; $j++){
			$stmt2->bindParam(':id', $j, PDO::PARAM_INT);
			try{
				$stmt2->execute();
			}catch (Exception $e) {
				if(strpos($e->getMessage(),'ORA-22835') !== false){
					//ORA-22835: CLOBからCHAR、またはBLOBからRAWへの変換には、バッファーが小さすぎます(実際: 34420、最大: 32767)
					//は再度CASEを使わず直接投入
					$stmt3->bindParam(':id', $j, PDO::PARAM_INT);
					try{
						$stmt3->execute();
					}catch (Exception $e) {
						//それでもエラーだったらエラー
						$flg_error ++;
						//エラー行
						$log->freeform("insert_scrape2_error", $j . "," . $e->getMessage());
					}
				}else{
					$flg_error ++;
					//エラー行
					$log->freeform("insert_scrape2_error", $j . "," . $e->getMessage());
				}
			}
		}
	}
	$dt_e = new DateTime();
	$elapsed = $dt_e->diff($dt_s)->format("%H:%I:%S.%F");
	if($flg_error == 0){
		$log->freeform("insert_scrape2", "SUCCESS," . $start_id . ',' . $end_id . ',' . $elapsed);
	}else{
		$log->freeform("insert_scrape2", "ERROR(" . $flg_error . ")," . $start_id . ',' . $end_id . ',' . $elapsed);
	}
	if($i % 100 == 0){
		echo $i . ' ';
	}
}

?>
