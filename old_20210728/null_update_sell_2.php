<?php

/** 実行ファイルから見たパスで指定 */
require __DIR__.'/../import/config.php';
require __DIR__.'/../import/db_oracle.php';
require __DIR__.'/../import/log.php';


# エラーメールの送付先
define('TO_MAIL_ADDR', 'develop@propre.com');
# 一度の処理件数
//define('PROCESS_NUM', 500);

# グローバル変数
$pdo = DB::getPdo(DB_ORA_TNS_TPURGENT);
//$pdo = DB::getPdo(DB_ORA_TNS_MEDIUM);
$log = new log();

# バッチ処理のトータル時間
$time_start_batch = microtime(true);
$log->freeform("null_update_sell", "null_update_sell.batch start");

//$tag_id = 1016;
//$time_start = microtime(true);


#　1015と1016データを暫定的に入れておくためのテンポラリーテーブルを作成する。
$sql1 = "CREATE TABLE TMP_TABLE_1015_SELL
(id NUMBER GENERATED ALWAYS AS IDENTITY NOT NULL, PUBLISH_ID NUMBER(19, 0) NOT NULL,
		 PRIMARY KEY (ID)
		 USING INDEX (CREATE UNIQUE INDEX TMP_INDEX_1015_SELL
		  ON TMP_TABLE_1015_SELL (ID ASC))
		 )";
//echo "{$sql1}\n";

$sql2 = "CREATE TABLE TMP_TABLE_1016_SELL
(id NUMBER GENERATED ALWAYS AS IDENTITY NOT NULL, PUBLISH_ID NUMBER(19, 0) NOT NULL,
		 PRIMARY KEY (ID)
		 USING INDEX (CREATE UNIQUE INDEX TMP_INDEX_1016_SELL
		  ON TMP_TABLE_1016_SELL (ID ASC))
		 )";

# テンポラリーテーブルの作成処理実行
try {
	$stmt = $pdo->query($sql1);
	$stmt = $pdo->query($sql2);
} catch (PDOException $e) {
	$msg = "create ".$e->getMessage();
	$log->freeform("null_update_sell", $msg);
	sendErrMail($msg);
	//echo $e->getMessage();
	exit;
}

## テンポラリーテーブルへのINSERT処理
# 1016
$sql1 = "INSERT /*+ PARALLEL(6) */ INTO TMP_TABLE_1016_SELL (publish_id) 
SELECT
  publish_sell.publish_id 
FROM
  publish_sell 
  LEFT JOIN propre.tag_mapping_sell tbl_tms 
    ON (
		publish_sell.publish_id = tbl_tms.publish_id
		AND tbl_tms.tag_id = 1016
	) 
   LEFT JOIN mst_currency     ON publish_sell.currency_cd = mst_currency.currency_cd 
WHERE
(tag_level IS NULL OR tag_level = 0) 
  AND publish_sell.unit_price IS NOT NULL
AND publish_sell.currency_cd != '-'
and publish_sell.unit_price <> 0
AND publish_sell.unit_price * mst_currency.exchange_rate * 100 > 1
AND publish_sell.unit_price * mst_currency.exchange_rate * 100 < 999999999
";

# 1015
$sql2 = "INSERT /*+ PARALLEL(6) */ INTO TMP_TABLE_1015_SELL (publish_id) 
SELECT
	publish_sell.publish_id
FROM
  publish_sell 
  LEFT JOIN propre.tag_mapping_sell tbl_tms 
    ON (
		publish_sell.publish_id = tbl_tms.publish_id
		AND tbl_tms.tag_id = 1015
	) 
   LEFT JOIN mst_currency     ON publish_sell.currency_cd = mst_currency.currency_cd 
WHERE
(tag_level IS NULL OR tag_level = 0) 
  AND publish_sell.price IS NOT NULL
AND publish_sell.currency_cd != '-'
and publish_sell.price <> 0
AND publish_sell.price * mst_currency.exchange_rate > 1
AND publish_sell.price * mst_currency.exchange_rate < 999999999
";

# テンポラリーテーブルのINSERT処理実行
try {
	$stmt = $pdo->query($sql1);
	$stmt = $pdo->query($sql2);
	$msg = "TMP INSERT success";
	$log->freeform("null_update_sell", $msg);
} catch (PDOException $e) {
	$msg = "create ".$e->getMessage();
	$log->freeform("null_update_sell", $msg);
	sendErrMail($msg);
	//echo $e->getMessage();
	exit;
}

## tag_id が 1016 のデータの更新処理
//$sql_1016 = "MERGE /*+ PARALLEL(12) */
$sql_1016 = "MERGE /*+ PARALLEL(6) */
INTO tag_mapping_sell 
  USING ( 
    SELECT
       1016 tag_id
      , case when floor( 
        publish_sell.unit_price * mst_currency.exchange_rate * 100
      ) is null then 0
      else  floor( 
        publish_sell.unit_price * mst_currency.exchange_rate * 100
      ) end tag_level
      , publish_sell.publish_id
      , publish_sell.meshcode1
      , publish_sell.meshcode2
      , publish_sell.meshcode3
      , publish_sell.meshcode6 
    FROM
      publish_sell  
      LEFT JOIN tag_mapping_sell 
        ON (tag_mapping_sell.publish_id = publish_sell.publish_id and tag_id = 1016)
      LEFT JOIN mst_currency 
        ON publish_sell.currency_cd = mst_currency.currency_cd 
    WHERE
      publish_sell.publish_id IN ( 
        SELECT
          publish_id 
        from
          TMP_TABLE_1016_SELL 
        where
          id between :start_cnt AND :end_cnt
      )
  ) aa 
    ON ( 
      tag_mapping_sell.tag_id = aa.tag_id 
      AND tag_mapping_sell.publish_id = aa.publish_id
     -- AND tag_mapping_sell.publish_id = aa.publish_id    
    ) WHEN MATCHED THEN UPDATE 
SET
  tag_mapping_sell.tag_level = aa.tag_level WHEN NOT MATCHED THEN 
INSERT ( 
  tag_id
  , tag_level
  , publish_id
  , meshcode1
  , meshcode2
  , meshcode3
  , meshcode6
) 
VALUES ( 
  aa.tag_id
  , aa.tag_level
  , aa.publish_id
  , aa.meshcode1
  , aa.meshcode2
  , aa.meshcode3
  , aa.meshcode6
)";
$stmt_1016 = $pdo->prepare($sql_1016);
//echo "{$sql}\n";
//$log->freeform("null_update_sell", $sql_1016);

## tag_id が 1015 のデータの更新処理
//$sql_1015 = "MERGE /*+ PARALLEL(12) */
$sql_1015 = "MERGE /*+ PARALLEL(6) */   
INTO tag_mapping_sell 
  USING ( 
    SELECT
       1015 tag_id
      , case when ( 
        publish_sell.price * mst_currency.exchange_rate
      ) is null then 0
      else  ( 
        publish_sell.price * mst_currency.exchange_rate
      ) end tag_level
      , publish_sell.publish_id
      , publish_sell.meshcode1
      , publish_sell.meshcode2
      , publish_sell.meshcode3
      , publish_sell.meshcode6 
    FROM
      publish_sell  
      LEFT JOIN tag_mapping_sell 
        ON (tag_mapping_sell.publish_id = publish_sell.publish_id and tag_id = 1015)
      LEFT JOIN mst_currency 
        ON publish_sell.currency_cd = mst_currency.currency_cd 
    WHERE
      publish_sell.publish_id IN ( 
        SELECT
          publish_id 
        from
          TMP_TABLE_1015_SELL 
        where
          id between :start_cnt AND :end_cnt
        )
  ) aa 
    ON ( 
      tag_mapping_sell.tag_id = aa.tag_id 
      AND tag_mapping_sell.publish_id = aa.publish_id    
    ) WHEN MATCHED THEN UPDATE 
SET
  tag_mapping_sell.tag_level = aa.tag_level WHEN NOT MATCHED THEN 
INSERT ( 
  tag_id
  , tag_level
  , publish_id
  , meshcode1
  , meshcode2
  , meshcode3
  , meshcode6
) 
VALUES ( 
  aa.tag_id
  , aa.tag_level
  , aa.publish_id
  , aa.meshcode1
  , aa.meshcode2
  , aa.meshcode3
  , aa.meshcode6
)";
$stmt_1015 = $pdo->prepare($sql_1015);
//echo "{$sql}\n";
//$log->freeform("null_update_sell", $sql_1015);


## テンポラリーテーブルのIDのMAX値取得し、ループのカウント数を決定する
$sql = "select max(id) max_num FROM TMP_TABLE_1016_SELL";
$stmt = $pdo->query($sql);
$row = $stmt->fetch();
$max_num = $row['max_num'];

$log->freeform("null_update_sell", "max_num(1016):{$max_num}");

# テーブルが空の場合は、処理を抜ける
if ($max_num==NULL || $max_num == ''){
	drop_table ();
	$log->freeform("null_update_sell", "[batch end]1016 has no data.");
	exit;
}
# 設定された回数ずつ処理を実行する
# 例外が発生した場合も、処理を継続する Exit しない
// 1016
$start_cnt = 1;
$offset = 1000;
//$max_num = 17523;		// 1016 の残データ件数
#　ループ回数を取得する
$loop_cnt = (int)($max_num / $offset) + 1;
$end_cnt= $start_cnt + $offset;
$log->freeform("null_update_sell", "loop_cnt(1016):{$loop_cnt}");
for ($i=1; $i<=$loop_cnt; $i++) {

	//echo "/" . $i; //これは進行をチェックするログ（画面に出力）
	$Msg = "{$i},1016,{$start_cnt},{$end_cnt}";

	// 1016 のデータを更新するSQLを実行する
	try {
		$time_start = microtime(true);
		$stmt_1016->bindParam(':start_cnt', $start_cnt, PDO::PARAM_INT);
		$stmt_1016->bindParam(':end_cnt', $end_cnt, PDO::PARAM_INT);
		$stmt_1016->execute();
//		$stmt_1016 = $pdo->query($sql_1016);
		$time = microtime(true) - $time_start;
		$Msg = $Msg . "," . sprintf("%.20f", $time);
	} catch (PDOException $e) {
		$msg = "MERGE(1016):" . $e->getMessage();
		$log->freeform("null_update_sell", $msg);
		sendErrMail($msg);
		$Msg = $Msg . "," . sprintf("ERROR");
	}

	// ループが終わるごとに、処理時間をログに出力する
	$log->freeform("null_update_time_sell", $Msg);

	$start_cnt = $end_cnt + 1;
	$end_cnt = $end_cnt + $offset;

}

## テンポラリーテーブルのIDのMAX値取得し、ループのカウント数を決定する
$sql = "select max(id) max_num FROM TMP_TABLE_1015_SELL";
$stmt = $pdo->query($sql);
$row = $stmt->fetch();
$max_num = $row['max_num'];

$log->freeform("null_update_sell", "max_num(1015):{$max_num}");

# テーブルが空の場合は、処理を抜ける
if ($max_num==NULL || $max_num == ''){
	drop_table ();
	$log->freeform("null_update_sell", "[batch end]1015 has no data.");
	exit;
}

$start_cnt = 1;
//$max_num = 19787;		// 1015 の残データ件数
#　ループ回数を取得する
$loop_cnt = (int)($max_num / $offset) + 1;
$end_cnt= $start_cnt + $offset;
$log->freeform("null_update_sell", "loop_cnt(1015):{$loop_cnt}");
for ($i=1; $i<=$loop_cnt; $i++) {

	$Msg = "{$i},1015,{$start_cnt},{$end_cnt}";

	// 1015 のデータを更新するSQLを実行する
	try {
		$time_start = microtime(true);

		$stmt_1015->bindParam(':start_cnt', $start_cnt, PDO::PARAM_INT);
		$stmt_1015->bindParam(':end_cnt', $end_cnt, PDO::PARAM_INT);
		$stmt_1015->execute();
//		$stmt_1015 = $pdo->query($sql_1015);

		$time = microtime(true) - $time_start;
		$Msg = $Msg.",".sprintf("%.20f", $time);
	} catch (PDOException $e) {
		$msg = "MERGE(1015):".$e->getMessage();
		$log->freeform("null_update_sell", $msg);
		sendErrMail($msg);
		$Msg = $Msg.",".sprintf("ERROR");
	}

	// ループが終わるごとに、処理時間をログに出力する
	$log->freeform("null_update_time_sell", $Msg);

	$start_cnt = $end_cnt + 1;
	$end_cnt = $end_cnt + $offset;
}

## テンポラリーテーブルの削除処理
//drop_table();


/**
 * テンポラリテーブル削除処理：　バッチ処理用に作成したテンポラリテーブルを削除する。
 *
 * 　・更新処理終了時
 * 　・テンポラリテーブル作成後のエラー発生時
 *
 */
function drop_table () {

	# グローバル変数を参照
	global $pdo;
	global $log;

	# バッチ用に作成したテンポラリーテーブルをデータを削除後、ドロップする
	$sql1 = "TRUNCATE TABLE TMP_TABLE_1016_SELL";
	$log->freeform("null_update_sell", $sql1);
	$sql2 = "TRUNCATE TABLE TMP_TABLE_1015_SELL";
	$log->freeform("null_update_sell", $sql2);
	$sql3 = "DROP TABLE TMP_TABLE_1016_SELL";
	$log->freeform("null_update_sell", $sql3);
	$sql4 = "DROP TABLE TMP_TABLE_1015_SELL";
	$log->freeform("null_update_sell", $sql4);


	try {
		$stmt = $pdo->query($sql1);
		$stmt = $pdo->query($sql2);
		$stmt = $pdo->query($sql3);
		$stmt = $pdo->query($sql4);
	} catch (PDOException $e) {
		$msg = "drop table:".$e->getMessage();
		$log->freeform("null_update_sell", $msg);
		sendErrMail($msg);
		//echo $e->getMessage();
		exit;
	}
	return;
}

/**
 * Errorメール送信処理：　例外発生時に、管理者へエラーメールを送信する。
 *
 */
function sendErrMail($body)
{

	mb_language("Japanese");
	mb_internal_encoding("UTF-8");


	$to = TO_MAIL_ADDR;     //エラーメールの送付先
	$header = "From: $to\nReply-To: $to\n";
	$title = '【ERROR】USD価格、NULLデータ再更新変更バッチ';

	try {
		mb_send_mail($to, $title, $body, $header);
		//echo "処理エラーメールを送信しました";
		return true;
	} catch (Exception $e) {
		//echo "エラーメールの送信に失敗しました";
		return false;
	}
}

# トータルの処理時間を取得
$time_total = microtime(true) - $time_start_batch;

#log
$log->freeform("null_update_sell", "total time:{$time_total} end");


?>