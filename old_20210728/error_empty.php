<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/qc4.propre.com/import/config.php";
require '/var/qc4.propre.com/import/dbclass.php';
require "/var/qc4.propre.com/import/log.php";

$db = DB::getInstance();
$log = new log();

/*
0が入っているエラーをきれいにする
mysql> select error from crawling_list where error=0 limit 10;
+-------+
| error |
+-------+
| 00    |
| 00    |
|       |
| 00    |
| 00    |
| 00    |
| 00    |
|       |
| 00    |
| 00    |
+-------+
10 rows in set (0.01 sec)

mysql> select error from crawling_list where error=0 and error<>'' limit 10;
+-------+
| error |
+-------+
| 000   |
| 000   |
| 0000  |
| 000   |
| 00    |
| 00    |
| 0     |
| 00    |
| 00    |
| 0000  |
+-------+
10 rows in set (0.00 sec)

+------------------------------+
| error                        |
+------------------------------+
| 00                           |
| 0                            |
| 00000                        |
| 00                           |
| m1m1m1m1m1m1m10m1m1m1m1m1m10 |
| 000                          |
| 00                           |
| 00                           |
| 00                           |
| 00                           |
+------------------------------+
10 rows in set (4.36 sec)

select crawl_id,error from crawling_list where error=0 and error<>'' limit 10;
*/

$limit_num=10;

$sql = "update crawling_list set error='' where error=0 and error<>'' limit " . $limit_num;

for ($i=0; $i<1000000; $i++){
	try{
		$stmt = $db->query($sql);
	}catch (PDOException $e) {
		echo $e->getMessage();
		continue;
	}
	if($i % 100 == 0){
		echo ($i * 10) . '/';
	}
}

?>
