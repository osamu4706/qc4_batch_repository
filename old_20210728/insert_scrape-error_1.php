<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/www/import/config.php";
require '/var/www/import/db_oracle.php';
require "/var/www/import/tools.php";
require "/var/www/import/log.php";

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);
$log = new log();

/*
INSERT INTO scrape SELECT * FROM scrape_20191029_2 WHERE crawl_id IN (SELECT crawl_id FROM tmp_serial_scrape WHERE id BETWEEN :start_id AND :end_id");
*/

$limit_num=1000;

$error_list=array(523001,647001);

$sql = "INSERT INTO scrape SELECT * FROM scrape_20191029_2 WHERE crawl_id IN (SELECT crawl_id FROM tmp_serial_scrape WHERE id BETWEEN :start_id AND :end_id)";
$stmt = $pdo_medium->prepare($sql);

for($i=0; $i <count($error_list); $i++){
	$dt_s = new DateTime();
	//100件分割
	for($j=0; $j <10; $j++){
		$start_id = $error_list[$i] + $j * 100 + 1;
		$end_id = $error_list[$i] + ($j + 1) * 100;
		$stmt->bindParam(':start_id', $start_id, PDO::PARAM_INT);
		$stmt->bindParam(':end_id', $end_id, PDO::PARAM_INT);
		$flg_error = 0;
		try{
			$stmt->execute();
		}catch (Exception $e) {  //duplicateエラー
			$log->freeform("insert_scrape-error_error", $start_id . ',' . $end_id  . ',' . $e->getMessage());
			//10件分割
			for($k=0; $k <10; $k++){
				$start_id_10 = $start_id + $k * 10 + 1;
				$end_id_10 = $start_id + ($k + 1) * 10;
				$stmt->bindParam(':start_id', $start_id_10, PDO::PARAM_INT);
				$stmt->bindParam(':end_id', $end_id_10, PDO::PARAM_INT);
				try{
					$stmt->execute();
				}catch (Exception $e) {  //duplicateエラー
					$log->freeform("insert_scrape-error_error", $start_id_10 . ',' . $end_id_10  . ',' . $e->getMessage());
					for($l=0; $l <10; $l++){
						//1件ごと
						$start_id_1 = $start_id + $k * 10 + $l + 1;
						$end_id_1 = $start_id + ($k + 1) * 10 + $l + 1;  //startと同じ
						$stmt->bindParam(':start_id', $start_id_1, PDO::PARAM_INT);
						$stmt->bindParam(':end_id', $end_id_1, PDO::PARAM_INT);
						try{
							$stmt->execute();
						}catch (Exception $e) {  //duplicateエラー
							$log->freeform("insert_scrape-error_error", $start_id_1 . ',' . $end_id_1  . ',' . $e->getMessage());
						}
					}
				}
			}
		}
	}
	$dt_e = new DateTime();
	$elapsed = $dt_e->diff($dt_s)->format("%s.%f");
	$log->freeform("insert_scrape-error_" . $error_list[0], $start_id . ',' . $end_id  . ',' . $elapsed);

	if($i % 10 == 0){
		echo $i . ' ';
	}
}

?>
