<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/qc4.propre.com/import/config.php";
require '/var/qc4.propre.com/import/dbclass.php';
require "/var/qc4.propre.com/import/log.php";

$db = DB::getInstance();
$log = new log();

$start_num=0;
$limit_num=1000; //1回で処理する件数。効率を保てる程度にできるだけ細かくする。
$total_num=10000000;

$sql = "DELETE FROM cleansing_sell WHERE crawl_id > :from_no AND crawl_id < :to_no";
$stmt = $db->prepare($sql);

for ($i=0; $i<($total_num / $limit_num); $i++){
	$from_no = $i * $limit_num;
	$to_no = $i * $limit_num + $limit_num;
	$stmt->bindParam(':from_no', $from_no, PDO::PARAM_INT);
	$stmt->bindParam(':to_no', $to_no, PDO::PARAM_INT);
	$stmt->execute();
	if($i % 100 ==0){
		echo $i * $limit_num . '/';
	}
}

?>
