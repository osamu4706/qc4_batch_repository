<?php

require "/var/www/import/config.php";
require '/var/www/import/db_oracle.php';
require "/var/www/import/tools.php";
require "/var/www/import/log.php";

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_high = DB::getPdo(DB_ORA_TNS_HIGH);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);

$log = new log();

$num = 10000;
$from_id = 1;
$to_id = 100000; 

$sql = "INSERT INTO tag_mapping_rent_20200319 SELECT tag_id,tag_level,publish_id,meshcode1,meshcode2,meshcode3,meshcode6 FROM tmp_serial_tag_mapping_rent WHERE id BETWEEN :start_id AND :end_id";
$stmt = $pdo_tp->prepare($sql);

for($i= (int)($from_id/$num) + 1; $i <= (int)($to_id/$num); $i++){
	$dt_s = new DateTime();
	$start_id = $i * $num + 1;
	$end_id = ($i+1) * $num;
	$stmt->bindParam(':start_id', $start_id, PDO::PARAM_INT);
	$stmt->bindParam(':end_id', $end_id, PDO::PARAM_INT);
	$flg_error = 0;
	try{
		$stmt->execute();
	}catch (Exception $e) {
		$log->freeform("tag_mapping_error", $start_id . ',' . $end_id  . ',' . $e->getMessage());
		$flg_error = 1;
	}
	$dt_e = new DateTime();
	$elapsed = $dt_e->diff($dt_s)->format("%s.%f");
	$log->freeform("tag_mapping_" . $from_id, $flg_error . ',' . $start_id . ',' . $end_id  . ',' . $elapsed);

	if($i % 10 == 0){
		echo $i . ' ';
	}
}

?>