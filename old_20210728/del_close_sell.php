<?php

require "/var/www/import/config.php";
require '/var/www/import/db_oracle.php';
require "/var/www/import/tools.php";
require "/var/www/import/log.php";

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_high = DB::getPdo(DB_ORA_TNS_HIGH);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);

$log = new log();

//注意：最後の端数はバインド数エラーが出るはずなので手動で残りを削除する

$num = 1000;  // ORA-01795: maximum number of expressions in a list is 1000;
$total = 10265187;
$i_start = 320; 

$sql = "SELECT publish_id FROM (SELECT publish_id,rownum AS nm FROM close_sell WHERE open_num=0) WHERE nm BETWEEN :start_num and :end_num";
$stmt = $pdo_tp->prepare($sql);

$in_txt = '?';
for($i=1;$i<$num;$i++){
	$in_txt .= ',?';
}

$sql2 = "DELETE FROM map_sell WHERE publish_id IN (" . $in_txt . ")";  //$num分の?,?,?...
$stmt2 = $pdo_tp->prepare($sql2);

//echo $sql2;

for($i=$i_start;$i<=$total/$num;$i++){
	$start_num = $i * $num + 1;
	$end_num = ($i+1) * $num;
//echo $start_num. '/' .$end_num;
	$stmt->bindParam(':start_num', $start_num, PDO::PARAM_INT);
	$stmt->bindParam(':end_num', $end_num, PDO::PARAM_INT);
	$stmt->execute();
	$rows = $stmt->fetchAll();
	$publish_ids = '';
	$j=0;
	foreach($rows as $row){
		$j++;
		$publish_id = $row['publish_id'];
		$stmt2->bindValue($j, $publish_id, PDO::PARAM_INT);    //bindパラメーターの付番は0ではなく1スタート（Columns/Parameters are 1-based）。
//echo '/'.$publish_id;
		$publish_ids .= ',' .$publish_id;
	}
	$stmt2->execute();

	$log->freeform("del_mapsell", $publish_ids);

	if($i % 10 == 0){
		echo ($i * $num) . ' ';
	}
}

?>