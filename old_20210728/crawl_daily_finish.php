<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/qc4.propre.com/import/config.php";
require '/var/qc4.propre.com/import/dbclass.php';
require "/var/qc4.propre.com/import/log.php";

$db = DB::getInstance();
$log = new log();

/**
毎時0,10,20,30,40,50分に実行
0,10,20,30,40,50 * * * * php /var/qc4.propre.com/batch/crawl_daily_finish.php
全ピッカーが当日分完了したサイトNo.に対して、
クロールリストの当日クロール分（ymd＝当日）のステータスが全て→当日は解除。ひとまず全レコードのステータスに変更
100:クレンジングまで正常完了
200:掲載終了
201:データ取扱完了(想定内)
210:同一物件他データにより掲載終了扱い
のいずれかになっているサイトを、
完了YMD	yyyymmddに当日をセットする。
*/

#ymd作成
$dt = new DateTime();
$ymd = $dt->format('Ymd');

#全ピッカーが当日分完了したサイトNo.を取得  //当日稼働していないpickerは完了判定から除外
$sql = "SELECT DISTINCT a.site_no FROM picker_target a WHERE a.status=200 AND a.yyyymmdd=:yyyymmdd AND a.site_no NOT IN (SELECT b.site_no FROM picker_target b WHERE b.yyyymmdd=:yyyymmdd AND b.status<>200)";
$stmt = $db->prepare($sql);

#クロールリストが全て完了かどうか
$sql2 = "SELECT count(*) AS cnt FROM crawling_list WHERE site_no=:site_no AND status NOT IN (100,200,201,210)";
$stmt2 = $db->prepare($sql2);

#クロール設定を当日分クロール～クレンジング完了にセット
$sql3 = "UPDATE crawl_setting SET yyyymmdd=:yyyymmdd WHERE site_no=:site_no";
$stmt3 = $db->prepare($sql3);
/*
$stmt->bindValue(':yyyymmdd', $ymd, PDO::PARAM_INT);
$stmt->execute();
$rows = $stmt->fetchAll();
foreach($rows as $row){
	$target_site_no = $row['site_no'];
	$stmt2->bindValue(':site_no', $target_site_no, PDO::PARAM_INT);
	$stmt2->execute();
	$row2 = $stmt2->fetch();
	if($row2['cnt'] == 0){  //クロールリストのステータスが全て100,200,201,210
		$stmt3->bindValue(':site_no', $target_site_no, PDO::PARAM_INT);
		$stmt3->bindValue(':yyyymmdd', $ymd, PDO::PARAM_INT);
		$stmt3->execute();
	}
}
*/
#ログ
error_log('[Batch] crawl_daily_finish.php実行');

?>
