<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/www/import/config.php";
require '/var/www/import/db_oracle.php';
require "/var/www/import/tools.php";
require "/var/www/import/log.php";

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);
$log = new log();

/*
cronで定期的に稼働することが前提のプログラム
日付ごとにステータスを変えていく
3300/3311/3320→40(スクレイプ＆クレンジング)
3330→50(クレンジング)
(1)40or50のmin(created)を取得
//(2)min(created)を対象に3300/3311/3320→40または3330→50を全件実行
再再クレンジングで変更
(2)min(created)を対象に1321/3300/3311/3320/3330→50を全件実行
(3)→40の場合crawl_setting.rescrape_num=1,crawl_setting.recleansing_num=0にセット
   →50の場合crawl_setting.rescrape_num=0,crawl_setting.recleansing_num=1にセット
*/
$logfile='recleansing_250';

//$site_no=7;
$site_no = array(42);
$site_no_sql = '';
if(count($site_no)==0){
	exit;
}else if(count($site_no)==1){
	$site_no_sql = ' = ' . $site_no[0] . ' ';
}else{
	$site_no_sql = ' in(';
	for($i=0; $i<count($site_no); $i++){
		if($i>0){
			$site_no_sql .= ',';
		}
		$site_no_sql .= $site_no[$i];
	}
	$site_no_sql .= ') ';
}
/*ターゲットのsite_noを決めて日付取得、min日付ごとにセットで回す必要あり*/

$limit_num=1000;

$from_status=9999;
$to_status=9999;

//$sql = "SELECT count(*) AS cnt FROM crawling_list WHERE site_no=" . $site_no . " AND status in (40,50)";
$sql = "SELECT count(*) AS cnt FROM crawling_list WHERE site_no " . $site_no_sql . " AND status = 50";
echo $sql;
$stmt = $pdo_medium->query($sql);
$row = $stmt->fetch();
if($row['cnt'] > 0){
	//前のバッチでの修正分の再クレンジングが完了していないため終了
	$log->freeform($logfile, '前バッチ分のクレンジング処理完了待ちで終了');
	exit;
}


//$sql = "SELECT min(TO_CHAR(created, 'YYYYMMDD')) AS created_yyyymmdd FROM crawling_list WHERE site_no=" . $site_no . " AND status in (3300,3311,3320) ";
//$stmt = $pdo_medium->query($sql);
//$row = $stmt->fetch();
$min_40 = 99999999;
//if(isset($row['created_yyyymmdd'])){
//	$min_40 = $row['created_yyyymmdd'];
//}

//$sql = "SELECT min(TO_CHAR(created, 'YYYYMMDD')) AS created_yyyymmdd FROM crawling_list WHERE site_no=" . $site_no . " AND status in (1321,3300,3311,3320,3330)";
$sql = "SELECT min(TO_CHAR(created, 'YYYYMMDD')) AS created_yyyymmdd FROM crawling_list WHERE site_no" . $site_no_sql . " AND status =3330";
$stmt = $pdo_medium->query($sql);
$row = $stmt->fetch();
$min_50 = 99999999;
if(isset($row['created_yyyymmdd'])){
	$min_50 = $row['created_yyyymmdd'];
}

$ymd=99999999;

//$log->freeform($logfile, $min_40 . ':' . $min_50);

if($min_40 == 99999999 && $min_50 ==99999999){
	//両方結果なし
	$log->freeform($logfile, '再クレンジング全処理完了！');
	$sql = "UPDATE crawl_setting SET rescrape_num=0, recleansing_num=0 WHERE site_no" . $site_no_sql;
	$stmt = $pdo_tp->query($sql);
	exit;
}else{
	$sql2="";
//	if($min_40<=$min_50){
//		$from_status= '(3300,3311,3320)';
//		$to_status=40;
//		$ymd = $min_40;
//		$sql2 = "UPDATE crawl_setting SET rescrape_num=1, recleansing_num=0 WHERE site_no=" . $site_no;
//	}else{
		$from_status='(3330)';
//		$from_status='(1321,3300,3311,3320,3330)';
//		$from_status='(330,2330)';
		$to_status=50;
		$ymd = $min_50;
//	}
	$sql = "SELECT site_no,count(*) AS cnt FROM  crawling_list WHERE site_no" . $site_no_sql . " AND status in " . $from_status . " AND TO_CHAR(created, 'YYYYMMDD')=:ymd GROUP BY site_no";
//	$sql = "SELECT count(*) AS cnt FROM  crawling_list WHERE site_no=" . $site_no . " AND status =330 AND TO_CHAR(created, 'YYYYMMDD')=:ymd";
	$stmt = $pdo_medium->prepare($sql);
	$stmt->bindParam(':ymd', $ymd, PDO::PARAM_STR);
	$stmt->execute();
	$rows = $stmt->fetchAll();
	//同一日でヒットしたサイトの数だけ回す
	foreach ($rows as $row) {
		$target_site_no = $row['site_no'];
		$cnt = $row['cnt'];
		$sql = "UPDATE crawling_list SET status=" . $to_status . " WHERE site_no=" . $target_site_no . " AND status in " . $from_status . " AND TO_CHAR(created, 'YYYYMMDD')=:ymd AND rownum<=:limit_num";
		$stmt = $pdo_medium->prepare($sql);
		for ($i=0; $i<(int)(($cnt -1) / $limit_num) + 1; $i++){
			$stmt->bindParam(':limit_num', $limit_num, PDO::PARAM_INT);
			$stmt->bindParam(':ymd', $ymd, PDO::PARAM_STR);
			$stmt->execute();
			$log->freeform($logfile, $ymd . ':' . $to_status . '->' . ($i + 1)*$limit_num . '/' . $cnt);
			if($i ==0){
				$sql2 = "UPDATE crawl_setting SET rescrape_num=0, recleansing_num=1 WHERE site_no=" . $target_site_no;
				$stmt2 = $pdo_tp->query($sql2);  //rescrape_num,recleansing_num数は1件目の時点で変えてすぐにdequeues処理に入れるようにする
				$log->freeform($logfile, 'crawl_setting変更完了');
			}
		}
		$log->freeform($logfile, $ymd . '(' . $target_site_no . '):' . $to_status . '->' . $cnt . '件ステータス変更(再クレンジング指示)処理完了');
	}
}
?>
