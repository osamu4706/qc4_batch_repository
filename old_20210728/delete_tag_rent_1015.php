<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/www/import/config.php";
require '/var/www/import/db_oracle.php';
require "/var/www/import/tools.php";
require "/var/www/import/log.php";

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);
$log = new log();

/*
USDタグの付け直しのため1015,1016を全部削除
*/

$tag_id=1015;
$listing_type="rent";
$limit_num=1000;
$target_num=20660000;
/*
rent1015:20656887
rent1016:17634366
sell1015:8287350
sell1016:6662039
*/

echo $target_num/$limit_num . ":start";
for($i=0; $i<=$target_num/$limit_num; $i++){
	$sql = "DELETE /*+INDEX(a tag_mapping_".$listing_type."_pk)*/ FROM tag_mapping_".$listing_type." a WHERE tag_id=". $tag_id ." AND rownum<=" . $limit_num;
	$stmt = $pdo_tp->query($sql);
	if($i % 10 == 0){
		echo $i."/";
	}
}

?>
