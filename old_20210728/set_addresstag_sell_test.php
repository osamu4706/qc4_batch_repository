<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/qc4.propre.com/import/config.php";
require '/var/qc4.propre.com/import/dbclass_test.php';
require "/var/qc4.propre.com/import/log.php";

$db = DB::getInstance();
$log = new log();

/*
アドレスタグをつける
処理用にlocal_flg5を利用
null/0：未処理
-1：処理中
1：タグ付け完了
2：タグ付け失敗（要調査）
*/

$recheck_num=1000000;  //処理数( * 10：limit数回す)
$status=0;  //セットするlocal_flg5

//$sql = "SELECT publish_id,lat,lon FROM publish_sell WHERE local_flg5 IS NULL AND yyyymmdd=99999999 LIMIT 1";
$sql = "SELECT publish_id,lat,lon,meshcode1,meshcode2,meshcode3,meshcode6 FROM propre_development.publish_sell WHERE (local_flg5 IS NULL OR local_flg5=0) AND lat IS NOT NULL AND lon IS NOT NULL LIMIT 1 FOR UPDATE";
//$sql = "SELECT publish_id,lat,lon,meshcode1,meshcode2,meshcode3,meshcode6 FROM propre_development.publish_sell WHERE (local_flg5 IS NULL OR local_flg5=0) AND lat IS NOT NULL AND lon IS NOT NULL LIMIT 10 FOR UPDATE";

$sql2 = "UPDATE propre_development.publish_sell SET local_flg5 = -1 WHERE publish_id=:publish_id";
//$sql2 = "UPDATE propre_development.publish_rent SET local_flg5 = -1 WHERE publish_id IN (:publish_ids)";
$stmt2 = $db->pdo->prepare($sql2);

$sql3 = "INSERT INTO propre_development.tag_mapping_sell VALUES (:tag_id, 0, :publish_id, :meshcode1, :meshcode2, :meshcode3, :meshcode6) ON DUPLICATE KEY UPDATE meshcode1=meshcode1";
$stmt3 = $db->pdo->prepare($sql3);

$sql4 = "UPDATE propre_development.publish_sell SET local_flg5 = :status WHERE publish_id=:publish_id";
$stmt4 = $db->pdo->prepare($sql4);

$error_cnt = 0;

for($i=0; $i<$recheck_num; $i++){
	if($error_cnt > 10){
		break;
	}
	$start_micro = microtime(true);
	try{
		$db->transaction();
		$stmt = $db->pdo->query($sql);
		$row = $stmt->fetch();
		$publish_id = $row['publish_id'];
		$stmt2->bindParam(':publish_id', $publish_id, PDO::PARAM_INT);
		$stmt2->execute();
		$db->commit();
		$fetch_micro = microtime(true);

		$lat = $row['lat'];
		$lon = $row['lon'];
		$meshcode1 = $row['meshcode1'];
		$meshcode2 = $row['meshcode2'];
		$meshcode3 = $row['meshcode3'];
		$meshcode6 = $row['meshcode6'];
		//gisapiからtag_idを取得する
		$data_post = array('id'=>'12346789','access_key'=>'vV38XKmk','lat'=>$lat,'lon'=>$lon);
		$data_post_json = json_encode($data_post);
		$start_curl_micro = microtime(true);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, 'http://gisapi.propre.com/tag_address.php');
		$post_result=curl_exec($ch);
		curl_close($ch);
		$end_curl_micro = microtime(true);
		$post_result_json=json_decode($post_result, true);
		$return_cd = $post_result_json['return_cd'];
		$tag_ids = $post_result_json['tag_ids'];  //「,」区切り文字列
		$arr_tag_ids = explode(",", $tag_ids);
		if($return_cd == 100){
			foreach($arr_tag_ids as $tag_id){
				$stmt3->bindParam(':tag_id', $tag_id, PDO::PARAM_INT);
				$stmt3->bindParam(':publish_id', $publish_id, PDO::PARAM_INT);
				$stmt3->bindParam(':meshcode1', $meshcode1, PDO::PARAM_INT);
				$stmt3->bindParam(':meshcode2', $meshcode2, PDO::PARAM_INT);
				$stmt3->bindParam(':meshcode3', $meshcode3, PDO::PARAM_INT);
				$stmt3->bindParam(':meshcode6', $meshcode6, PDO::PARAM_INT);
				$stmt3->execute();
			}
			$status = 1;
		}else{
			$status = 2;
		}
		$stmt4->bindParam(':status', $status, PDO::PARAM_INT);
		$stmt4->bindParam(':publish_id', $publish_id, PDO::PARAM_INT);
		$stmt4->execute();
	
	}catch (PDOException $e) {
		$error_cnt++;
		echo $e->getMessage();
		continue;
	}

	$reg_micro = microtime(true);
	$log->freeform("set_addrestag_sell_v3", '(' . ($fetch_micro - $start_micro) . '/' . ($end_curl_micro - $start_curl_micro) . '/' . ($reg_micro - $end_curl_micro) . '/'  . $i . ')' . $publish_id . ':' . $post_result);

	if($i % 10 ==0){
		echo $i . '/';  //セッション切れ対策
	}
}

?>
