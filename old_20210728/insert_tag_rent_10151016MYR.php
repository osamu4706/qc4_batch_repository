<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/www/import/config.php";
require '/var/www/import/db_oracle.php';
require "/var/www/import/tools.php";
require "/var/www/import/log.php";

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);
$log = new log();

/*
TWD	台湾 ドル	0.03517
HKD	香港ドル	0.12902
CHF	スイス／フラン	1.1051
CNY	中国人民元	0.15207
EUR	ユーロ	1.196
PHP	フィリピン／ペソ	0.02078
USD	アメリカ／ドル	1
SGD	シンガポール／ドル	0.74732
NZD	ニュージランド／ドル	0.70183
AUD	オーストラリア／ドル	0.7387
THB	タイ／バーツ	0.03298
JPY	日本／円	0.009605
MMK	ミャンマー／チャット	0.0007629
GBP	イギリス／ポンド	1.3306
MYR	マレーシア／リンギッド	0.2465
VND	ベトナム／ドン	0.0000433112
IDR	インドネシア／ルピー	0.0000710322
CAD	カナダ／ドル	0.76966
*/

$tag_id=1016;
$listing_type="rent";
$currency="MYR";
$rate=0.2465;

$limit_num=1000;
$target_num=0;

$sql = "SELECT MAX(id) AS num FROM tmp_tag_".$listing_type."_".$currency;
$stmt = $pdo_tp->query($sql);
$row = $stmt->fetch();
$target_num = (int)$row['num'];

echo $target_num/$limit_num . ":start";
$sql1015 = "INSERT INTO tag_mapping_".$listing_type." SELECT 1015,price*".$rate."*1000,publish_id,meshcode1,meshcode2,meshcode3,meshcode6 FROM tmp_tag_".$listing_type."_".$currency." WHERE id BETWEEN :start_id AND :end_id";
$sql1016 = "INSERT INTO tag_mapping_".$listing_type." SELECT 1016,price*".$rate."/floor_sqft*1000,publish_id,meshcode1,meshcode2,meshcode3,meshcode6 FROM tmp_tag_".$listing_type."_".$currency." WHERE id BETWEEN :start_id AND :end_id AND floor_sqft>0";
$stmt1015 = $pdo_tp->prepare($sql1015);
$stmt1016 = $pdo_tp->prepare($sql1016);
for($i=0; $i<=$target_num/$limit_num; $i++){
	$start_id = $i * $limit_num + 1;
	$end_id = ($i + 1) * $limit_num;
	$stmt1015->bindParam(':start_id', $start_id, PDO::PARAM_INT);
	$stmt1015->bindParam(':end_id', $end_id, PDO::PARAM_INT);
	$stmt1015->execute();
	$stmt1016->bindParam(':start_id', $start_id, PDO::PARAM_INT);
	$stmt1016->bindParam(':end_id', $end_id, PDO::PARAM_INT);
	$stmt1016->execute();
	if($i % 10 == 0){
		echo $i."/";
	}
}

?>
