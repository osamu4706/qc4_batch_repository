<?php

if (PHP_OS == "WIN32" || PHP_OS == "WINNT") {
    // Windwos用の処理
    require "C:\SRC\dev_saikio_batch\import\config.php";
    require "C:\SRC\dev_saikio_batch\import\db_oracle.php";
    require "C:\SRC\dev_saikio_batch\import\\tools.php";
    require "C:\SRC\dev_saikio_batch\import\log.php";
    require "C:\SRC\dev_saikio_batch\import\crawler_setting_data.php";
} else {
    // サーバ環境用の処理
    require "/var/www/import/config.php";
    require '/var/www/import/db_oracle.php';
    require "/var/www/import/tools.php";
    require "/var/www/import/log.php";
    require "/var/www/import/crawler_setting_data.php";
}

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_high = DB::getPdo(DB_ORA_TNS_HIGH);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);

$log = new log();
$dt_start = new DateTime();
$dt_start = $dt_start->format("%I:%S.%F");
$logfile='publish_analysis_table_upload_'.$dt_start;
$limit_num=100;
$target_num=10000000;

if( $argc != 3 ){
    echo "[1] loop_from\n";
    echo "[2] loop_to\n";
    exit;
}
if($argv[1]=='') die("loop_from");
if($argv[2]=='') die("loop_to");

$loop_from = $argv[1];
$loop_to = $argv[2];

$logfile='publish_analysis_table_upload_'.$loop_from.'_'.$loop_to;

$publish_up_num=0;

$sql = "select distinct publish_id from propre.tmp_saikio_del_392_3_sell where id BETWEEN ".$loop_from." AND ".$loop_to;
$stmt = $pdo_tp->prepare($sql);
$stmt->execute();
$rows = $stmt->fetchAll();
$dt_loop_in = new DateTime();
$log->freeform($logfile,"処理開始▷".$loop_from." TO ".$loop_to."検索対象:".count($rows).'件');



for($i=0;$i<count($rows);$i++){

    //対象のpublish_idを設定
    $publish_id=$rows[$i]['publish_id'];
    //changed_numの最大値取得
    $get_max_change="select max(changed_num) as t_max from propre.cleansing_sell where publish_id = ".$publish_id;
    $stmt = $pdo_tp->prepare($get_max_change);
    $stmt->execute();
    $get_maxchanged_rows = $stmt->fetch();

    //changed_numの最大値に対応したcrawl_id取得
    $fresh_crawl_id_sql="select crawl_id from propre.cleansing_sell where changed_num = ".$get_maxchanged_rows['t_max'] ." and publish_id = ".$publish_id;
    $fresh_crawl_id_stmt = $pdo_tp->prepare($fresh_crawl_id_sql);
    $fresh_crawl_id_stmt->execute();
    $fresh_crawl_id = $fresh_crawl_id_stmt->fetch();

    //諸条件でデータが存在するか確認
    $scrape_search_sql="select count(*) as cnt from propre.scrape s where crawl_id = ".$fresh_crawl_id['crawl_id']." and site_no=3 and s.scraped_data.renovated != '-' and s.scraped_data.renovated is not null";
    $stmt = $pdo_tp->prepare($scrape_search_sql);
    $stmt->execute();
    $target_rows = $stmt->fetch();

    if($target_rows['cnt']>0){

        $e="select s.scraped_data.renovated from propre.scrape s where crawl_id = ".$fresh_crawl_id['crawl_id'];
        $e_stmt = $pdo_tp->prepare($e);
        $e_stmt->execute();
        $e_rows = $e_stmt->fetch();

        try{
            $log->freeform($logfile,"test▷".$e_rows['renovated']);

            $renovated_data="'{\"renovated\" : \"".$e_rows['renovated']."\"}'";
//            print_r($renovated_data);

            //publish_sellテーブルの更新
            $publish_update_sql = "UPDATE propre.publish_sell pb_sell SET local_flg3=1, crawl_info = json_mergepatch(crawl_info, ".$renovated_data.") WHERE publish_id = ".$publish_id;

            $publish_update_stmt = $pdo_tp->query($publish_update_sql);

            $publish_up_num = $publish_up_num + 1;

            $dt_loop_exec = new DateTime();
            $elapsed = $dt_loop_exec->diff($dt_loop_in)->format("%I:%S.%F");
            $log->freeform($logfile, 'PUBLISH_TABLEを'.$publish_up_num.'件更新->' . $elapsed);

        }catch (Exception $ex){

            printf("%d\n",$publish_id);

        }


//        $renovated_data=$e_rows['renovated'];
//      publish_id=711639202


    }

}

$dt_loop_exec_last = new DateTime();
$elapsed_last = $dt_loop_exec_last->diff($dt_loop_in)->format("%I:%S.%F");
$log->freeform($logfile, '処理完了!!' . $elapsed_last);

exit;

?>
