<?php

require '../import/dbclass.php';
require "../import/log.php";
require "../import/worldmesh.php";

$db = DB::getInstance();


$sql2 = 'UPDATE spot SET meshcode3=:m3 WHERE spot_id=:spot_id';
$stmt2 = $db->prepare($sql2);

$sql = 'SELECT spot_id,center_lat,center_lon FROM spot WHERE meshcode3=0 LIMIT :num';
$stmt = $db->prepare($sql);

$loop = 754;
$num = 1000;

for($i=0; $i<$loop;$i++){
	$start = $i * $num;
	$stmt->bindParam(':num', $num, PDO::PARAM_INT);
	$stmt->execute();
	$rows = $stmt->fetchAll();
	$cnt = 0;
	foreach($rows as $row){
		$cnt++;
		$m3=cal_meshcode3($row['center_lat'],$row['center_lon']);
		$stmt2->bindParam(':m3', $m3, PDO::PARAM_INT);
		$stmt2->bindParam(':spot_id', $row['spot_id'], PDO::PARAM_INT);
		$stmt2->execute();
	}
	echo $i;
}
echo 'end';

