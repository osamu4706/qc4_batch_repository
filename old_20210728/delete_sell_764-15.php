<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/www/import/config.php";
require '/var/www/import/db_oracle.php';
require "/var/www/import/tools.php";
require "/var/www/import/log.php";

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);

/*
*/

$country_cd=764;
$site_no=15;
$listing_type='sell';
$limit_num=100;
$target_num=1000000;

$log = new log();
$logfile='delete_'.$listing_type.'-'.$country_cd.'-'.$site_no;
$log->freeform($logfile, 'START');
$dt_start = new DateTime();

$loop_max = $target_num / $target_num;
$loop_num=0;
$dt_loop_in = new DateTime();
$dt_loop_out = new DateTime();
//対象publish_id抽出
$sql = "SELECT publish_id FROM cleansing_" . $listing_type . " WHERE site_no=" . $site_no . " AND rownum<=" . $limit_num;
$stmt = $pdo_tp->prepare($sql);
for($i=0; $i<=$target_num/$limit_num; $i++){
	$loop_num++;
	$dt_loop_in = new DateTime();
//$log->freeform($logfile, $sql);
	$stmt->execute();
	$dt_loop_exec = new DateTime();
	$elapsed = $dt_loop_exec->diff($dt_loop_in)->format("%I:%S.%F");
	$log->freeform($logfile, 'ループ開始(' . $loop_num .'件) publish_id取得時間-> ' . $elapsed);
	$rows = $stmt->fetchAll();
	$in_clause = '';
//1件ずつ処理する
	$sql1 = "DELETE FROM tag_mapping_" . $listing_type . " WHERE publish_id=:publish_id";
	$sql2 = "DELETE FROM cleansing_" . $listing_type . " WHERE publish_id=:publish_id AND country_cd=" . $country_cd;
	$sql3 = "DELETE FROM publish_sell WHERE publish_id=:publish_id";
	$stmt1 = $pdo_tp->prepare($sql1);
	$stmt2 = $pdo_tp->prepare($sql2);
	$stmt3 = $pdo_tp->prepare($sql3);
	foreach ($rows as $row) {
		$publish_id = $row['publish_id'];
		$stmt1->bindParam(':publish_id', $publish_id, PDO::PARAM_INT);
		$stmt2->bindParam(':publish_id', $publish_id, PDO::PARAM_INT);
		$stmt3->bindParam(':publish_id', $publish_id, PDO::PARAM_INT);
echo 'a';
		$dt1 = new DateTime;
		$stmt1->execute();
echo 'b';
		$dt2 = new DateTime;
		$stmt2->execute();
echo 'c';
		$dt3 = new DateTime;
		$stmt3->execute();
echo 'd';
		$dt4 = new DateTime;
		$elapsed1 = $dt2->diff($dt1)->format("%I:%S.%F");
		$elapsed2 = $dt3->diff($dt2)->format("%I:%S.%F");
		$elapsed3 = $dt4->diff($dt3)->format("%I:%S.%F");
		$log->freeform($logfile, $publish_id . '/' . $elapsed1 . '/' . $elapsed2 . '/' . $elapsed3);
	}
	$dt_loop_out = new DateTime();
	$elapsed = $dt_loop_out->diff($dt_loop_in)->format("%H:%I:%S.%F");
	$log->freeform($logfile, 'ループ終了(' . $loop_num .'件) -> ' . $elapsed);
	if($loop_num % 10 == 0){
		echo $loop_num * $limit_num . ' ';
	}
}
$dt_end = new DateTime();
$elapsed = $dt_end->diff($dt_start)->format("%H:%I:%S.%F");
$log->freeform($logfile, '処理終了 -> ' . $elapsed);

?>
