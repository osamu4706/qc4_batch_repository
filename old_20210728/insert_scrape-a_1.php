<?php

/** 実行環境から見るので絶対パスで指定 */
require "/var/www/import/config.php";
require '/var/www/import/db_oracle.php';
require "/var/www/import/tools.php";
require "/var/www/import/log.php";

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);
$log = new log();

/*
INSERT INTO scrape SELECT * FROM scrape_20191029_2 WHERE crawl_id IN (SELECT crawl_id FROM tmp_serial_scrape WHERE id BETWEEN :start_id AND :end_id");
*/

$limit_num=1000;

$error_list=array(523001,647001,905001,1807001,2157001,2217001,2381001,2503001,2511001,2654001,2795001,2955001,2979001,3156001,3316001,3384001,3453001,3865001,3986001,4365001,4672001,4727001,4735001,4977001,5036001,6055001,6078001,6205001,6372001,6414001,6490001,6508001,6610001,6817001,7385001,7664001,7701001,7850001,8064001,8208001,8279001,8802001,8890001,9135001,9200001,9495001,9844001,9909001,10254001,10621001,10749001,11000001,11120001,11141001,12091001,12116001,12249001,12582001,12583001,12607001,13083001,13208001,13385001,13464001,13499001,13547001,13942001,14229001,14278001,14700001,14728001,14770001,14798001,14806001,14808001,14828001,14880001,14949001,15072001,15432001,15640001,15701001,15782001,16494001,16522001,16631001,16823001,16906001,17420001,17545001,17654001,17856001,17963001,18196001,18206001,18278001,18491001,18543001,18619001,18807001,19254001,19573001,19707001,21062001,21266001,21281001,21362001,21708001,22044001,22176001,22267001,22295001,22327001,22410001,22590001,22730001,22895001);

$sql = "select distinct c.crawl_id from crawling_list c left outer join scrape s on c.crawl_id=s.crawl_id and s.crawl_id in (select crawl_id from tmp_serial_scrape where id between :start_id AND :end_id) where s.scraped_data is not null";
$stmt = $pdo_tp->prepare($sql);

$crawl_ids='';

for($i=0; $i <count($error_list); $i++){
	$dt_s = new DateTime();
	$start_id = $error_list[$i];
	$end_id = $error_list[$i] + 999;
	$stmt->bindParam(':start_id', $start_id, PDO::PARAM_INT);
	$stmt->bindParam(':end_id', $end_id, PDO::PARAM_INT);
	$stmt->execute();
	$rows = $stmt->fetchAll();
	foreach($rows as $row){
		$crawl_id = $row['crawl_id'];
		$crawl_ids .= ',' . $crawl_id;
	}
}
$log->freeform("insert_rest", $crawl_ids);

?>
