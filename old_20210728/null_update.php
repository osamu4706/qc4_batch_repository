<?php

/** 実行ファイルから見たパスで指定 */
require __DIR__.'/../import/config.php';
require __DIR__.'/../import/db_oracle.php';
require __DIR__.'/../import/log.php';


# エラーメールの送付先
define('TO_MAIL_ADDR', 'develop@propre.com');
# 一度の処理件数
//define('PROCESS_NUM', 500);

# グローバル変数
$pdo = DB::getPdo(DB_ORA_TNS_HIGH);
$log = new log();

# バッチ処理のトータル時間
$time_start_batch = microtime(true);

//$tag_id = 1016;
//$time_start = microtime(true);

#　ループ回数を取得する
//$loop_cnt = (int)($max_num / $offset) + 1;
//$end_cnt= $start_cnt + $offset;

//$log->freeform("null_update", "({$currency_cd}) start_cnt:{$start_cnt},max_num:{$max_num},loop_cnt:{$loop_cnt}");

//$time_start_cd = microtime(true);

$sql_1016 = "MERGE 
INTO tag_mapping_sell 
  USING ( 
    SELECT
       1016 tag_id
      , case when floor( 
        publish_sell.unit_price * mst_currency.exchange_rate * 100
      ) is null then 0
      else  floor( 
        publish_sell.unit_price * mst_currency.exchange_rate * 100
      ) end tag_level
      , publish_sell.publish_id
      , publish_sell.meshcode1
      , publish_sell.meshcode2
      , publish_sell.meshcode3
      , publish_sell.meshcode6 
    FROM
      publish_sell  
      LEFT JOIN tag_mapping_sell 
        ON (tag_mapping_sell.publish_id = publish_sell.publish_id and tag_id = 1016)
      LEFT JOIN mst_currency 
        ON publish_sell.currency_cd = mst_currency.currency_cd 
    WHERE
      (tag_mapping_sell.tag_level IS NULL or tag_mapping_sell.tag_level = 0)
      and rownum < 10000
      AND publish_sell.currency_cd != 'USD'
      and publish_sell.unit_price <> 0
      and publish_sell.unit_price is not null
      and publish_sell.unit_price < 999999999
  ) aa 
    ON ( 
      tag_mapping_sell.tag_id = aa.tag_id 
      AND tag_mapping_sell.publish_id = aa.publish_id
     -- AND tag_mapping_sell.publish_id = aa.publish_id    
    ) WHEN MATCHED THEN UPDATE 
SET
  tag_mapping_sell.tag_level = aa.tag_level WHEN NOT MATCHED THEN 
INSERT ( 
  tag_id
  , tag_level
  , publish_id
  , meshcode1
  , meshcode2
  , meshcode3
  , meshcode6
) 
VALUES ( 
  aa.tag_id
  , aa.tag_level
  , aa.publish_id
  , aa.meshcode1
  , aa.meshcode2
  , aa.meshcode3
  , aa.meshcode6
)";

//$stmt_1016 = $pdo->prepare($sql_1016);
//echo "{$sql}\n";
$log->freeform("null_update", $sql_1016);


$sql_1015 = "MERGE 
INTO tag_mapping_sell 
  USING ( 
    SELECT
       1015 tag_id
      , case when ( 
        publish_sell.price * mst_currency.exchange_rate
      ) is null then 0
      else  ( 
        publish_sell.price * mst_currency.exchange_rate
      ) end tag_level
      , publish_sell.publish_id
      , publish_sell.meshcode1
      , publish_sell.meshcode2
      , publish_sell.meshcode3
      , publish_sell.meshcode6 
    FROM
      publish_sell  
      LEFT JOIN tag_mapping_sell 
        ON (tag_mapping_sell.publish_id = publish_sell.publish_id and tag_id = 1015)
      LEFT JOIN mst_currency 
        ON publish_sell.currency_cd = mst_currency.currency_cd 
    WHERE
      (tag_mapping_sell.tag_level IS NULL or tag_mapping_sell.tag_level = 0)
      and rownum < 10000
      AND publish_sell.currency_cd != 'USD'
      and publish_sell.price <> 0
      and publish_sell.price is not null
      and publish_sell.price < 999999999
  ) aa 
    ON ( 
      tag_mapping_sell.tag_id = aa.tag_id 
      AND tag_mapping_sell.publish_id = aa.publish_id
     -- AND tag_mapping_sell.publish_id = aa.publish_id    
    ) WHEN MATCHED THEN UPDATE 
SET
  tag_mapping_sell.tag_level = aa.tag_level WHEN NOT MATCHED THEN 
INSERT ( 
  tag_id
  , tag_level
  , publish_id
  , meshcode1
  , meshcode2
  , meshcode3
  , meshcode6
) 
VALUES ( 
  aa.tag_id
  , aa.tag_level
  , aa.publish_id
  , aa.meshcode1
  , aa.meshcode2
  , aa.meshcode3
  , aa.meshcode6
)";

//$stmt_1015 = $pdo->prepare($sql_1015);
//echo "{$sql}\n";
$log->freeform("null_update", $sql_1015);

# 設定された回数ずつ処理を実行する

$loop_cnt = 100;
for ($i=1; $i<=$loop_cnt; $i++) {

	//echo "/" . $i; //これは進行をチェックするログ（画面に出力）
	$Msg = "{$i}";
	//$log->freeform("null_update", $Msg);

    try {
        $time_start = microtime(true);
//        $stmt_1016->execute();
		$stmt_1016 = $pdo->query($sql_1016);
        $time = microtime(true) - $time_start;
        $Msg = $Msg.",".sprintf("%.20f", $time);
        //echo $Msg;
        $log->freeform("null_update_time", $Msg);

    } catch (PDOException $e) {
		$msg = "MERGE(1016):".$e->getMessage();
        $log->freeform("null_update", $msg);
        sendErrMail($msg);
//        drop_table ($table_pref);
        //echo $e->getMessage();
//        exit;
    }

	try {
		$time_start = microtime(true);
//		$stmt_1015->execute();
		$stmt_1015 = $pdo->query($sql_1015);

		$time = microtime(true) - $time_start;
		$Msg = $Msg.",".sprintf("%.20f", $time);
		//echo $Msg;
		$log->freeform("null_update_time", $Msg);

	} catch (PDOException $e) {
		$msg = "MERGE(1015):".$e->getMessage();
		$log->freeform("null_update", $msg);
		sendErrMail($msg);
		$Msg = $Msg.",".sprintf("ERROR");
		$log->freeform("null_update_time", $Msg);
//		drop_table ($table_pref);
		//echo $e->getMessage();
//		exit;
	}

}

/**
 * テンポラリテーブル削除処理：　バッチ処理用に作成したテンポラリテーブルを削除する。
 *
 * 　・更新処理終了時
 * 　・テンポラリテーブル作成後のエラー発生時
 * @param $table_pref
 */
/*
function drop_table ($table_pref) {

	# グローバル変数を参照
	global $pdo;
	global $log;

	# バッチ用に作成したテンポラリーテーブルをデータを削除後、ドロップする
	$sql1 = "TRUNCATE TABLE TMP_TABLE_".$table_pref."_NULL";
	$log->freeform("null_update", $sql1);

	$sql2 = "DROP TABLE TMP_TABLE_".$table_pref."_NULL";
	$log->freeform("null_update", $sql2);

	try {
		$stmt = $pdo->query($sql1);
		$stmt = $pdo->query($sql2);
	} catch (PDOException $e) {
		$msg = "{$table_pref}:drop table:".$e->getMessage();
		$log->freeform("null_update", $msg);
		sendErrMail($msg);
		//echo $e->getMessage();
		exit;
	}
	return;
}
*/

/**
 * Errorメール送信処理：　例外発生時に、管理者へエラーメールを送信する。
 *
 */
function sendErrMail($body)
{

	mb_language("Japanese");
	mb_internal_encoding("UTF-8");


	$to = TO_MAIL_ADDR;     //エラーメールの送付先
	$header = "From: $to\nReply-To: $to\n";
	$title = '【ERROR】USD価格、NULLデータ再更新変更バッチ';

	try {
		mb_send_mail($to, $title, $body, $header);
		//echo "処理エラーメールを送信しました";
		return true;
	} catch (Exception $e) {
		//echo "エラーメールの送信に失敗しました";
		return false;
	}
}

# トータルの処理時間を取得
$time_total = microtime(true) - $time_start_batch;

#log
$log->freeform("null_update", "total time:{$time_total} end");


?>