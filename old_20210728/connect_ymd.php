<?php

/** 実行ファイルから見たパスで指定 */
require '/var/www/import/config.php';
require '/var/www/import/db_oracle.php';
require '/var/www/import/log.php';
require '/var/www/import/tools.php';

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);
//$pdo_high = DB::getPdo(DB_ORA_TNS_HIGH);

$log = new log();

//site_no:1=>991705行
//$target_num = 991705;  //ループの最大回数
$target_num = 1000000;  //ループの最大回数

/*
 *
 * CURLの大文字小文字／月マタギで掲載の連続が切れてしまったレコードをつなげる
 * (1)cleansingテーブルからcrawling_listの親を決める(同一URLでstartの一番若いもの)
 * (2)掲載が連続する子を見つけて(複数)正しいfrom～toを見つける
 * (3)親のyyyymmddを子の値に伸ばす
 * (4)子のcleansingデータを消す(Deleteは危険なのでとりあえずフラグで)：publish_id:2にしてlocal_int2にコピーしておく
 * (5)子のCURL＝親のcrawl_idに変更、status=291
 * (6)親のpublishの件数と掲載期間を再カウント
 * (7)子のpublishを再カウント。同一ゼロ件だったら削除
 *
*/

$log->freeform('connect_ymd', 'START');
$dt = new DateTime();
$dt_old = new DateTime();

##※無限ループで1レコードずつ回す。
$sql1 = "SELECT crawl_id,max(changed_num) AS changed_num,max(publish_id) AS publish_id FROM TMP_CLEANSING_SELL_ID_3 WHERE crawl_id = (SELECT min(crawl_id) FROM TMP_CLEANSING_SELL_ID_3) GROUP BY crawl_id";
$sql2 = "SELECT crawl_id,TO_CHAR(created,'YYYYMMDD') AS created,yyyymmdd,lower_curl FROM tmp_lower_curl WHERE lower_curl= (SELECT LOWER(curl) FROM crawling_list WHERE crawl_id=:crawl_id) ORDER BY created";
$stmt2 = $pdo_tp->prepare($sql2);
$sql3 = "INSERT INTO tmp_closetarget_sell_3 (publish_id) VALUES (:publish_id)";  //unique制約あり
$stmt3 = $pdo_tp->prepare($sql3);

for($i=0; $i<$target_num; $i++){
	$dt_loop_in = new DateTime();
	#対象クロールID抽出
	$stmt1 = $pdo_tp->query($sql1);

	$row1 = $stmt1->fetch();
	if($row1 == null){
		#対象がなくなったらバッチ終了
		exit;
	}
	#取得したcrawl_idの件数分終了処理をする。
	$crawl_id = (int)$row1['crawl_id'];
	$publish_id = (int)$row1['publish_id'];
//	$crawl_id = 134783022;
//	$publish_id = 631113870;
$log->freeform('connect_ymd', 'crawl_id:'.$crawl_id .'/publish_id:'.$publish_id);

	#マージ対象抽出
	$stmt2->bindParam(':crawl_id', $crawl_id, PDO::PARAM_INT);
	$stmt2->execute();
	$rows2 = $stmt2->fetchAll();
	
	$cnt=0;
	$base_crawl_id = 0;
	$base_created = '';
	$base_yyyymmdd = 0;
	$target_lower_curl = '';
	$del_crawl_ids = array();
	$update_publish_ids = array();
	foreach ($rows2 as $row2) {
		$cnt++;
//echo $row2['crawl_id'];
		if($cnt==1){  //1件目は親
			$base_crawl_id = $row2['crawl_id'];
			$base_created = $row2['created'];
			$base_yyyymmdd = $row2['yyyymmdd'];
			$target_lower_curl = $row2['lower_curl'];
		}else{
			$target_crawl_id = $row2['crawl_id'];
			$target_created = $row2['created'];
			$target_yyyymmdd = $row2['yyyymmdd'];
$log->freeform('connect_ymd', 'str:'.$base_yyyymmdd.' | '.$target_created);
			#同じCURLでyyyymmddとcreatedの連続性比較、3日以内ならつなげる
//			$format = 'Y-m-d';
			$format = 'Ymd';
			$target_created_dt=DateTime::createFromFormat($format, $target_created);
			$base_yyyymmdd_dt=DateTime::createFromFormat($format, $base_yyyymmdd);
$log->freeform('connect_ymd', '比較:base='.$base_yyyymmdd_dt->format('Y-m-d H:i:s').' | target='.$target_created_dt->format('Y-m-d H:i:s'));
			$interval = $base_yyyymmdd_dt->diff($target_created_dt);
$log->freeform('connect_ymd', 'interval:('.$interval->invert.')'.$interval->format('%a'));
			if(($interval->invert == 1) || ($interval->invert == 0 && $interval->format('%a') <= 3)){
				array_push($del_crawl_ids, $target_crawl_id);
				if($target_yyyymmdd > $base_yyyymmdd){
					$base_yyyymmdd = $target_yyyymmdd;
				}
			}
		}
	}
$log->freeform('connect_ymd', 'cnt:'.$cnt.'|del:'.count($del_crawl_ids));
	if(count($del_crawl_ids)>=1){ //重複あったら各アップデート処理
		array_push($update_publish_ids, $publish_id); //親のpublish_id
		$del_crawl_ids_csv='';
		for($j=0;$j<count($del_crawl_ids);$j++){
			if($j>0){
				$del_crawl_ids_csv .= ',';
			}
			$del_crawl_ids_csv .= $del_crawl_ids[$j];
		}
		$sql_sel_tmp_cleansing = "SELECT publish_id FROM TMP_CLEANSING_SELL_ID_3 WHERE crawl_id IN(" . $del_crawl_ids_csv . ")";
$log->freeform('connect_ymd', $sql_sel_tmp_cleansing);
		$stmt = $pdo_tp->query($sql_sel_tmp_cleansing);
		$stmt->execute();
		$rows = $stmt->fetchAll();
		foreach ($rows as $row) {
			array_push($update_publish_ids, $row['publish_id']);
		}
		$sql_del_crawling_list = "UPDATE crawling_list SET curl='" . $base_crawl_id . "', status=291 WHERE crawl_id IN(" . $del_crawl_ids_csv . ")";
		$sql_del_tmp_cleansing = "DELETE FROM tmp_cleansing_sell_id_3 WHERE crawl_id IN(" . $del_crawl_ids_csv . ")";
		$sql_del_cleansing = "UPDATE cleansing_sell SET publish_id=2, local_int2=publish_id WHERE crawl_id IN(" . $del_crawl_ids_csv . ")";
		$sql_del_lower_curl = "DELETE FROM tmp_lower_curl WHERE lower_curl='" . $target_lower_curl . "' AND crawl_id IN(" . $del_crawl_ids_csv . ")";
$log->freeform('connect_ymd', $sql_del_crawling_list);
$log->freeform('connect_ymd', $sql_del_tmp_cleansing);
$log->freeform('connect_ymd', $sql_del_cleansing);
$log->freeform('connect_ymd', $sql_del_lower_curl);
		$stmt = $pdo_tp->query($sql_del_crawling_list);
		$stmt = $pdo_tp->query($sql_del_tmp_cleansing);
		$stmt = $pdo_tp->query($sql_del_cleansing);
		$stmt = $pdo_tp->query($sql_del_lower_curl);

		$sql_update_base = "UPDATE crawling_list SET yyyymmdd=" . $base_yyyymmdd .",lastpublished=TO_DATE('" . $base_yyyymmdd . " 00:00:00', 'YYYYMMDD HH24:MI:SS') WHERE crawl_id=" . $base_crawl_id;
$log->freeform('connect_ymd', $sql_update_base);
		$stmt = $pdo_tp->query($sql_update_base);

		for($j=0;$j<count($update_publish_ids);$j++){
			$stmt3->bindParam(':publish_id', $update_publish_ids[$j], PDO::PARAM_INT);
			try {
$log->freeform('connect_ymd', 'sql3-exec->'.$update_publish_ids[$j]);
				$stmt3->execute();
			} catch (PDOException $e) {
				//重複はエラー、無視する
			}
		}
	}
	//del対象なしの場合処理漏れなのでtmpから削除
	$sql_del_tmp_cleansing = "DELETE FROM tmp_cleansing_sell_id_3 WHERE crawl_id =" . $crawl_id;
$log->freeform('connect_ymd', 'del(base)->'.$crawl_id);
	$stmt = $pdo_tp->query($sql_del_tmp_cleansing);
	$dt_loop_end = new DateTimeImmutable();
	$elapsed = $dt_loop_end->diff($dt_loop_in)->format("%H:%I:%S.%F");
	$log->freeform('connect_ymd', 'ループ終了(' . $i .') -> ' . $elapsed);
	if($i % 1000 == 0){
		$dt_now = new DateTime();
echo ' '.$dt_now->format('H:i:s').'/'.$dt_old->format('H:i:s') .' ';
		$elapsed = $dt_now->diff($dt_old)->format("%H:%I:%S.%F");
		echo $i . '->'. $elapsed . ' ';
		$dt_old = new DateTime();
	}
}
$dt2 = new DateTime();
$elapsed = $dt2->diff($dt)->format("%H:%I:%S.%F");
$log->freeform("connect_ymd", 'FINISH -> ' . $elapsed);


?>
