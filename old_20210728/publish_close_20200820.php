
<?php

/** 実行ファイルから見たパスで指定 */
require '/var/www/import/config.php';
require '/var/www/import/db_oracle.php';
require '/var/www/import/log.php';
require '/var/www/import/tools.php';

$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
$pdo_medium = DB::getPdo(DB_ORA_TNS_MEDIUM);
//$pdo_high = DB::getPdo(DB_ORA_TNS_HIGH);

$log = new log();

//$target_num = 10;
//$target_max = 10;
$target_num = 1000;  //この件数ずつcrawling_listから抽出して処理
$target_max = 10000000;  //1回のバッチで対象とする最大数
//$target_ymd=20200501;

/*
 *
 * 掲載終了処理Dailyバッチ
 *  publishに掲載終了セット
 *  同一物件crawling_listに掲載終了セット
 *  マップ／マップ用タグマッピング(未実装)から掲載終了の物件情報を削除、
 *  ※DailyバッチではCRAWLING_LIST_I03索引を使ってyyyymmddで対象を照会。再クレンジング等で過去分の処理多数の場合はCRAWLING_LIST_I01索引でstatus=100の過去yyyymmddを抽出する。
 *
 * ■16:00にスケジュール実行する。＜現行未設定＞
 * 0 16 * * * opc /usr/bin/php /var/www/batch/publish_close.php
 *
 * 【掲載終了日の設定ルール】
 * ①同一物件が複数の場合に、ある物件の2件目の掲載終了に対して、
 * 　最初に掲載終了した日を掲載終了日とする
 * ②同一物件が存在しない場合は、最初に終了した日とする
 *
 * 【掲載終了の処理】
 * (1)当日のバッチのターゲットとなるcrawl_idを抽出
 *    クロール～クレンジングが正常完了している、完了日付の更新(yyyymmdd)が指定日数止まっている、
 *    かつclosed設定されていないクロールID (status=100 かつ yyyymmddが指定日付)
 * (2)対応するpublishデータの同一物件と掲載終了数をチェック
 *   (2-1)同一物件が1件(自分だけ)の場合、掲載終了処理
 *     掲載物件をクローズ処理
 *     当URLをクローズ処理
 *   (2-2)同一物件が2件以上の場合
 *     (2-2-1)既に掲載物件が掲載終了している場合、当URLはクローズ処理 ※再クレンジングなどで発生
 *       当URLをクローズ処理(min(終了日)とyyyymmddが等しいURLはstatus:200、それ以外は202
 *     (2-2-2)掲載物件が掲載中の場合
 *       (2-2-2-1)既に掲載終了しているURLがある場合、掲載終了処理
 *         掲載物件をクローズ処理
 *         同一物件URLをクローズ処理(min(終了日)とyyyymmddが等しいURLはstatus:200、それ以外は202)
 *       (2-2-2-2)同一物件内で初めての掲載終了の場合、掲載物件は掲載中のまま、URLをクローズ処理(status:200)
 *         当URLをクローズ処理(min(終了日)とyyyymmddが等しいURLはstatus:200、それ以外は202)
 * 
 * 【物件掲載終了の処理】
 * <publish_rent/sell>に以下の値をセットする
 * ・PUBLISH_DAYS に値を入れる（start_datetimeからyyyymmddの日付の差分）
 * 　　⇒ start_datetimeに値がないものをどうするか
 * 　　　　　⇒　cleansing_rent/sellのreg_datetimeとか？
 * ・close_datetime(最終掲載確認日時) => yyyymmdd
 * ・complete_datetime-> sysdate
 * ・status_flg(掲載ステータスフラグ)：１
 * ・close_num（掲載終了数）
 * ・termination_flg(掲載終了理由)
 *
 * <map_rent/sell>の以下のレコードの削除
 * ・掲載終了対象の publilsh_id が一致
 *
 * <tag_mapping_map_rent/map_sell> ※map用tag_mappingテーブル作成＆運用後
 * ・掲載終了対象の publilsh_id が一致
 * 
 * <crawling_list> crawl_idが一致するレコードに以下の値をセット
 * ・status => 200
 * 　　⇒同一物件判定で掲載終了したものは、202をセット
 * ・closed => yyyymmdd の値
 * ・updated(更新日時) => SYSDATE + 9/24
 *
 * <scrape>
 * ・　現行そのまま
 *
 *【関連テーブル】
 * crawling_list
 * scrape
 * cleansing_xxxx
 * publish_xxxx
 * map_xxxx
 * tag_mapping_xxxx
 * tag_mapping_map_xxxx ※未作成
 *
 *
*/

$dt = new DateTimeImmutable();  //modify()をしても元の$dtの値は不変
// timezoneをQC用(Asia/Tokyo)に合わせて3日前ymd取得
$target_ymd = $dt->setTimezone(new DateTimeZone('Asia/Tokyo'))->modify('- ' . CLOSE_JUDGMENT_DAYS . ' days')->format('Ymd');
//echo "3日前の日付：{$target_ymd}\n";

$target_ymd='20200820';

$log->freeform('publish_close_' . $target_ymd, '(バッチスタート) 対象yyyymmdd : ' . $target_ymd);

/*
$dt2 = new DateTime();
$elapsed = $dt2->diff($dt)->format("%H:%I:%S.%F");
$log->freeform("publish_close", 'テンポラリーテーブルINSERT -> ' . $elapsed);
*/

#########################
##※無限ループで回す。指定件数超の場合終了。
## (1)当日のバッチのターゲットとなるcrawl_idを抽出
##   クロール～クレンジングが正常完了している、完了日付の更新(yyyymmdd)が指定日数止まっている、
##   かつclosed設定されていないクロールID (yyyymmddが指定日付でstatus=100 or 198のもの)
#########################
$sql = "SELECT /*+INDEX(a CRAWLING_LIST_I03)*/ a.crawl_id, a.recrawl_count, a.status" . 
	" FROM crawling_list a " . 
	" WHERE a.yyyymmdd = " . $target_ymd . " AND status in (100, 198) AND site_no in (1,3) AND rownum <= " . $target_num;

$log->freeform('publish_close_' . $target_ymd, $sql);
$sql_get_publish_id_rent = "SELECT country_cd,publish_id FROM cleansing_rent WHERE crawl_id=:crawl_id AND changed_num=:changed_num";
$stmt_get_publish_id_rent = $pdo_tp->prepare($sql_get_publish_id_rent);
$sql_get_publish_id_sell = "SELECT country_cd,publish_id FROM cleansing_sell WHERE crawl_id=:crawl_id AND changed_num=:changed_num";
$stmt_get_publish_id_sell = $pdo_tp->prepare($sql_get_publish_id_sell);
$sql_update_publish_count_rent = "UPDATE publish_rent SET duplicate_num = :duplicate_num, close_num = :close_num WHERE publish_id=:publish_id";
$stmt_update_publish_count_rent = $pdo_tp->prepare($sql_update_publish_count_rent);
$sql_update_publish_count_sell = "UPDATE publish_sell SET duplicate_num = :duplicate_num, close_num = :close_num WHERE publish_id=:publish_id";
$stmt_update_publish_count_sell = $pdo_tp->prepare($sql_update_publish_count_sell);
$sql_get_publish_closed_rent = "SELECT status_flg,TO_CHAR(start_datetime," . DB_DATE_FORMAT . ") AS start_datetime FROM publish_rent WHERE publish_id=:publish_id";
$stmt_get_publish_closed_rent = $pdo_tp->prepare($sql_get_publish_closed_rent);
$sql_get_publish_closed_sell = "SELECT status_flg,TO_CHAR(start_datetime," . DB_DATE_FORMAT . ") AS start_datetime FROM publish_sell WHERE publish_id=:publish_id";
$stmt_get_publish_closed_sell = $pdo_tp->prepare($sql_get_publish_closed_sell);

$loop_num = 0;
$loop_max = $target_max / $target_num;
while($loop_num < $loop_max){
	$loop_num ++;
	$dt_loop_1 = new DateTimeImmutable();
	$processed_ids = array(); //当ループ内で同一物件として処理済みのcrawl_id(ループ外の同一物件はこのループでstatus変更されて以降のループで抽出されない)
	try {
		$stmt = $pdo_tp->query($sql);
	} catch (PDOException $e) {
		$log->freeform('publish_close_' . $target_ymd, 'crawling_list対象抽出エラー' . $e->getMessage());
		exit;
	}
	$dt_loop_2 = new DateTimeImmutable();
	$rows = $stmt->fetchAll();
	if($rows == null){
		#対象がなくなったらバッチ終了
		exit;
	}
	#取得したcrawl_idの件数分終了処理をする。
	foreach ($rows as $row) {
		$target_crawl_id = (int)$row['crawl_id'];
		$target_changed_num = (int)$row['recrawl_count'];
		$target_status = (int)$row['status'];
		$target_publish_id_rent = 0;
		$target_publish_id_sell = 0;
		#処理済みチェック
		if(in_array($target_crawl_id, $processed_ids)){
$log->freeform('publish_close_' . $target_ymd, '処理済み：$target_crawl_id->' . $target_crawl_id);
			#処理済みのため何もしない
		}else{
$log->freeform('publish_close_' . $target_ymd, '処理開始：$target_crawl_id->' . $target_crawl_id);
			$target_rent_data = getPublishId('rent', $target_crawl_id, $target_changed_num);
			$target_publish_id_rent = $target_rent_data[0];
			$target_country_cd_rent = $target_rent_data[1];
			$target_sell_data = getPublishId('sell', $target_crawl_id, $target_changed_num);
			$target_publish_id_sell = $target_sell_data[0];
			$target_country_cd_sell = $target_sell_data[1];
			#rent/sellそれぞれ存在チェック＆同一抽出
			//rent/sellに両方存在していても(再クレンジング等で発生する可能性あり)異常データにはせずそのまま両方処理する
			#cleansing_rentに存在チェック＆同一抽出
//			if($target_publish_id_rent > 1){
$log->freeform('publish_close_' . $target_ymd, 'target_publish_id_rent：'.$target_publish_id_rent.'/'.'target_publish_id_sell：'.$target_publish_id_sell);
			if($target_publish_id_rent > 50000000){
				$close_num = 0;
				$crawl_ids_ymd = array();  //min(yyyymmdd)と同一
				$crawl_ids_already = array();  //yyyymmddより大きく既に終了(<=$target_ymd)
				$crawl_ids_notyet = array();  //yyyymmddより大きい(まだ終了対象ではない)
				$same_crawl_ids = getSameCrawlIds('rent', $target_publish_id_rent, $target_country_cd_rent); //[crawl_id, yyyymmdd, created]の配列(recrawl_count=changed_numとなっているもののみ。yyyymmdd昇順が保証されている)
				$close_yyyymmdd = $same_crawl_ids[0][1]; //$target_crawl_idではなく同一物件の最小yyyymmddを格納
				$duplicate_num = count($same_crawl_ids);
				$publish_closed = getPublishClosed('rent', $target_publish_id_rent);
				$flg_closed = $publish_closed[0];
				$min_start_datetime = $publish_closed[1];
				$bool_min_start_datetime_changed = false;
				#同一対象のyyyymmddにより分類
				for($i=0; $i<$duplicate_num; $i++){
					$tmp_crawl_id = $same_crawl_ids[$i][0];
					$tmp_ymd = $same_crawl_ids[$i][1];
					$tmp_created = $same_crawl_ids[$i][2];
					if($tmp_created < $min_start_datetime){
						$min_start_datetime = $tmp_created;
						$bool_min_start_datetime_changed = true;
					}
//$log->freeform('publish_close_' . $target_ymd, 'tmp_crawl_id：'.$tmp_crawl_id.'/'.'tmp_ymd：'.$tmp_ymd.'/target_ymd:'.$target_ymd.'/close_yyyymmdd:'.$close_yyyymmdd);
					if($tmp_ymd > $target_ymd){
//$log->freeform('publish_close_' . $target_ymd, '$crawl_ids_notyet->push');
						array_push($crawl_ids_notyet, $tmp_crawl_id);
					}else if($tmp_ymd == $close_yyyymmdd){
//$log->freeform('publish_close_' . $target_ymd, '$crawl_ids_ymd->push');
						array_push($crawl_ids_ymd, $tmp_crawl_id);
					}else{
//$log->freeform('publish_close_' . $target_ymd, '$crawl_ids_already->push');
						array_push($crawl_ids_already, $tmp_crawl_id);
					}
				}
				#createdの更新が必要な場合先に更新しておく
				if($bool_min_start_datetime_changed){
					updatePublishStart('rent', $target_publish_id_rent, $min_start_datetime);
				}
				$close_num = count($crawl_ids_ymd) + count($crawl_ids_already);
				#同一数により各ステータス変更＆掲載終了判定を実施
//$log->freeform('publish_close_' . $target_ymd, 'count($same_crawl_ids):'.count($same_crawl_ids));
				if(count($same_crawl_ids) == 1){
					#同一なしの場合は掲載終了処理
					$termination_flg=2;  //2:1/1で掲載終了確認
					closePublish('rent', $target_publish_id_rent, $duplicate_num, $close_num, $termination_flg, $close_yyyymmdd);
					#crawlステータス更新
					if(count($crawl_ids_ymd) >= 1){
						updateCrawlStatus($crawl_ids_ymd, 200);
					}
				}else if(count($same_crawl_ids) >= 2){
					if($flg_closed){
						#既に終了している場合(カウントのみ更新)
						updatePublishCount('rent', $target_publish_id_rent, $duplicate_num, $close_num); //終了には関与せずカウントのみupdate
						#crawlステータス更新
						if(count($crawl_ids_notyet) >= 1){
							updateCrawlStatus($crawl_ids_notyet, 198);
						}
						if(count($crawl_ids_ymd) >= 1){
							updateCrawlStatus($crawl_ids_ymd, 200);
						}
						if(count($crawl_ids_already) >= 1){
							updateCrawlStatus($crawl_ids_already, 201);
						}
					}else{
						#終了判定
						if($close_num == 1){
							#自分だけ終了の場合publishはカウントのみアップデート、終了済みURLは掲載終了待ち199
							updatePublishCount('rent', $target_publish_id_rent, $duplicate_num, $close_num); //終了には関与せずカウントのみupdate
							#crawlステータス更新
							if(count($crawl_ids_ymd) >= 1){
								updateCrawlStatus($crawl_ids_ymd, 199);
							}
							if(count($crawl_ids_already) >= 1){  //現仕様では当条件には$crawl_ids_ymdに1件のみしか入らないはずだけど掲載終了済みのステータス処理として記載しておく
								updateCrawlStatus($crawl_ids_already, 199);
							}
						}else if($close_num >=2){
							#同一2件以上の場合は掲載終了2件以上で掲載終了処理
							$termination_flg=3;  //3:一定の割合で掲載終了確認
							closePublish('rent', $target_publish_id_rent, $duplicate_num, $close_num, $termination_flg, $close_yyyymmdd);
							#crawlステータス更新
							if(count($crawl_ids_notyet) >= 1){
								updateCrawlStatus($crawl_ids_notyet, 198);
							}
							if(count($crawl_ids_ymd) >= 1){
								updateCrawlStatus($crawl_ids_ymd, 200);
							}
							if(count($crawl_ids_already) >= 1){
								updateCrawlStatus($crawl_ids_already, 201);
							}
						}
					}
				}
			}
//			if($target_publish_id_sell > 1){
			if($target_publish_id_sell > 50000000){
				$close_num = 0;
				$crawl_ids_ymd = array();  //min(yyyymmdd)と同一
				$crawl_ids_already = array();  //yyyymmddより大きく既に終了(<=$target_ymd)
				$crawl_ids_notyet = array();  //yyyymmddより大きい(まだ終了対象ではない)
				$same_crawl_ids = getSameCrawlIds('sell', $target_publish_id_sell, $target_country_cd_sell); //[crawl_id, yyyymmdd]の配列(recrawl_count=changed_numとなっているもののみ。yyyymmdd昇順が保証されている)
				$close_yyyymmdd = $same_crawl_ids[0][1]; //$target_crawl_idではなく同一物件の最小yyyymmddを格納
				$duplicate_num = count($same_crawl_ids);
				$publish_closed = getPublishClosed('rent', $target_publish_id_rent);
				$flg_closed = $publish_closed[0];
				$min_start_datetime = $publish_closed[1];
				$bool_min_start_datetime_changed = false;
				#同一対象のyyyymmddにより分類
				for($i=0; $i<$duplicate_num; $i++){
					$tmp_crawl_id = $same_crawl_ids[$i][0];
					$tmp_ymd = $same_crawl_ids[$i][1];
					$tmp_created = $same_crawl_ids[$i][2];
					if($tmp_created < $min_start_datetime){
						$min_start_datetime = $tmp_created;
						$bool_min_start_datetime_changed = true;
					}
					if($tmp_ymd > $target_ymd){
						array_push($crawl_ids_notyet, $tmp_crawl_id);
					}else if($tmp_ymd == $close_yyyymmdd){
						array_push($crawl_ids_ymd, $tmp_crawl_id);
					}else{
						array_push($crawl_ids_already, $tmp_crawl_id);
					}
				}
				#createdの更新が必要な場合先に更新しておく
				if($bool_min_start_datetime_changed){
					updatePublishStart('sell', $target_publish_id_sell, $min_start_datetime);
				}
				$close_num = count($crawl_ids_ymd) + count($crawl_ids_already);
				#同一数により各ステータス変更＆掲載終了判定を実施
				if(count($same_crawl_ids) == 1){
					#同一なしの場合は掲載終了処理
					$termination_flg=2;  //2:1/1で掲載終了確認
					closePublish('sell', $target_publish_id_sell, $duplicate_num, $close_num, $termination_flg, $close_yyyymmdd);
					#crawlステータス更新
					if(count($crawl_ids_ymd) >= 1){
						updateCrawlStatus($crawl_ids_ymd, 200);
					}
				}else if(count($same_crawl_ids) >= 2){
					if($flg_closed){
						#既に終了している場合(カウントのみ更新)
						updatePublishCount('sell', $target_publish_id_rent, $duplicate_num, $close_num); //終了には関与せずカウントのみupdate
						#crawlステータス更新
						if(count($crawl_ids_notyet) >= 1){
							updateCrawlStatus($crawl_ids_notyet, 198);
						}
						if(count($crawl_ids_ymd) >= 1){
							updateCrawlStatus($crawl_ids_ymd, 200);
						}
						if(count($crawl_ids_already) >= 1){
							updateCrawlStatus($crawl_ids_already, 201);
						}
					}else{
						#終了判定
						if($close_num == 1){
							#自分だけ終了の場合publishはカウントのみアップデート、終了済みURLは掲載終了待ち199
							updatePublishCount('sell', $target_publish_id_sell, $duplicate_num, $close_num); //終了には関与せずカウントのみupdate
							#crawlステータス更新
							if(count($crawl_ids_ymd) >= 1){
								updateCrawlStatus($crawl_ids_ymd, 199);
							}
							if(count($crawl_ids_already) >= 1){  //現仕様では当条件には$crawl_ids_ymdに1件のみしか入らないはずだけど掲載終了済みのステータス処理として記載しておく
								updateCrawlStatus($crawl_ids_already, 199);
							}
						}else if($close_num >=2){
							#同一2件以上の場合は掲載終了2件以上で掲載終了処理
							$termination_flg=3;  //3:一定の割合で掲載終了確認
							closePublish('sell', $target_publish_id_sell, $duplicate_num, $close_num, $termination_flg, $close_yyyymmdd);
							#crawlステータス更新
							if(count($crawl_ids_notyet) >= 1){
								updateCrawlStatus($crawl_ids_notyet, 198);
							}
							if(count($crawl_ids_ymd) >= 1){
								updateCrawlStatus($crawl_ids_ymd, 200);
							}
							if(count($crawl_ids_already) >= 1){
								updateCrawlStatus($crawl_ids_already, 201);
							}
						}
					}
				}
			}
			#処理済みに追加
			array_push($processed_ids, $target_crawl_id);
		}
	}
	$dt_loop_end = new DateTimeImmutable();
	$elapsed = $dt_loop_end->diff($dt_loop_1)->format("%H:%I:%S.%F");
	$log->freeform('publish_close_' . $target_ymd, 'ループ終了(' . $loop_num .') -> ' . $elapsed);
echo $loop_num . '->'. $elapsed . ' ';
}
$dt2 = new DateTime();
$elapsed = $dt2->diff($dt)->format("%H:%I:%S.%F");
$log->freeform("publish_close", 'バッチ終了 -> ' . $elapsed);


/**
 * crawl_idに対応するpublish_idを取得する
 * cleansing_XXXXテーブルにpublish_idのみのINDEXがないためcountry_cdも合わせて返す
 *
 * @param string $listing_type 'sell'または'rent'
 * @param int $crawl_id
 * @param int $recrawl_count
 * @return array{int $country_cd, int $publish_id}
 */
function getPublishId($listing_type, $crawl_id, $recrawl_count){

	# グローバル変数を参照
	global $stmt_get_publish_id_rent;
	global $stmt_get_publish_id_sell;
	global $log;
	global $target_ymd;

	$publish_id = 0;
	$country_cd = 0;

	if($listing_type == 'rent'){
        $stmt_get_publish_id_rent->bindParam(':crawl_id', $crawl_id, PDO::PARAM_INT);
        $stmt_get_publish_id_rent->bindParam(':changed_num', $recrawl_count, PDO::PARAM_INT);
		try {
			$stmt_get_publish_id_rent->execute();
		} catch (PDOException $e) {
			$log->freeform('publish_close_' . $target_ymd, 'getPublishIdエラー(rent)->param:' . $crawl_id . '/' . $recrawl_count . '|' . $e->getMessage());
		}
        $row = $stmt_get_publish_id_rent->fetch();
		if($row != null){
			$country_cd = $row['country_cd'];
			$publish_id = $row['publish_id'];
		}
	}else if($listing_type == 'sell'){
        $stmt_get_publish_id_sell->bindParam(':crawl_id', $crawl_id, PDO::PARAM_INT);
        $stmt_get_publish_id_sell->bindParam(':changed_num', $recrawl_count, PDO::PARAM_INT);
		try {
			$stmt_get_publish_id_sell->execute();
		} catch (PDOException $e) {
			$log->freeform('publish_close_' . $target_ymd, 'getPublishIdエラー(sell)->param:' . $crawl_id . '/' . $recrawl_count . '|' . $e->getMessage());
		}
        $row = $stmt_get_publish_id_sell->fetch();
		if($row != null){
			$publish_id = $row['publish_id'];
			$country_cd = $row['country_cd'];
		}
	}
	$return_data=array($publish_id, $country_cd);
	return $return_data;
}

/**
 * publishに対する同一のcrawl_idを返す
 * crawl_idはchanged_num=recrawl_countになっているもののみを抽出する。
 *
 * @param string $listing_type 'sell'または'rent'
 * @param int $publish_id
 * @return array{int $crawl_id, int $yyyymmdd, str $created}[]
 */
function getSameCrawlIds($listing_type, $publish_id, $country_cd){

	# グローバル変数を参照
	global $pdo_tp;
	global $log;

	$crawl_ids = array();
	$sql = "SELECT /*+INDEX (cl CLEANSING_" . strtoupper($listing_type) . "_CCD_AND_PUBID) INDEX (c CRAWLING_LIST_PK) */ c.crawl_id,c.yyyymmdd,TO_CHAR(c.created," . DB_DATE_FORMAT . ") AS created FROM cleansing_" . $listing_type . " cl INNER JOIN crawling_list c ON cl.crawl_id=c.crawl_id AND cl.changed_num=c.recrawl_count WHERE cl.publish_id=" . $publish_id . " AND cl.country_cd=" . $country_cd . " ORDER BY c.yyyymmdd ASC";
	$stmt = null;
	try {
		$stmt = $pdo_tp->query($sql);
	} catch (PDOException $e) {
		$log->freeform('publish_close_' . $target_ymd, 'getSameCrawlIdsエラー' . $e->getMessage());
		exit;
	}
	$rows = $stmt->fetchAll();
	#取得したcrawl_idの件数分終了処理をする。
	foreach ($rows as $row) {
		$crawl_id_ymd = array((int)$row['crawl_id'], (int)$row['yyyymmdd'], $row['created']);
		array_push($crawl_ids, $crawl_id_ymd);
	}
	return $crawl_ids;

}

/**
 * crawling_listのstatusを更新する
 *
 * @param array int[] $crawl_ids
 * @param int $status
 * @return void
 */
function updateCrawlStatus($crawl_ids, $status){

	# グローバル変数を参照
	global $pdo_tp;
	global $log;
	global $target_ymd;

	#一括更新用SQL作成
	$in_clause = '';
	for($i=0; $i<count($crawl_ids); $i++){
		if($in_clause != ''){
			$in_clause .= ',';
		}
		$in_clause .= $crawl_ids[$i];
	}
	$sql = "UPDATE crawling_list SET status=" . $status . " WHERE crawl_id IN (" . $in_clause . ")";
$log->freeform('publish_close_' . $target_ymd, $sql);

	try {
		$stmt = $pdo_tp->query($sql);
	} catch (PDOException $e) {
		$log->freeform('publish_close_' . $target_ymd, 'updateCrawlStatusエラー' . $e->getMessage());
		exit;
	}
}

/**
 * publishデータが公開かどうかを返す
 *
 * @param string $listing_type 'sell'または'rent'
 * @param int $publish_id
 * @return array{boolean $flg_closed, str $start_datetime} 
 */
function getPublishClosed($listing_type, $publish_id){
	# グローバル変数を参照
	global $stmt_get_publish_closed_rent;
	global $stmt_get_publish_closed_sell;
	global $log;

	$flg_closed = false;
	$start_datetime = '';
	if($listing_type == 'rent'){
        $stmt_get_publish_closed_rent->bindParam(':publish_id', $publish_id, PDO::PARAM_INT);
		try {
			$stmt_get_publish_closed_rent->execute();
			$row = $stmt_get_publish_closed_rent->fetch();
			if($row['status_flg'] == 1){
				$flg_closed = true;
			}
			$start_datetime = $row['start_datetime'];
		} catch (PDOException $e) {
			$log->freeform('publish_close_' . $target_ymd, 'getPublishClosed(rent)->param:' . $publish_id . '|' . $e->getMessage());
		}
	}else if($listing_type == 'sell'){
        $stmt_get_publish_closed_sell->bindParam(':publish_id', $publish_id, PDO::PARAM_INT);
		try {
			$stmt_get_publish_closed_sell->execute();
			$row = $stmt_get_publish_closed_sell->fetch();
			if($row['status_flg'] == 1){
				$flg_closed = true;
			}
			$start_datetime = $row['start_datetime'];
		} catch (PDOException $e) {
			$log->freeform('publish_close_' . $target_ymd, 'getPublishClosed(sell)->param:' . $publish_id . '|' . $e->getMessage());
		}
	}
	$arr_return = array($flg_closed, $start_datetime);	
	return $arr_return;
}

/**
 * publishデータを掲載終了にする
 *
 * @param string $listing_type 'sell'または'rent'
 * @param int $publish_id
 * @param int $duplicate_num
 * @param int $status_flg
 * @param int $termination_flg
 * @param int $close_num
 * @param int $close_yyyymmdd
 * @return void
 */
function closePublish($listing_type, $publish_id, $duplicate_num, $close_num, $termination_flg, $close_yyyymmdd){
	
	# グローバル変数を参照
	global $pdo_tp;
	global $log;
	global $target_ymd;

	$format_ymd = 'Ymd H:i:s';
	$close_datetime = DateTime::createFromFormat($format_ymd, $close_yyyymmdd . ' 00:00:00');
	$close_datetime_str = $close_datetime->format('Y/m/d H:i:s');
	$sql = "UPDATE publish_" . $listing_type . " SET duplicate_num=" . $duplicate_num . 
		", close_num=" . $close_num . ", termination_flg=" . $termination_flg . 
		", yyyymmdd=" . $close_yyyymmdd . ",close_datetime=TO_DATE('" . $close_datetime_str . "', " . DB_DATE_FORMAT . ")" .
		", publish_days=TO_NUMBER(TRUNC(TO_DATE('" . $close_datetime_str . "', " . DB_DATE_FORMAT . ")) - TRUNC(start_datetime))" .
		", status_flg=1,complete_datetime=SYSDATE + 9/24 WHERE publish_id=" . $publish_id;
$log->freeform('publish_close_' . $target_ymd, $sql);
	try {
		$stmt = $pdo_tp->query($sql);
	} catch (PDOException $e) {
		$log->freeform('publish_close_' . $target_ymd, 'closePublishエラー->' . $e->getMessage());
		exit;
	}

}
/**
 * publishデータを更新する(件数のみ)
 *
 * @param string $listing_type 'sell'または'rent'
 * @param int $publish_id
 * @param int $duplicate_num
 * @param int $close_num
 * @return void
 */
function updatePublishCount($listing_type, $publish_id, $duplicate_num, $close_num){
	# グローバル変数を参照
	global $stmt_update_publish_count_rent;
	global $stmt_update_publish_count_sell;
	global $log;
	global $target_ymd;

	if($listing_type == 'rent'){
        $stmt_update_publish_count_rent->bindParam(':publish_id', $publish_id, PDO::PARAM_INT);
        $stmt_update_publish_count_rent->bindParam(':duplicate_num', $duplicate_num, PDO::PARAM_INT);
        $stmt_update_publish_count_rent->bindParam(':close_num', $close_num, PDO::PARAM_INT);
		try {
			$stmt_update_publish_count_rent->execute();
		} catch (PDOException $e) {
			$log->freeform('publish_close_' . $target_ymd, 'updatePublishCountエラー(rent)->param:' . $publish_id . '|' . $e->getMessage());
		}
	}else if($listing_type == 'sell'){
        $stmt_update_publish_count_sell->bindParam(':publish_id', $publish_id, PDO::PARAM_INT);
        $stmt_update_publish_count_sell->bindParam(':duplicate_num', $duplicate_num, PDO::PARAM_INT);
        $stmt_update_publish_count_sell->bindParam(':close_num', $close_num, PDO::PARAM_INT);
		try {
			$stmt_update_publish_count_sell->execute();
		} catch (PDOException $e) {
			$log->freeform('publish_close_' . $target_ymd, 'updatePublishCountエラー(sell)->param:' . $publish_id . '|' . $e->getMessage());
		}
	}
}

/**
 * publishデータの開始日を更新する
 *
 * @param string $listing_type 'sell'または'rent'
 * @param int $publish_id
 * @param int $start_datetime
 * @return void
 */
function updatePublishStart($listing_type, $publish_id, $start_datetime){
	# グローバル変数を参照
	global $pdo_tp;
	global $log;
	global $target_ymd;

	$sql = "UPDATE publish_" . $listing_type . " SET start_datetime=TO_DATE('" . $start_datetime . "', " . DB_DATE_FORMAT . ") WHERE publish_id=" . $publish_id;
$log->freeform('publish_close_' . $target_ymd, $sql);
	try {
		$stmt = $pdo_tp->query($sql);
	} catch (PDOException $e) {
		$log->freeform('publish_close_' . $target_ymd, 'updatePublishStartエラー->param:' . $listing_type . '|' . $publish_id . '|' . $e->getMessage());
	}
}

?>
