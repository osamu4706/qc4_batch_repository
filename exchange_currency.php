<?php

require __DIR__.'/../import/config.php';
require __DIR__.'/../import/db_oracle.php';
require __DIR__.'/../import/log.php';


$pdo_tp = DB::getPdo(DB_ORA_TNS_TP);
//$log = new log();

/**
 * 為替レート取得サイトへのURL
 */
$usd_cd = "USD";
$urlFormat = 'http://ja.exchange-rates.org/converter/%s/USD/1';

/**
 * 国のコード（CURRENCY_CD）を取得する
 */
$currencies = array();
$sql = 'SELECT currency_cd FROM mst_currency';
$currencies = $pdo_tp->query($sql);

//結果の取り出し
$error_flg=false;
$rows = $currencies->fetchAll();
//print_r($rows);
//die();

// 取得エラーの場合は、処理の終了
$currencies_limit = 200;    // 通貨単位の上限値
$code_count = count($rows);
//print_r($code_count);
if($code_count < 1){
    echo "error occurred:get currency code";
    $ErrMsg = "通貨コードの取得エラーです:-1";
    outputErrorLog($ErrMsg);
    sendErrMail($ErrMsg);
    exit;
} elseif ($code_count > $currencies_limit){    //通貨単位の上限を超えた場合は、処理を終わらせる
    echo "error occurred:currency count";
    $ErrMsg = "通貨コードが上限をこえています";
    outputErrorLog($ErrMsg);
    sendErrMail($ErrMsg);
    exit;
}

/**
 * 通貨コードの数分、対USDの為替データを取得する
 */
$num = count($rows);
//echo "number::{$num}";
$flg = true;    //LOOP内の処理判定
$err_cnt = 0;   //リトライカウント用
for ($i=0; $i<$num; $i++) {

    //LOOPの最初に＄flgにtrueをセットする。
    if ($i == 0){
        $flg = true;
    }

    $currency = $rows[$i];
    $code_name = $currency['currency_cd'];
//    echo "country::{$code_name}";
    //print_r($code_name);

    //USD同士の場合は処理を飛ばす
    if ($code_name == $usd_cd) continue;

    //為替レート検索のためのURL作成
    $url = sprintf(
        $urlFormat,
        $code_name
    );
    //echo "url:{$url}";
    //検索結果の取得
    $match = array();

    // 接続先($url)のコンテンツを取得し、中身をチェックする。
    list($result,$html) = getCurrencyContents($url);
    //print($html);
    //die();

    /**
     * 取得したHTMLコンテンツのチェックを実行する　　
     */
    if ($result){  //コンテンツが正常に取得できた場合
        $html = str_replace(array("\r\n", "\r", "\n", "\t"), '', $html);
        //正規表現を使って金額を検索する
        preg_match("/<strong><span>([0-9.]+?)<\/span><span>USD<\/span><\/strong>/ismU", $html, $match);
//var_dump($match);
//exit();
        // 2021-06-19 modify Sasachika Shinohara
        //preg_match("/<span id=\"ctl00_M_lblToAmount\">(.*)<\/span>/ismU", $html, $match);
        //print_r($match);
        $value = str_replace(array(','), '', $match[1]);

        // $valueの値が数値かどうかのチェック
        if (is_null($value) || !is_numeric($value)) {
            echo "{$code_name}の為替レートの値が取得できませんでした。検索wordの確認を行ってください。";
            //$flg = false;
            $ErrMsg = "{$code_name}の為替レートの値が取得できませんでした。検索wordの確認を行ってください。";
            outputErrorLog($ErrMsg);
            sendErrMail($ErrMsg);
            //continue;
            exit();

        } else {
            //update処理
            //echo "code_name:{$code_name}";
            //echo "rate:{$value}";

            //test用
            //$update_sql = "UPDATE mst_currency_test SET exchange_rate=:rate, updated=sysdate+9/24 WHERE currency_cd=:code_name";
            $update_sql = "UPDATE mst_currency SET exchange_rate=:rate, updated=sysdate+9/24 WHERE currency_cd=:code_name";

            try {

                $stmt = $pdo_tp->prepare($update_sql);
                $stmt->bindParam(':code_name', $code_name, PDO::PARAM_STR);
                $stmt->bindParam(':rate', $value, PDO::PARAM_STR);

                $stmt->execute();   // updateSQLの実施

            } catch (PDOException $e) {
                $ErrMsg = "例外発生:{$e->getMessage()}";
                echo $e;
                outputErrorLog($ErrMsg);
                sendErrMail($ErrMsg);
                exit;
            }
        }
    } else {
        //　HTML取得時にエラーが発生していた場合は、ファイルにログを出力し、エラーメールを送信する。
        echo "コンテンツが正常に取得できませんでした";
        $flg = false;

        outputErrorLog("{$html}:{$url}");
        //sendErrMail("{$html}:{$url}");
        //exit;   // エラーになった時点で、処理を抜ける


    }

    // 一度でも処理エラーになった場合は、時間をおいて処理を繰り返す。
    if (!$flg && $err_cnt < 10) {
        $i = 0;
        $err_cnt++;
        sleep(10);
        continue;
    } elseif (!$flg && $err_cnt == 9) {     // 10回繰り返してもダメな場合は、ログ出力＋エラーメール出力する。
        outputErrorLog($html);
        sendErrMail("{$html}:{$url}");
        exit;
    }
}

/**
 * サイト受信内容チェック処理
 * 戻り値：[$result]チェック結果（true:OK, false:NG）
 * 　　　　　[$html]OKの場合：接続先のHTML内容
 *              NGの場合：エラーメッセージ
 */
function getCurrencyContents($url) {

    $result = true;

    list($result, $html) = checkURL($url);  // 接続先ドメインが有効か無効かのチェック
    if(!$result){   //チェック結果がfalseの場合は、結果をそのまま返す
        return array($result,$html);
    }

    //file_get_contents関数でデータを取得
    $html = file_get_contents($url);
    //print_r($html);
    //echo "file_get_contentsの結果:${html}";

    while (!$html) {   //file_get_contentsの結果がfalseの場合
        $result = false;
        echo "ドメインが不正です";
        $html = "タイムアウトまたはドメイン不正により、指定したページのデータを取得できませんでした";

        //file_get_contents関数処理が失敗した場合。10回はリトライする
        for ($i=0; $i<10; $i++){
            sleep(10);
            $contents = file_get_contents($url);

            if($contents) { // コンテンツが取れたらループを抜ける
                $result = true;
                $html = $contents;
                break;
            } else{
                echo "コンテンツが取れなかった";
                //$i++;

            }
            $html = $html + $i;
            //die();
        }
    }

    //var_dump($http_response_header);
    //$resultがtrueの場合のみ実行
    if($result){
        //HTTPステータスコードを取得する
        preg_match('/HTTP\/1\.[0|1|x] ([0-9]{3})/', $http_response_header[0], $matches);
        $statusCode = $matches[1];
        echo "error_header:{$statusCode}";

        // 正常値(200)、もしくは３００番台以外の場合は、対応するエラーコードが何かを取得する
        if ($statusCode >= 400){
            $result = false;
            $html = "指定したページのデータを取得できませんでした(エラーコード：${statusCode})";
        } elseif ($statusCode >= 504) {
            $result = false;
            $html = "タイムアウトまたはドメイン不正により、指定したページのデータを取得できませんでした(エラーコード：${statusCode})";
        }
    }

    return array($result,$html);
}

/**
 * URLのドメインチェック処理
 * 戻り値：[$result]チェック結果（true:OK, false:NG）
 * 　　　　　[$html]OKの場合：接続先のHTML内容
 *              NGの場合：エラーメッセージ
 */
function checkURL($url){
    //ドメインが有効かどうかのチェック
    $array = parse_url($url);
    $html = "";

    if ($array && $array['host']) {
        $ip = gethostbyname($array['host']);
        //echo "ip:${ip}";
        $long = ip2long($ip);
        if ($long === false || $ip !== long2ip($long)) {
            echo '指定したページは名前解決が出来ないため、存在しないドメイン';
            $html = "指定したページは名前解決が出来ないため、存在しないドメインです。";
            $result = false;
        } else {
            $result = true;
            //echo 'OK!存在するドメインです';
        }
    } else {
        echo '指定したページはURLの値が正しくありません';
        $html = "指定したページはURLの値が正しくありません。ドメイン不正により、指定したページのデータを取得できませんでした";
        $result = false;
    }
    return array($result,$html);
}

/**
 * エラー内容ファイル出力処理
 */
function outputErrorLog($ErrMsg){

    try {
        $log = new log();
        $log->freeform("currency_test.log", $ErrMsg);
        return true;
    } catch (Exception $e) {
        echo "outputErrorLog エラー:{$e->getMessage()}";
        return false;
    }

}


/**
 * メール送信処理
 * 　メール送信に失敗した場合は、ログファイルに出力する
 */
function sendInfoMail($to, $title, $body){
    mb_language("Japanese");
    mb_internal_encoding("UTF-8");

    $header = "From: $to\nReply-To: $to\n";

    if(mb_send_mail($to, $title, $body, $header)){
        echo "メールを送信しました";
        return true;
    } else {
        echo "メールの送信に失敗しました";
        outputErrorLog('メールの送信に失敗しました');
        return false;

    }
}

/**
 * Errorメール送信処理：本文のみ指定の場合使用する
 *
 */
function sendErrMail($body){

    mb_language("Japanese");
    mb_internal_encoding("UTF-8");

    $to = 'development@propre.com'; //エラーメールの送付先

    $header = "From: $to\nReply-To: $to\n";
    $title = '【ERROR】為替情報取得バッチ';

    try {
        mb_send_mail($to, $title, $body, $header);
        echo "処理エラーメールを送信しました";
        return true;
    } catch (Exception $e) {
        echo "エラーメールの送信に失敗しました";
        outputErrorLog("エラーメールの送信に失敗しました{$e->getMessage()}");
        return false;
    }
}
